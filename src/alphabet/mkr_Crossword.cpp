// Sriramajayam

#include <mkr_Crossword.h>
#include <opencv2/highgui.hpp>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/index/rtree.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <cassert>

namespace mkr
{
  // Alphabet board
  
  // Constructor
  Crossword::Crossword(const std::string xmlfile)
    :nsides(0),
     coloring{}
  {
    cv::FileStorage fs(xmlfile, cv::FileStorage::READ);
    CV_Assert(fs.isOpened());

    // dimensions
    int Nx, Ny;
    auto fn = fs["rows"];
    CV_Assert(!fn.empty());
    cv::read(fn, Nx, -1);
    CV_Assert(Nx>0);

    fn = fs["cols"];
    CV_Assert(!fn.empty());
    cv::read(fn, Ny, -1);
    CV_Assert(Ny>0);
    
    // num sides
    fn = fs["sides"];
    CV_Assert(!fn.empty());
    cv::read(fn, nsides, -1);
    CV_Assert(nsides==1 || nsides==2);
    if(nsides==2)
      CV_Assert(Ny%2==0);

    // coloring
    coloring.clear();
    fn = fs["coloring"];
    CV_Assert(!fn.empty());
    cv::read(fn, coloring);
    CV_Assert(!coloring.empty());
    CV_Assert(static_cast<int>(coloring.size())==Nx);
    for(auto& v:coloring)
      CV_Assert(static_cast<int>(v.size())==Ny);

    // dictionary size
    int dict_size;
    fn = fs["alphabet_size"];
    CV_Assert(!fn.empty());
    cv::read(fn, dict_size, -1);
    CV_Assert(dict_size>0);

    // number of bits
    int nbits;
    fn = fs["nbits"];
    CV_Assert(!fn.empty());
    cv::read(fn, nbits, -1);
    CV_Assert(nbits>=2);

    // num words
    int nwords;
    fn = fs["nwords"];
    CV_Assert(!fn.empty());
    cv::read(fn, nwords, -1);
    CV_Assert(nwords>0);

    // words
    std::vector<std::vector<int>> words{};
    fn = fs["words"];
    CV_Assert(!fn.empty());
    cv::read(fn, words);
    CV_Assert(static_cast<int>(words.size())==nwords);
    for(auto& w:words)
      CV_Assert(static_cast<int>(w.size())==5);

    // done reading
    fs.release();
    
    // create dictionary
    dictionary = cv::Ptr<cv::aruco::Dictionary>(new cv::aruco::Dictionary(cv::aruco::extendDictionary(dict_size, nbits)));
    CV_Assert(dictionary->bytesList.rows>=dict_size);

    // enumerate mwords
    mwords2indxMap.clear();
    int count = 0;
    for(auto& it:words)
      {
	CV_Assert(static_cast<int>(it.size())==5);
	WordType mword{it[0],it[1],it[2],it[3],it[4]};
	mwords2indxMap[mword] = count++;
      }

    // done
  }


  // Access the number of sides
  int Crossword::GetNumSides() const
  {
    return nsides;
  }

  // Access the coloring
  const std::vector<std::vector<int>>& Crossword::GetColoring() const
  {
    return coloring;
  }

  // Access the set of words
  WordListType Crossword::GetWords() const
  {
    WordListType words{};
    for(auto& it:mwords2indxMap)
      words.insert(it.first);
    return words;
  }
  
  // Paints requested side on a rectangular image
  void Crossword::Paint(const int marker_size, const int gap,
			const std::pair<int,int> top_left_corner,
			const int sidenum, cv::Mat_<uchar>& mat) const
  {
    if(nsides==1)
      CV_Assert(sidenum==0);
    else
      CV_Assert(sidenum==0 || sidenum==1);

    const int Nx = static_cast<int>(coloring.size());
    const int Ny = static_cast<int>(coloring[0].size());
    const int colstart = (sidenum==0) ? 0 : Ny/2;
    const int ncols    = (nsides==1)  ? Ny : Ny/2;
    const int colend   = colstart+ncols;

    CV_Assert(top_left_corner.first+Nx*(marker_size+gap)<mat.rows);
    CV_Assert(top_left_corner.second+ncols*(marker_size+gap)<mat.cols);

    // initialize to white
    mat.setTo(cv::Scalar(static_cast<uchar>(255)));
    
    cv::Mat_<uchar> marker_image;	      
    int row = top_left_corner.first;
    for(int p=0; p<Nx; ++p)
      {
	// reset column count
	int col = top_left_corner.second+gap;
	for(int q=colstart; q<colend; ++q)
	  {
	    const auto& marker_id = coloring[p][q];
	    if(marker_id>=0)
	      {
		// generate this marker
		cv::aruco::generateImageMarker(*dictionary, marker_id, marker_size, marker_image);
		assert(marker_image.rows==marker_size);
		assert(marker_image.cols==marker_size);

		// paint the image
		for(int i=0; i<marker_image.rows; ++i)
		  for(int j=0; j<marker_image.cols; ++j)
		    {
		      assert(row+i>=0 && row+i<mat.rows);
		      assert(col+j>=0 && col+j<mat.cols);
		      mat.at<uchar>(row+i,col+j) = marker_image.at<uchar>(i,j);
		    }
	      }
	    col += marker_size+gap;
	  }
	
	row += marker_size+gap;
	col = top_left_corner.second;
      }
    
    // done
    return;
  }

  
  // Detects markers in an image
  void Crossword::Detect(const std::string imagefile, std::map<Mkey, Mcoords2D>& key2coords) const
  {
    // read this image
    cv::Mat image = cv::imread(imagefile, cv::IMREAD_GRAYSCALE);
    CV_Assert(!image.empty());

    // detection options
    cv::Ptr<cv::aruco::DetectorParameters> detectorParams(new cv::aruco::DetectorParameters());
    detectorParams->cornerRefinementMethod = cv::aruco::CORNER_REFINE_CONTOUR;//SUBPIX;
    detectorParams->adaptiveThreshWinSizeMax = 100;
    //detectorParams->minMarkerPerimeterRate = 0.01;
    //detectorParams->maxMarkerPerimeterRate = 0.25;

    // detect markers
    std::vector<int> ids{};
    std::vector<std::vector<cv::Point2f>> coords{};
    cv::aruco::detectMarkers(image, dictionary, coords, ids, detectorParams);
    const int dict_size = dictionary->bytesList.rows;
    for(auto& m:ids)
      CV_Assert(m>=0 && m<dict_size);
    {
      cv::Mat detected = cv::imread(imagefile, cv::IMREAD_COLOR);
      cv::aruco::drawDetectedMarkers(detected, coords, ids);
      cv::imwrite("detected-" + imagefile, detected);
    }

    // detect neighbors. nighbors[i] = marker_index of {e,n,w,s} marker, -1 for non-existent neighbors
    std::map<int, std::array<int,4>> neighbors{};
    DetectNeighbors(coords, 0.5, neighbors);

    // detect words composed of marker indices
    std::set<std::array<int,5>> index_words{};
    for(auto& it:neighbors)
      {
	const auto& indx = it.first;
	const auto& nbs  = it.second;
	if(nbs[0]>=0 && nbs[1]>=0 && nbs[2]>=0 && nbs[3]>=0)
	  index_words.insert(std::array<int,5>{indx,nbs[0],nbs[1],nbs[2],nbs[3]});
      }

    // compute encodings: (word#,letter#,color#) -> 4-corner coordinates
    key2coords.clear();
    for(auto& word:index_words)
      {
	// this m-word
	WordType mword{ids[word[0]], ids[word[1]], ids[word[2]], ids[word[3]], ids[word[4]]};

	// m-word should be in the list
	auto jt = mwords2indxMap.find(mword);
	CV_Assert(jt!=mwords2indxMap.end());

	// mword #
	const int mword_num = jt->second;

	// letter #
	for(int letter_num=0; letter_num<5; ++letter_num)
	  {
	    const int& marker_indx = word[letter_num];
	    const auto& cnrs       = coords[marker_indx];
	    const int& id          = ids[marker_indx];
	    key2coords[std::make_tuple(mword_num,letter_num,id)] = std::array<cv::Point2f,4>{cnrs[0], cnrs[1], cnrs[2], cnrs[3]};
	  }
      }

    // done
    return;
  }


  // determine neighbor information
  void Crossword::DetectNeighbors(const std::vector<std::vector<cv::Point2f>>& coords,
				  const double eps,
				  std::map<int, std::array<int,4>>& neighbors)
  {
    const int nmarkers = static_cast<int>(coords.size());

    // order of corners: clockwise, starting from the top left corners
    const int nverts[] = {0,1};
    const int everts[] = {1,2};
    const int sverts[] = {2,3};
    const int wverts[] = {3,0};

    // marker sizes
    std::vector<double> msizes{};

    // midpoints of N, E, W and S edges
    std::vector<std::array<double,2>> n_mid{}, e_mid{}, w_mid{}, s_mid{};
    
    // Create rtrees for N,E,W and S edges
    namespace bgi = boost::geometry::index;
    using boost_point = boost::geometry::model::point<double, 2, boost::geometry::cs::cartesian>;
    using pointID = std::pair<boost_point, int>;
    using boost_tree = boost::geometry::index::rtree<pointID, bgi::quadratic<8>>;
    boost_tree ntree, etree, wtree, stree;
    double N[2], E[2], W[2], S[2];
    for(int i=0; i<nmarkers; ++i)
      {
	const auto& cnrs = coords[i];

	// mid points of each detected edge
	for(int j=0; j<2; ++j)
	  N[j] = E[j] = W[j] = S[j] = 0.;

	for(int j=0; j<2; ++j)
	  {
	    N[0] += 0.5*cnrs[nverts[j]].x;
	    N[1] += 0.5*cnrs[nverts[j]].y;

	    E[0] += 0.5*cnrs[everts[j]].x;
	    E[1] += 0.5*cnrs[everts[j]].y;

	    W[0] += 0.5*cnrs[wverts[j]].x;
	    W[1] += 0.5*cnrs[wverts[j]].y;

	    S[0] += 0.5*cnrs[sverts[j]].x;
	    S[1] += 0.5*cnrs[sverts[j]].y;
	  }

	// insert into rtrees
	ntree.insert(std::make_pair(boost_point(N[0],N[1]),i));
	etree.insert(std::make_pair(boost_point(E[0],E[1]),i));
	wtree.insert(std::make_pair(boost_point(W[0],W[1]),i));
	stree.insert(std::make_pair(boost_point(S[0],S[1]),i));

	// save mid points of edges
	n_mid.push_back( std::array<double,2>{N[0],N[1]} );
	e_mid.push_back( std::array<double,2>{E[0],E[1]} );
	w_mid.push_back( std::array<double,2>{W[0],W[1]} );
	s_mid.push_back( std::array<double,2>{S[0],S[1]} );

	// compute average marker size
	double size = 0.;
	for(int j=0; j<4; ++j)
	  {
	    const auto& A = cnrs[j];
	    const auto& B = cnrs[(j+1)%4];
	    size += std::sqrt( (A.x-B.x)*(A.x-B.x) +  (A.y-B.y)*(A.y-B.y) );
	  }
	size /= 4.;
	msizes.push_back(size);
      }

    
    // detect neighbors
    neighbors.clear();
    for(int i=0; i<nmarkers; ++i)
      {
	// mid points of edges for this marker
	const boost_point n_pt(n_mid[i][0], n_mid[i][1]);
	const boost_point e_pt(e_mid[i][0], e_mid[i][1]);
	const boost_point w_pt(w_mid[i][0], w_mid[i][1]);
	const boost_point s_pt(s_mid[i][0], s_mid[i][1]);
	
	// identify closest neighbors along each edge
	// use closest neighbor search on the opposite-sided-tree.
	std::vector<pointID> n_closest{}, e_closest{}, w_closest{}, s_closest{};

	// north edge
	stree.query(bgi::nearest(n_pt, 1), std::back_inserter(n_closest));
	CV_Assert(static_cast<int>(n_closest.size())==1);
	const auto& n_nb_pt = n_closest[0].first;
	const int n_nb_indx = n_closest[0].second;

	// east edge
	wtree.query(bgi::nearest(e_pt, 1), std::back_inserter(e_closest));
	CV_Assert(static_cast<int>(e_closest.size())==1);
	const auto& e_nb_pt = e_closest[0].first;
	const int e_nb_indx = e_closest[0].second;
	
	// west edge
	etree.query(bgi::nearest(w_pt, 1), std::back_inserter(w_closest));
	CV_Assert(static_cast<int>(w_closest.size())==1);
	const auto& w_nb_pt = w_closest[0].first;
	const int w_nb_indx = w_closest[0].second;
	
	// south edge
	ntree.query(bgi::nearest(s_pt, 1), std::back_inserter(s_closest));
	CV_Assert(static_cast<int>(s_closest.size())==1);
	const auto& s_nb_pt = s_closest[0].first;
	const int s_nb_indx = s_closest[0].second;

	// compute distances to neighbors
	const double n_dist = std::sqrt(std::pow(n_pt.get<0>()-n_nb_pt.get<0>(),2.) + std::pow(n_pt.get<1>()-n_nb_pt.get<1>(),2.));
	const double e_dist = std::sqrt(std::pow(e_pt.get<0>()-e_nb_pt.get<0>(),2.) + std::pow(e_pt.get<1>()-e_nb_pt.get<1>(),2.));
	const double w_dist = std::sqrt(std::pow(w_pt.get<0>()-w_nb_pt.get<0>(),2.) + std::pow(w_pt.get<1>()-w_nb_pt.get<1>(),2.));
	const double s_dist = std::sqrt(std::pow(s_pt.get<0>()-s_nb_pt.get<0>(),2.) + std::pow(s_pt.get<1>()-s_nb_pt.get<1>(),2.));

	// size of this marker
	const auto& msize = msizes[i];

	// identify indices of neighbors
	const int n_indx = ((n_nb_indx==i) || n_dist>eps*msize) ? -1 : n_nb_indx;
	const int e_indx = ((e_nb_indx==i) || e_dist>eps*msize) ? -1 : e_nb_indx;
	const int w_indx = ((w_nb_indx==i) || w_dist>eps*msize) ? -1 : w_nb_indx;
	const int s_indx = ((s_nb_indx==i) || s_dist>eps*msize) ? -1 : s_nb_indx;

	// save neighbors
	neighbors[i] = std::array<int,4>{e_indx, n_indx, w_indx, s_indx};
      }


    // Weed out non reflexive neighbor information: e<->w, ...
    for(auto& it:neighbors)
      {
	const int& indx = it.first;
	auto& nbs = it.second;

	// nb = m.neighbor(face j)
	// nb.neighbor(face (j+2)%4)) = m
	for(int j=0; j<4; ++j)
	  if(nbs[j]!=-1)
	    {
	      const int j_nb = (j+2)%4;
	      auto jt = neighbors.find(nbs[j]);
	      CV_Assert(jt!=neighbors.end() && "Could not find neighbor");
	      if(jt->second[j_nb]!=indx)
		{
		  // inconsistency in neighbor info. Discard both records
		  nbs[j] = -1;
		  jt->second[j_nb] = -1;
		}
	    }
      }
    
    // Run some checks
    for(auto& it:neighbors)
      {
	const int& indx = it.first;
	const auto& nbs = it.second;

	std::set<int> nbs_set{};
	for(int j=0; j<4; ++j)
	  if(nbs[j]!=-1)
	    {
	      // 1. Neighbor cannot be the marker itself
	      CV_Assert(nbs[j]!=indx);

	      // 2. Reflexivity with neighbor
	      // nb = m.neighbor(face j)
	      // nb.neighbor(face (j+2)%4)) = m
	      const int j_nb = (j+2)%4;
	      auto jt = neighbors.find(nbs[j]);
	      CV_Assert(jt!=neighbors.end() && "Could not find neighbor");
	      CV_Assert(jt->second[j_nb]==indx);

	      // 3. None of the neighbors can be repeated
	      auto kt = nbs_set.insert(nbs[j]);
	      CV_Assert(kt.second==true);
	    }
      }
    
    // done
    return;
  }


  // Paints detected mwords on an image
  void Crossword::PaintWords(const std::map<int,cv::Scalar>& colors,
			     const std::string imagefile,
			     const std::map<Mkey, Mcoords2D>& key2coords)
  {
    // Convert key to (word#, letter#) 
    std::map<std::pair<int,int>, Mcoords2D> newkey2coords{};
    std::map<std::pair<int,int>, int>       newkey2id{};
    std::set<int> word_list{};
    for(auto& it:key2coords)
      {
	newkey2coords[std::make_pair(std::get<0>(it.first),std::get<1>(it.first))] = it.second;
	newkey2id    [std::make_pair(std::get<0>(it.first),std::get<1>(it.first))] = std::get<2>(it.first);
	word_list.insert(std::get<0>(it.first));
      }
    
    // map from word index -> (id,coordinates) of 5 markers
    std::map<int, std::array<std::pair<int,Mcoords2D>,5>> word2Info{};
    for(auto& wnum:word_list)
      {
	std::array<std::pair<int,Mcoords2D>,5> info;
	for(int lnum=0; lnum<5; ++lnum)
	  {
	    auto it = newkey2coords.find({wnum,lnum});
	    CV_Assert(it!=newkey2coords.end());
	    auto jt = newkey2id.find({wnum,lnum});
	    CV_Assert(jt!=newkey2id.end());
	    info[lnum] = std::make_pair(jt->second,it->second);
	  }
	word2Info[wnum] = info;
      }

    // check that every marker has been assigned a color
    std::set<int> idset{};
    for(auto& it:newkey2id)
      idset.insert(it.second);
    for(auto& it:idset)
      assert(colors.find(it)!=colors.end());

    // Paint words
   
    //cv::Mat combined_mat = cv::Mat::zeros(mat.rows, mat.cols, mat.type());
    for(auto& it:word2Info)
      {
	const int& wnum  = it.first;
	const auto& info = it.second; // list of 5 (id,coordinates) 
	    
	//cv::Mat mword_mat = mat; //cv::Mat::zeros(mat.rows, mat.cols, mat.type());
	//CV_Assert(!mat.empty());

	cv::Mat mat = cv::imread(imagefile, cv::IMREAD_COLOR);
	CV_Assert(!mat.empty());
	 
	for(auto& minfo:info)
	  {
	    const int& mid      = minfo.first;
	    const auto& mcoords = minfo.second;
	    const auto& color = colors.find(mid)->second;
	    
	    // paint this marker
	    std::vector<cv::Point> mcoords_vec(4);
	    for(int i=0; i<4; ++i)
	      mcoords_vec[i] = cv::Point(static_cast<int>(mcoords[i].x), static_cast<int>(mcoords[i].y));
	    cv::fillPoly(mat, mcoords_vec, color, cv::LineTypes::LINE_8);

	    // write its id
	    cv::Point2f center(0.25*(mcoords[0].x+mcoords[1].x+mcoords[2].x+mcoords[3].x),
			       0.25*(mcoords[0].y+mcoords[1].y+mcoords[2].y+mcoords[3].y));
	    cv::putText(mat, std::to_string(mid), center, cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(255,255,255));
	  }

	// save this image with the word colored in
	cv::imwrite(imagefile+"-"+std::to_string(wnum)+".jpg", mat);
      }

    // done
    return;
  }
  
}
