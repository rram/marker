// Sriramajayam

#ifndef MKR_CROSS_WORD_H
#define MKR_CROSS_WORD_H

#include <mkr_Alphabet.h>
#include <opencv2/aruco.hpp>
#include <vector>
#include <map>

namespace mkr
{
  using Mkey      = std::tuple<int,int,int>;          //!< key (word#, letter#, color#) used to encode markers
  using Mcoords2D = std::array<cv::Point2f,4>;        //!< coordinates of 4 corners in 2D
  using Mcoords3D = std::array<cv::Point3d,4>;        //!< coordinates of 4 corners in 3D
    
  class Crossword
  {
  public:
    //! Constructor
    Crossword(const std::string xmlfile);
    
    //! Destructor
    inline virtual ~Crossword() {}

    // disable copy and assignment
    Crossword(const Crossword&) = delete;
    Crossword operator=(const Crossword&) = delete;

    //! Access the dictionary
    inline const cv::Ptr<cv::aruco::Dictionary> GetDictionary() const
    { return dictionary; }

    //! Access the number of sides
    int GetNumSides() const;

    //! Access the coloring
    const std::vector<std::vector<int>>& GetColoring() const;

    //! Access the set of words
    WordListType GetWords() const;
    
    //! Paints requested side on a rectangular image
    void Paint(const int marker_size, const int gap_pixels,
	       const std::pair<int,int> top_left_corner,
	       const int sidenum, cv::Mat_<uchar>& mat) const;

    //! Detects marker words in an image
    void Detect(const std::string imagefile, std::map<Mkey, Mcoords2D>& key2coords) const;

    //! Paints detected mwords on an image
    static void PaintWords(const std::map<int, cv::Scalar>& colors,
			   const std::string imagefile,
			   const std::map<Mkey, Mcoords2D>& key2coords);
    
  protected:
    static void DetectNeighbors(const std::vector<std::vector<cv::Point2f>>& coords,
				const double eps,
				std::map<int, std::array<int,4>>& mwords);
  private:
    int nsides;
    std::vector<std::vector<int>> coloring;
    std::map<WordType,int> mwords2indxMap;
    cv::Ptr<cv::aruco::Dictionary> dictionary;
  };
}

#endif
