// Sriramajayam

#include <mkr_Alphabet.h>
#include <random>
#include <iostream>
#include <opencv2/highgui.hpp>

namespace mkr
{
  
  // Constructor
  Alphabet::Alphabet(const int nx, const int ny,
		     const int word_dist,
		     int guess_alphabet_size,
		     int nbits)
    :isCreated(false),
     Nx(nx),
     Ny(ny),
     word_equivalence(word_dist),
     dict_size(guess_alphabet_size),
     num_bits(nbits),
     used_words{},
     layout(Nx, std::vector<bool>(Ny)),
     coloring(Nx, std::vector<int>(Ny))
  {
    assert(Nx>0 && Ny>0);
    assert(word_equivalence>0 && word_equivalence<5);
    assert(dict_size>0);
    assert(num_bits>0);
    
    // default layout
    for(int i=0; i<Nx; ++i)
      for(int j=0; j<Ny; ++j)
	layout[i][j] = true;
  }

  // Constructor
  Alphabet::Alphabet(const std::vector<std::vector<bool>>& lay_out,
		     const int word_dist, int guess_alphabet_size,
		     int nbits)
    :isCreated(false),
     Nx(static_cast<int>(lay_out.size())),
     Ny(static_cast<int>(lay_out[0].size())),
     word_equivalence(word_dist),
     dict_size(guess_alphabet_size),
     num_bits(nbits),
     used_words{},
     layout(lay_out),
     coloring(Nx, std::vector<int>(Ny))
  {
    for(int i=0; i<Nx; ++i)
      assert(static_cast<int>(layout[i].size())==Ny);
  }
    
  
  // Returns the computed dictionary size
  int Alphabet::GetAlphabetSize() const
  {
    assert(isCreated);
    return dict_size;
  }

  // Access the layout
  const std::vector<std::vector<bool>>& Alphabet::GetLayout() const
  { return layout; }

  // Returns the size of the board
  std::pair<int,int> Alphabet::GetLayoutSize() const
  {
    assert(isCreated);
    return std::make_pair(static_cast<int>(layout.size()), static_cast<int>(layout[0].size()));
  }
  
  // Access the coloring
  const std::vector<std::vector<int>>& Alphabet::GetColoring() const
  {
    assert(isCreated);
    return coloring;
  }

  // Access the number of bits
  int Alphabet::GetNumBits() const
  {
    return num_bits;
  }

  // Access the list of used words
  const WordListType& Alphabet::GetWords() const
  {
    assert(isCreated);
    return used_words;
  }
  
  // Utility to check if a given word exists in the list of used words
  bool Alphabet::IsNewWord(const WordType& word) const
  {
    for(auto& it:used_words)
      if(WordDistance(it,word)<=word_equivalence)
	return false;
    return true;
  }

  // Compute the inter-word distance
  int Alphabet::WordDistance(const WordType& A, const WordType& B) const
  {
    int ndiff = 0;
    for(int i=0; i<5; ++i)
      if(A[i]!=B[i])
	++ndiff;
    return ndiff;
  }

  // Returns the inter word distance
  int Alphabet::GetInterwordDistance() const
  {
    assert(isCreated==true);
    return word_equivalence;
  }
  
  // Verify correctness of a coloring
  void Alphabet::VerifyCorrectness() const
  {
    assert(dict_size>0);
    assert(static_cast<int>(coloring.size())==Nx);
    assert(static_cast<int>(layout.size())==Nx);
    
    // layout and coloring
    for(int i=0; i<Nx; ++i)
      {
	assert(static_cast<int>(coloring[i].size())==Ny);
	assert(static_cast<int>(layout[i].size())==Ny);
	
	for(int j=0; j<Ny; ++j)
	  if(layout[i][j]==true)
	    assert(coloring[i][j]>=0 && coloring[i][j]<dict_size);
	  else
	    assert(coloring[i][j]==-1);
      }
    
    for(auto& A:used_words)
      for(auto& B:used_words)
	if(A!=B)
	  assert(WordDistance(A,B)>word_equivalence);
     
    // Check for no duplication
    std::set<WordType> word_set{};
    for(int i=0; i<Nx; ++i)
      for(int j=0; j<Ny; ++j)
	if(HasWord(i,j))
	  {
	    auto it = word_set.insert( GetWord(i,j) );
	    assert(it.second==true);
	  }
    assert(word_set.size()==used_words.size());
    assert(word_set==used_words);
    
    return;
  }


  // Attempt to revise the coloring
  bool Alphabet::ReviseColoring(const int i, const int j)
  {
    assert(i>=0 && i<Nx && j>=0 && j<Ny);
    
    // there should be a need for a new word at (i,j)
    assert(HasWord(i,j));
    auto indx = GetWordIndices(i,j);
    auto word = GetWord(i,j);
    assert(IsNewWord(word)==false);

    // attempt changing the letter to the south
    int new_S = -1;
    for(int k=0; k<dict_size; ++k)
      {
	WordType test_word{word[0],word[1],word[2],word[3],k};
	if(IsNewWord(test_word)==true)
	  {
	    new_S = k;
	    break;
	  }
      }
  
    // managed to find a new word by changing entry to the south?
    if(new_S!=-1)
      {
	word[4] = new_S;
	assert(IsNewWord(word)==true);
	coloring[indx[4].first][indx[4].second] = word[4];
	used_words.insert(word);
	return true;
      }
    
    // attempt changing the letter to the east
    // this requires examining the word at the North-East neighbor
    if(Has_NE_Neighbor_Word(i,j))
      {
	int new_E = -1;
	auto ne_indx = Get_NE_Neighbor(i,j);
	WordType word_nb = GetWord(ne_indx.first, ne_indx.second);
	int nErased = used_words.erase(word_nb);
	assert(nErased==1);
	for(int k=0; k<dict_size; ++k)
	  {
	    WordType test_word1{word[0],k,word[2],word[3],word[4]};
	    WordType test_word2{word_nb[0],word_nb[1],word_nb[2],word_nb[3],k};

	    if(WordDistance(test_word1,test_word2)>word_equivalence && 
	       IsNewWord(test_word1)==true &&   // new word at (i,j)
	       IsNewWord(test_word2)==true)     // new word at (i-1,j+1)
	      {
		new_E = k;
		break;
	      }
	  }
      
	if(new_E!=-1)
	  {
	    coloring[indx[1].first][indx[1].second] = new_E;
	    word[1]    = new_E;
	    word_nb[4] = new_E;
	    assert(IsNewWord(word)==true && IsNewWord(word_nb)==true);
	    auto it = used_words.insert(word);     assert(it.second==true);
	    auto jt = used_words.insert(word_nb);  assert(jt.second==true);
	    return true;
	  }
	else
	  // undo erasure of neighbor word
	  used_words.insert(word_nb);
      }
    else  // no neighbor to the NE => no NE word to worry about
      {
	int new_E = -1;
	for(int k=0; k<dict_size; ++k)
	  {
	    WordType test_word{word[0],k,word[2],word[3],word[4]};
	    if(IsNewWord(test_word))
	      {
		new_E = k;
		break;
	      }
	  }
	if(new_E!=-1)
	  {
	    coloring[indx[1].first][indx[1].second] = new_E;
	    word[1]    = new_E;
	    assert(IsNewWord(word)==true);
	    used_words.insert(word);
	    return true;
	  }
      }

    return false; // revision failed. Need a larger slate
  }


  // Main functionality: compute the coloring
  void Alphabet::Compute(int nIters)
  {
    assert(Nx>0 && Ny>0);
    assert(word_equivalence>0 && word_equivalence<5);
    assert(dict_size>0);
    assert(isCreated==false);
    assert(nIters>0);
    const int dict_size_guess = dict_size;
    int min_dict_size = -1;
    std::vector<std::vector<int>> min_coloring{};
    WordListType min_used_words{};
    
    for(int iter=0; iter<nIters; ++iter)
      {
	// Initialize coloring for this iteration
	dict_size = dict_size_guess;

	bool success = false;
	while(success==false)
	  {
	    success = true;
	    
	    // Random initialization with currect slate
	    std::random_device rd;
	    std::mt19937 mt(rd());
	    std::uniform_int_distribution<int> dist(0, dict_size-1);
	    for(int i=0; i<Nx; ++i)
	      for(int j=0; j<Ny; ++j)
		if(layout[i][j]==true)
		  coloring[i][j] = dist(mt);
		else
		  coloring[i][j] = -1;

	    used_words.clear();

	    // Correct the alphabet on the fly
	    for(int i=0; i<Nx && success==true; ++i)
	      for(int j=0; j<Ny && success==true; ++j)
		if(HasWord(i,j))                         // Does this location define a word?
		  {
		    // Word # (i,j)
		    auto word = GetWord(i,j);

		    // If this word is new, add it to the list of used words
		    // If not, attempt to revise the word. If this fails, increase the dictionary size
		    if(IsNewWord(word)==true)
		      used_words.insert(word);
		    else
		      {
			bool is_revised = ReviseColoring(i, j);
			if(is_revised==false) // revision failed => need a larger slate
			  {
			    ++dict_size;
			    success = false;
			  }
		      }
		  }
	  }

	// Verify correctness of the computed coloring
	VerifyCorrectness();
	
	// Save this alphabet if it is the best one yet
	if(min_dict_size==-1 || dict_size<min_dict_size)
	  {
	    min_dict_size  = dict_size;
	    min_coloring   = coloring;
	    min_used_words = used_words;
	  }
      }

    // Save the best dictionary
    dict_size  = min_dict_size;
    coloring   = min_coloring;
    used_words = min_used_words;
    VerifyCorrectness();
    isCreated = true;

    return;
  }


  // Save coloring to xml file
  void Alphabet::Save(const std::string xmlfile)
  {
    // Open this file
    cv::FileStorage fs(xmlfile, cv::FileStorage::WRITE);
    CV_Assert(fs.isOpened() && "Alphabet::Save- Could not open file");

    // Size
    fs.writeComment((char*)"Number of rows", 0);
    fs<<"rows"<<Nx;
    
    fs.writeComment((char*)"Number of cols", 0);
    fs<<"cols"<<Ny;

    fs.writeComment((char*)"Interword distance", 0);
    fs<<"word_distance"<<word_equivalence;
    
    // Type
    int nsides = GetNumSides();
    assert(nsides==1 || nsides==2);
    fs.writeComment((char*)"Number of sides", 0);
    fs<<"sides"<<nsides;
    
    // dictionary size
    fs.writeComment((char*)"Alphabet size", 0);
    fs<<"alphabet_size" << dict_size;

    // number of bits
    fs.writeComment((char*)"num bits", 0);
    fs<<"nbits" << num_bits;
    
    // coloring
    fs.writeComment((char*)"Coloring", 0);
    cv::Mat_<int> mat_coloring(Nx, Ny);
    fs<<"coloring" << coloring;
    
    // number of words
    const int nwords = static_cast<int>(used_words.size());
    fs.writeComment((char*)"num words", 0);
    fs<<"nwords" << nwords;
    
    // words
    fs.writeComment((char*)"words", 0);
    std::vector<std::vector<int>> word_vec(nwords, std::vector<int>(5));
    int count = 0;
    for(auto& it:used_words)
      {
	for(int k=0; k<5; ++k)
	  word_vec[count][k] = it[k];
	++count;
      }
    fs << "words" << word_vec;

    fs.release();
    return;
  }


  // Create from a file
  Alphabet::Alphabet(const std::string xmlfile)
    :isCreated(false),
     Nx(-1),
     Ny(-1),
     word_equivalence(-1),
     dict_size(-1),
     num_bits(-1),
     used_words{},
     layout{},
     coloring{}
  {
    // Open the xml file to create this surface from
    cv::FileStorage fs(xmlfile, cv::FileStorage::READ);
    CV_Assert(fs.isOpened() && "Alphabet- could not open xml file");

    // Read the dimensions
    auto fn = fs["rows"];
    CV_Assert(!fn.empty() && "Could not read number of rows");
    cv::read(fn, Nx, -1);
    CV_Assert(Nx>0 && "Alphabet- Unexpected number of rows");

    fn = fs["cols"];
    CV_Assert(!fn.empty() && "Could not read number of cols");
    cv::read(fn, Ny, -1);
    CV_Assert(Ny>0 && "Alphabet- Unexpected number of cols");

    // inter word distance
    fn = fs["word_distance"];
    CV_Assert(!fn.empty() && "Could not read inter word distance");
    cv::read(fn, word_equivalence, -1);
    CV_Assert((word_equivalence>=0 && word_equivalence<4) && "Alphabet- Unexpected interword distance");

    // #sides = 1 or 2
    fn = fs["sides"];
    CV_Assert(!fn.empty() && "Could not read number of sides");
    int nsides;
    cv::read(fn, nsides, -1);
    CV_Assert((nsides==1 || nsides==2) && "Alphabet- Unexpected number of sides");

    // alphabet size
    fn = fs["alphabet_size"];
    CV_Assert(!fn.empty() && "Could not read alphabet size");
    cv::read(fn, dict_size, -1);
    CV_Assert(dict_size>0 && "Alphabet- Unexpected alphabet size");

    // number of bits
    fn = fs["nbits"];
    CV_Assert(!fn.empty() && "Could not read num bits");
    cv::read(fn, num_bits, -1);
    CV_Assert(num_bits>=1 && "Alphabet- Unexpected number of bits");
    
    // coloring
    fn = fs["coloring"];
    CV_Assert(!fn.empty() && "Could not read coloring");
    cv::read(fn, coloring);
    assert(static_cast<int>(coloring.size())==Nx);
    assert(static_cast<int>(coloring[0].size())==Ny);
        
    // layout
    layout = std::vector<std::vector<bool>>(Nx, std::vector<bool>(Ny));
    for(int i=0; i<Nx; ++i)
      for(int j=0; j<Ny; ++j)
	layout[i][j] = (coloring[i][j]==-1) ? false : true;
    
    // number of words
    int nwords;
    fn = fs["nwords"];
    CV_Assert(!fn.empty() && "Could not read number of words");
    cv::read(fn, nwords, -1);
	    
    // words
    std::vector<std::vector<int>> word_vec{};
    fn = fs["words"];
    CV_Assert(!fn.empty() && "Could not read words");
    cv::read(fn, word_vec);
    assert(static_cast<int>(word_vec.size())==nwords);
    assert(static_cast<int>(word_vec[0].size())==5);
    used_words.clear();
    for(auto& w:word_vec)
      used_words.insert(WordType{w[0],w[1],w[2],w[3],w[4]});
      
    // Done reading
    fs.release();

    // done
    isCreated = true;
  }

  // Gets the interword distance histogram
  std::vector<int> Alphabet::GetInterwordDistanceHistogram() const
  {
    assert(isCreated);

    std::vector<int> histogram(5,0);
    for(auto& A:used_words)
      for(auto& B:used_words)
	if(A!=B)
	  ++histogram[WordDistance(A,B)];
  
    return histogram;
  }

  // Count the number of encoded and non encoded locations
  void Alphabet::Count(int& encoded, int& not_encoded) const
  {
    assert(isCreated==true);

    // Track the encoded locations
    std::set<std::pair<int,int>> encoded_set{}, all_pairs{}, not_encoded_set{};
    for(int i=0; i<Nx; ++i)
      for(int j=0; j<Ny; ++j)
	if(layout[i][j]==true)
	  {
	    all_pairs.insert(std::make_pair(i,j));
	    if(HasWord(i,j))
	      {
		auto indx = GetWordIndices(i,j);
		for(auto& it:indx)
		  encoded_set.insert(std::make_pair(it.first,it.second));
	      }
	  }

    // unencoded
    std::set_difference(all_pairs.begin(), all_pairs.end(),
			encoded_set.begin(), encoded_set.end(),
			std::inserter(not_encoded_set, not_encoded_set.begin()));

    // count
    encoded = static_cast<int>(encoded_set.size());
    not_encoded = static_cast<int>(not_encoded_set.size());
    assert(encoded+not_encoded==static_cast<int>(all_pairs.size()));

    return;
  }
  
}
