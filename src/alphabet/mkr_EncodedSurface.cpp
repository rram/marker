// Sriramajayam

#include <mkr_EncodedSurface.h>
#include <mkr_ICP.h>
#include <iostream>

namespace mkr
{
  // Template XML file
  void EncodedSurface::CreateTemplate(const cv::String xmlfile)
  {
    // Create a set of dummy template parameters
    
    // Open this file
    cv::FileStorage fs(xmlfile, cv::FileStorage::WRITE);
    CV_Assert(fs.isOpened() && "Could not open file");

    // Coloring
    fs.writeComment((char*)"xml file with coloring details", 0);
    fs<<"coloring_xml"<<"coloring.xml";
	
    // Number of poses
    fs.writeComment((char*)"number of poses", 0);
    fs<<"numPoses"<<2;
    
    // Paths to left camera files for all poses
    fs.writeComment((char*)"filenames for images from the left camera in order", 0);
    fs<<"lcam_filenames"<< std::vector<cv::String>{"lcam_filename_1","lcam_filename_2"};

    // Absolute paths to right camera files for all poses
    fs.writeComment((char*)"Right camera file names for all poses", 0);
    fs<<"rcam_filenames"<< std::vector<cv::String>{"rcam_filename_1","rcam_filename_2"};

    // Merge sequence
    fs.writeComment((char*)"Merge sequence for poses", 0);
    fs<<"merge_sequence" << std::vector<int>{0,1};
	
    fs.release();
  }


  // Read an XML file
  void EncodedSurface::ReadXML(const cv::String xmlfile,
			       cv::String& coloring_xmlfile,
			       std::vector<cv::String>& lcam_files,
			       std::vector<cv::String>& rcam_files,
			       std::vector<int>& merge_sequence)
  {
    // Open this file
    cv::FileStorage fs(xmlfile, cv::FileStorage::READ);
    CV_Assert(fs.isOpened() && "Could not open file");

    // Coloring xml file
    auto fn = fs["coloring_xml"];
    CV_Assert(!fn.empty());
    cv::read(fn, coloring_xmlfile, "");
    CV_Assert(!coloring_xmlfile.empty());

    // Number of poses
    int numPoses;
    fn = fs["numPoses"];
    CV_Assert(!fn.empty());
    cv::read(fn, numPoses, -1);
    CV_Assert(numPoses>=1);

    // left camera images
    lcam_files.clear();
    fn = fs["lcam_filenames"];
    CV_Assert(!fn.empty() );
    cv::read(fn, lcam_files);
    CV_Assert(static_cast<int>(lcam_files.size())==numPoses);

    // right camera images
    rcam_files.clear();
    fn = fs["rcam_filenames"];
    CV_Assert(!fn.empty() );
    cv::read(fn, rcam_files);
    CV_Assert(static_cast<int>(rcam_files.size())==numPoses);

    // merge sequence
    merge_sequence.clear();
    fn = fs["merge_sequence"];
    CV_Assert(!fn.empty());
    cv::read(fn, merge_sequence);
    CV_Assert(static_cast<int>(merge_sequence.size())==numPoses);
    std::set<int> merge_set(merge_sequence.begin(), merge_sequence.end()); 
    CV_Assert(static_cast<int>(merge_set.size())==numPoses);

    // done
    fs.release();
    return;
  }

  
  // Triangulate pose
  void EncodedSurface::TriangulatePose(const str::Triangulator& tri, EncodedPose& pose)
  {
    // triangulate common locations of left and right images
    assert(!pose.left_key2coords.empty() && !pose.right_key2coords.empty());
    for(auto& it:pose.left_key2coords)
      {
	const auto& mkey = it.first;
	auto jt = pose.right_key2coords.find(mkey);  
	auto kt = pose.key2coords.find(mkey);       

	// triangulate only common markers. do not repeat triangulation
	if(jt!=pose.right_key2coords.end() && kt==pose.key2coords.end())
	  {
	    cv::Point3d pt;
	    const auto& left_cnrs  = it.second;
	    const auto& right_cnrs = jt->second;

	    // triangulate common markers
	    std::array<cv::Point3d,4> X;
	    for(int k=0; k<4; ++k)
	      tri.Triangulate<str::TriMethod::Sym>(left_cnrs[k], right_cnrs[k], X[k]);

	    pose.key2coords[mkey] = X;
	  }
      }
    return;
  }


  
  // Merges poses
  void EncodedSurface::MergePoses(const std::map<Mkey,Mcoords3D>& src,
				  std::map<Mkey,Mcoords3D>& tgt,
				  Transformation& transformation,
				  std::set<Mkey>& commonKeys)
  {
    commonKeys.clear();
    
    // Collate a list of correspondences
    std::vector<cv::Point3d> src_points{};
    std::vector<cv::Point3d> tgt_points{};
    for(auto& it:src)
      {
	const auto& mkey = it.first;
	auto jt = tgt.find(mkey);
	if(jt!=tgt.end())
	  {
	    // it.second and jt->second correspond
	    commonKeys.insert(mkey);
	    for(int k=0; k<4; ++k)
	      {
		src_points.push_back( it.second[k] );
		tgt_points.push_back( jt->second[k] );
	      }
	  }
      }

    // should have sufficiently many corresponding points
    CV_Assert(static_cast<int>(commonKeys.size())>=4);

    // compute the isometry using ICP
    double Rot[3][3], Tvec[3];
    ComputeIsometry(tgt_points, src_points, Rot, Tvec);
    for(int i=0; i<3; ++i)
      {
	transformation.Tvec[i] = Tvec[i];
	for(int j=0; j<3; ++j)
	  transformation.Rot[i][j] = Rot[i][j];
      }

    // transform coordinates of the source
    for(auto& it:src)
      {
	// transform
	std::array<cv::Point3d,4> transformed_cnrs;
	for(int k=0; k<4; ++k)
	  transformed_cnrs[k] = transformation.Apply(it.second[k]);
	
	// new key in the target => append
	// otherwise, update to the averaged location
	const auto& mkey = it.first;
	auto jt = tgt.find(mkey);
	if(jt==tgt.end())
	  tgt[mkey] = transformed_cnrs;
	else
	  for(int k=0; k<4; ++k)
	    {
	      jt->second[k].x *= 0.5;
	      jt->second[k].x += 0.5*transformed_cnrs[k].x;

	      jt->second[k].y *= 0.5;
	      jt->second[k].y += 0.5*transformed_cnrs[k].y;

	      jt->second[k].z *= 0.5;
	      jt->second[k].z += 0.5*transformed_cnrs[k].z;
	    }
      }
	
    // done
    return;
  }

  // Constructor
  EncodedSurface::EncodedSurface(const cv::String experiment_xml)
    
    :isProcessed(false),
     numPoses(0),
     poses{}
  {
    // read the xml file
    cv::String coloring_xml;
    std::vector<cv::String> lcam_files{}, rcam_files{};
    std::vector<int> merge_sequence{};
    ReadXML(experiment_xml, coloring_xml, lcam_files, rcam_files, mergedPose.sequence);
    numPoses = static_cast<int>(lcam_files.size());
    Crossword cword(coloring_xml);
    
    // initialize pose information
    poses.resize(numPoses);
    for(int p=0; p<numPoses; ++p)
      {
	poses[p].left_img  = lcam_files[p];
	poses[p].right_img = rcam_files[p];

	// detect encodings
	std::cout<<std::endl
		 <<"Decoding pose "<<p << " from files "<<poses[p].left_img<<" and "<<poses[p].right_img<<": ";
	cword.Detect(poses[p].left_img,  poses[p].left_key2coords);
	cword.Detect(poses[p].right_img, poses[p].right_key2coords);
	std::cout<<std::endl
		 <<" # detected words: L = "<<poses[p].left_key2coords.size()
		 <<", R = "<<poses[p].right_key2coords.size() << std::endl;
      }

    // done
  }


  // Triangulate and merge poses
  void EncodedSurface::Triangulate(const str::Triangulator& tri)
  {
    assert(isProcessed==false);
    
    // triangulate each pose
    for(int p=0; p<numPoses; ++p)
      {
	std::cout<< std::endl << "Merging pose "<<p;
	TriangulatePose(tri, poses[p]);
      }
    
    // Initialize merged pose members
    mergedPose.commonKeys.resize(numPoses-1);
    mergedPose.transformations.resize(numPoses-1);
    mergedPose.key2coords.clear();

    // merge poses following the given sequence
    const auto& sequence = mergedPose.sequence;
    mergedPose.key2coords = poses[sequence[0]].key2coords;
    for(int i=0; i<numPoses-1; ++i)
      {
	std::cout<<"\nMerging pose "<<sequence[i] <<" to "<<sequence[i+1] << std::flush;
	std::map<Mkey,Mcoords3D> target = poses[sequence[i+1]].key2coords;
	MergePoses(mergedPose.key2coords, target, mergedPose.transformations[i], mergedPose.commonKeys[i]);
	mergedPose.key2coords.clear();
	mergedPose.key2coords = target;
      }

    // done
    isProcessed = true;
    return;
  }
  
}
