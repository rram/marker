// Sriramajayam

#ifndef MKR_ALPHABET_H
#define MKR_ALPHABET_H

#include <array>
#include <set>
#include <cassert>
#include <vector>
#include <string>

namespace mkr
{
  using WordType = std::array<int,5>;

  using WordListType = std::set<WordType>;
  
  class Alphabet
  {
  public:
    //! Constructor
    Alphabet(const int nx, const int ny, const int word_dist,
	     int guess_alphabet_size, int nbits);

    //! Constructor
    Alphabet(const std::vector<std::vector<bool>>& lay_out,
	     const int word_dist, int guess_alphabet_size, int nbits);
    
    //! Constructor that initializes from a file
    Alphabet(const std::string xmlfile);
    
    //! Destructor
    inline virtual ~Alphabet() {}

    // Disable copy and assignment
    Alphabet(const Alphabet&) = delete;
    
    //! Main functionality: compute the coloring
    virtual void Compute(int nIters);

    //! Returns the computed dictionary size
    int GetAlphabetSize() const;

    //! Returns the size of the board
    std::pair<int,int> GetLayoutSize() const;

    //! Returns the inter word distance
    int GetInterwordDistance() const;

    //! Returns the number of bits
    int GetNumBits() const;
      
    //! Access the layout
    const std::vector<std::vector<bool>>& GetLayout() const;
    
    //! Access the coloring
    const std::vector<std::vector<int>>& GetColoring() const;

    //! Access the list of used words
    const WordListType& GetWords() const;
      
    // Save coloring to xml file
    void Save(const std::string xmlfile);

    // Gets the interword distance histogram
    std::vector<int> GetInterwordDistanceHistogram() const;

    // Count the number of encoded and non encoded locations
    void Count(int& encoded, int& not_encoded) const;

    //! Number of sides being colored
    virtual int GetNumSides() const = 0;
    
  protected:
    
    //! Check if a word exists at a given location
    virtual bool HasWord(int i, int j) const = 0;

    //! Access the indices defining a word
    virtual std::array<std::pair<int,int>,5> GetWordIndices(int i, int j) const = 0;

    //! Check if there is a word defined for the neighbor to the North-East
    virtual bool Has_NE_Neighbor_Word(int i, int j) const = 0;

    //! Access the location of the neighbor to the North-East
    virtual std::pair<int,int> Get_NE_Neighbor(int i, int j) const = 0;
    
    //! Access the word at a given location
    WordType GetWord(int i, int j) const
    {
      assert(i>=0 && i<Nx && j>=0 && j<Ny);
      assert(HasWord(i,j));
      auto indx = GetWordIndices(i,j);
      return WordType{
	coloring[indx[0].first][indx[0].second],
	  coloring[indx[1].first][indx[1].second],
	  coloring[indx[2].first][indx[2].second],
	  coloring[indx[3].first][indx[3].second],
	  coloring[indx[4].first][indx[4].second]
	  };
    }

    //! Utility to check if a given word exists in the list of used words
    bool IsNewWord(const WordType& word) const;

    //! Compute the inter-word distance
    int WordDistance(const WordType& A, const WordType& B) const;

    // Attempt to revise the coloring
    virtual bool ReviseColoring(const int i, const int j);
    
    //! Verify correctness of a coloring
    void VerifyCorrectness() const;

    bool isCreated;              //! Has the alphabet been created?
    int Nx;                      //!< Number of rows
    int Ny;                      //!< Number of cols
    int word_equivalence;       //!< Minimum interword distance
    int dict_size;              //!< Size of the dictionary
    int num_bits;               //!< number of bits
    std::vector<std::vector<bool>> layout;  //!< layout of the board
    std::vector<std::vector<int>> coloring; //!< Computed coloring
    WordListType used_words;                //!< List of used words
  };
}

#endif
