// Sriramajayam

#ifndef MKR_TWO_SIDE_RECTANGLE_H
#define MKR_TWO_SIDE_RECTANGLE_H

#include <mkr_Alphabet.h>
#include <string>

namespace mkr
{
  class TwoSideRectangle: public Alphabet
  {
  public:
    //! Constructor
    inline TwoSideRectangle(const std::string xmlfile)
      :Alphabet(xmlfile)
    {
      // Verify correctness
      VerifyCorrectness();
    }
    
    //! Constructor
    inline TwoSideRectangle(const int rows, const int cols,
			    const int word_dist, int guess_alphabet_size, int nbits)
      :Alphabet(rows, cols, word_dist, guess_alphabet_size, nbits)
    {
      // require even number of columns
      // front:   cols 0 to Ny/2-1
      // reverse: cols Ny/2 to Ny
      assert(cols%2==0);
    }

    //! Disable copy and assignment
    TwoSideRectangle(const TwoSideRectangle&) = delete;
    TwoSideRectangle operator=(const TwoSideRectangle&) = delete;

    //! Number of sides being colored
    inline int GetNumSides() const override final
    { return 2; }
    
  protected:
    
    //! Check if a word exists at a given location
    inline virtual bool HasWord(int i, int j) const override
    {
      const int ny = Ny/2;
      assert(i>=0 && i<Nx && j>=0 && j<Ny);
      if(j>=ny) j -= ny;
      return (i-1>=0 && i+1<Nx && j-1>=0 && j+1<ny);
    }

    //! Access the word at a given location
    std::array<std::pair<int,int>,5> GetWordIndices(int i, int j) const override
      {
	assert(i>=0 && i<Nx && j>=0 && j<Ny);
	return std::array<std::pair<int,int>,5>{
	  std::make_pair(i,j),
	    std::make_pair(i,j+1),
	    std::make_pair(i-1,j),
	    std::make_pair(i,j-1),
	    std::make_pair(i+1,j)};
      }

    //! Check if there is a word defined at the neighbor to the North-East
    bool Has_NE_Neighbor_Word(int i, int j) const override
    {
      assert(i>=0 && i<Nx && j>=0 && j<Ny);

      const int ny = Ny/2;
      if(j>=ny) j-= ny;
      return (i>=2 && j<=ny-3);
    }

    //! Access the location of the neighbor to the North-East
    virtual std::pair<int,int> Get_NE_Neighbor(int i, int j) const override
    {
      assert(i>=0 && i<Nx && j>=0 && j<Ny);
      assert(Has_NE_Neighbor_Word(i,j));
      return std::make_pair(i-1,j+1);
    }
  };
}

#endif
