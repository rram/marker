// Sriramajayam

#ifndef MKR_ONESIDE_LAYOUT_H
#define MKR_ONESIDE_LAYOUT_H

#include <mkr_Alphabet.h>

namespace mkr
{
  class OneSideLayout: public Alphabet
  {
  public:
    //! Constructor
    inline OneSideLayout(const std::string xmlfile)
      :Alphabet(xmlfile)
    {
      // Verify correctness
      VerifyCorrectness();
    }

    //! Constructor
    inline OneSideLayout(const std::vector<std::vector<bool>>& lay_out,
			 const int word_dist, int guess_alphabet_size, int nbits)
      :Alphabet(lay_out, word_dist, guess_alphabet_size, nbits) {}

    //! Destructor
    inline virtual ~OneSideLayout() {}

    //! Disable copy and assignment
    OneSideLayout(const OneSideLayout&) = delete;
    OneSideLayout operator=(const OneSideLayout&) = delete;

    //! Number of sides being colored
    inline int GetNumSides() const override final
    { return 1; }
    
  protected:

    //! check is a word exists at a give location
    inline bool HasWord(int i, int j) const override final
    {
      assert(i>=0 && i<Nx && j>=0 && j<Ny);

      // all 5 indices should be within bounds and should be part of the layout
      if(i-1>=0 && i+1<Nx && j-1>=0 && j+1<Ny)
	{
	  bool flag = true;
	  auto indx = GetWordIndices(i,j);
	  for(auto& it:indx)
	    {
	      assert(it.first>=0 && it.first<Nx && it.second>=0 && it.second<Ny);
	      if(layout[it.first][it.second]==false)
		flag = false;
	    }
	  return flag;
	}
      else
	return false;
    }
    
    //! Access the word at a given location
    std::array<std::pair<int,int>,5> GetWordIndices(int i, int j) const override final
      {
	assert(i>=0 && i<Nx && j>=0 && j<Ny);
	return std::array<std::pair<int,int>,5>{
	  std::make_pair(i,j),
	    std::make_pair(i,j+1),
	    std::make_pair(i-1,j),
	    std::make_pair(i,j-1),
	    std::make_pair(i+1,j)};
      }

    //! Check if there is a word defined at the neighbor to the North-East
    bool Has_NE_Neighbor_Word(int i, int j) const final
    {
      assert(i>=0 && i<Nx && j>=0 && j<Ny);
      if(i>=2 && j<=Ny-3)
	{
	  auto indx = GetWordIndices(i-1,j+1);
	  bool flag = true;
	  for(auto& it:indx)
	    {
	      assert(it.first>=0 && it.first<Nx && it.second>=0 && it.second<Ny);
	      if(layout[it.first][it.second]==false)
		flag = false;
	    }
	  return flag;
	}
      else
	return false;
    }

    //! Access the location of the neighbor to the North-East
    virtual std::pair<int,int> Get_NE_Neighbor(int i, int j) const override final
    {
      assert(i>=0 && i<Nx && j>=0 && j<Ny);
      assert(Has_NE_Neighbor_Word(i,j));
      return std::make_pair(i-1,j+1);
    }

    
  };
}

#endif
