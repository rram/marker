// Sriramajayam

#include <mkr_MobiusRectangle.h>
#include <iostream>

namespace mkr
{
  // Access the indices defining a word
  std::array<std::pair<int,int>,5> MobiusRectangle::GetWordIndices(int i, int j) const 
  {
    const int ny = Ny/2;
    assert(i>=0 && i<Nx && j>=0 && j<Ny);
    assert(HasWord(i,j));
    if(j==0) // left edge of the front side
      {
	// W neighbor is special
	return std::array<std::pair<int,int>,5>{
	  std::make_pair(i,j),
	    std::make_pair(i,j+1),
	    std::make_pair(i-1,j),
	    std::make_pair(Nx-1-i,Ny-1),
	    std::make_pair(i+1,j)
	    };
      }
    else if(j==ny-1) // right edge of the front side
      {
	// E neighbor is special
	return std::array<std::pair<int,int>,5>{
	  std::make_pair(i,j),
	    std::make_pair(Nx-1-i,ny),
	    std::make_pair(i-1,j),
	    std::make_pair(i,j-1),
	    std::make_pair(i+1,j)
	    };
      }
    else if(j==ny) // left edge of the reverse side
      {
	// W neighbor is special
	return std::array<std::pair<int,int>,5>{
	  std::make_pair(i,j),
	    std::make_pair(i,j+1),
	    std::make_pair(i-1,j),
	    std::make_pair(Nx-1-i,ny-1),
	    std::make_pair(i+1,j)
	    };
      }
    else if(j==Ny-1)
      {
	// E neighbor is special
	return std::array<std::pair<int,int>,5>{
	  std::make_pair(i,j),
	    std::make_pair(Nx-1-i,0),
	    std::make_pair(i-1,j),
	    std::make_pair(i,j-1),
	    std::make_pair(i+1,j)
	    };
      }
    else
      return std::array<std::pair<int,int>,5>{
	std::make_pair(i,j),
	  std::make_pair(i,j+1),
	  std::make_pair(i-1,j),
	  std::make_pair(i,j-1),
	  std::make_pair(i+1,j)
	  };
  }


  // Main functionality: compute the coloring
  void MobiusRectangle::Compute(int nIters)
  {
    assert(isCreated==false);
    const int ny = Ny/2;
    const int dict_size_guess = dict_size;
    int min_dict_size = -1;
    std::vector<std::vector<int>> min_coloring{};
    WordListType min_used_words{};
 
    for(int iter=0; iter<nIters; ++iter)
      {
	// Start with the initial guess for the alphabet size
	dict_size = dict_size_guess;

	bool success = false;
	while(success==false)
	  {
	    // Compute a 2-sided coloring without concern for interfaces
	    TwoSideRectangle rect(Nx, Ny, word_equivalence, dict_size, num_bits);
	    rect.Compute(1);
	    coloring.clear();
	    coloring = rect.GetColoring();
	    used_words.clear();
	    used_words = rect.GetWords();
	    assert(static_cast<int>(used_words.size())==2*(Nx-2)*(ny-2));
	    dict_size = rect.GetAlphabetSize();
	    
	    // Examine words along columns 0, ny-1, ny and Ny-1
	    success = true;
	    const std::vector<int> interface_columns{0, ny-1, ny, Ny-1};
	    for(int k=0; k<4 && success==true; ++k)
	      {
		const int j = interface_columns[k];
		for(int i=0; i<Nx && success==true; ++i)
		  if(HasWord(i,j))
		    {
		      if(IsNewWord(GetWord(i,j)))
			used_words.insert(GetWord(i,j));
		      else
			success = false;
		    }
	      }
	    
	    // If successful, we have a coloring
	    // Otherwise, it is necessary to increase the dictionary size
	    if(success==false)
	      ++dict_size;
	    else
	      assert(static_cast<int>(used_words.size())==(Nx-2)*Ny);
	  }

	// Verify correctness of the computed coloring
	VerifyCorrectness();
	
	// Save this alphabet if it is the best one yet
	if(min_dict_size==-1 || dict_size<min_dict_size)
	  {
	    min_dict_size = dict_size;
	    min_coloring  = coloring;
	    min_used_words = used_words;
	  }
      }

    // Save the best dictionary
    dict_size  = min_dict_size;
    coloring   = min_coloring;
    used_words = min_used_words;
    isCreated = true;

    return;
  }

}
