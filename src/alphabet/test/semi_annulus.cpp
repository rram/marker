// Sriramajayam

#include <mkr_OneSideLayout.h>
#include <mkr_EncodedSurface.h>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include "./CLI11.hpp"
#include <iostream>

// List of options
// ./semi_annulus --help

// To generate
// ./semi_annulus -g -x 20 -y 40 -i 10.0 -o 15. -b 3 -m 60 -w 8 -c coloring.xml -f expt.xml

// To execute
// ./semi_annulus -e -f expt.xml -s stereo.xml -z cloud.xyz

int main(int argc, char** argv)
{
  // Layout dimensions
  int Nx = 0;
  int Ny = 0;
  int nbits = 0;
  
  // Dimensions of the semi-annulus
  double r_in = -1., r_out = -1.;

  // Marker size and gap (in pixels)
  int markerSize, gapSize;
  
  // Frozen options
  const int word_dist = 1;
  const int dict_size_guess = 2;
  const int nIters = 100;

  // XML file
  std::string coloring_xmlfile{};
  std::string expt_xmlfile{};
  std::string stereo_xmlfile{};
  std::string cloud_xyzfile{};
  
  // Read commandline options
  CLI::App app{"generate pattern and process data for a semi annular ribbon"};
  bool is_generate = false;
  bool is_execute  = false;
  app.add_flag("-g", is_generate, "generate coloring");
  app.add_flag("-e", is_execute,  "execute calculations for an experiment");
  auto optX = app.add_option("-x", Nx, "number of rows \t\t (with -g)");
  auto optY = app.add_option("-y", Ny, "number of columns \t (with -g)");
  auto optI = app.add_option("-i", r_in, "inner radius \t\t (with -g)");
  auto optO = app.add_option("-o", r_out, "outer radius \t\t (with -g)");
  auto optB = app.add_option("-b", nbits, "num bits \t\t\t (with -g)");
  auto optM = app.add_option("-m", markerSize, "marker size in pixels \t (with -g)");
  auto optW = app.add_option("-w", gapSize, "gap size in pixels \t (with -g)");
  auto optC = app.add_option("-c", coloring_xmlfile, "coloring xml filename \t (with -g)");
  auto optF = app.add_option("-f", expt_xmlfile, "experiment xml filename \t\t (template written with -g, read with -e)");
  auto optS = app.add_option("-s", stereo_xmlfile, "calibration stereo xml filename \t (with -e)");
  auto optZ = app.add_option("-z", cloud_xyzfile, "3D cloud xyz filename \t\t (with -e)");
  CLI11_PARSE(app, argc, argv);
  assert( (is_generate || is_execute) && !(is_generate && is_execute) ); // either generate or execute
  
  // Generation
  if(is_generate)
    {
      // check options
      optX->required()->check(CLI::PositiveNumber);
      optY->required()->check(CLI::PositiveNumber);
      optI->required()->check(CLI::PositiveNumber);
      optO->required()->check(CLI::PositiveNumber);
      optB->required()->check(CLI::PositiveNumber);
      optM->required()->check(CLI::PositiveNumber);
      optW->required()->check(CLI::PositiveNumber);
      optC->required()->check(!CLI::ExistingFile);
      optF->required()->check(!CLI::ExistingFile);
      optS->required(false);
      optZ->required(false);
      app.parse(argc, argv);
      assert(r_out>r_in &&  (nbits==3 || nbits==4));
      
      // layout for a 1-sided annulus
      const double center[] = {static_cast<double>(Nx/2), static_cast<double>(Ny/2)};
      std::vector<std::vector<bool>> layout(Nx, std::vector<bool>(Ny, false));
      for(int i=0; i<Nx; ++i)
	for(int j=0; j<=Ny/2; ++j)
	  {
	    const double r = std::sqrt((i-center[0])*(i-center[0])+(j-center[1])*(j-center[1]));
	    if(r>=r_in && r<=r_out)
	      layout[i][j] = true;
	  }
      
      // print the layout
      std::cout<<"\nLAYOUT: "<<std::endl;
      for(int i=0; i<Nx; ++i)
	{
	  std::cout<<std::endl;
	  for(int j=0; j<Ny; ++j)
	    std::cout<<layout[i][j]<<" ";
	}
      
      // Compute coloring
      mkr::OneSideLayout semi_ann(layout, word_dist, dict_size_guess, nbits);
      semi_ann.Compute(nIters);
      semi_ann.Save(coloring_xmlfile);
      int num_encoded, num_unencoded;
      semi_ann.Count(num_encoded, num_unencoded);
      
      // Add details of this generation to the xml file
      cv::FileStorage fs;
      fs.open(coloring_xmlfile, cv::FileStorage::APPEND);
      CV_Assert(fs.isOpened());
      fs.writeComment((char*)"Inner radius", 0);
      fs<<"rin"<<r_in;
      fs.writeComment((char*)"Outer radius", 0);
      fs<<"rout"<<r_out;
      fs.writeComment((char*)"num encoded locations", 0);
      fs<<"encoded"<<num_encoded;
      fs.writeComment((char*)"num  unencoded locations", 0);
      fs<<"unencoded"<<num_unencoded;
      fs.writeComment((char*)"marker size", 0);
      fs<<"markersize"<<markerSize;
      fs.writeComment((char*)"gap size", 0);
      fs<<"gapsize"<<gapSize;
      fs.release();

      // Paint the markers
      mkr::Crossword cword(coloring_xmlfile);
      cv::Mat_<uchar> mat(Nx*(markerSize+gapSize)+5*gapSize, Ny*(markerSize+gapSize)+5*gapSize);
      cword.Paint(markerSize, gapSize, std::make_pair(gapSize,gapSize), 0, mat);

      // draw inner and outer circumference
      //cv::circle(mat, cv::Point2d(center[0]*(markerSize+gapSize)+gapSize+0.5*markerSize, center[1]*(markerSize+gapSize)+gapSize+0.5*markerSize),
      //	 static_cast<int>(r_in*(markerSize+gapSize)-markerSize),  cv::Scalar(0,0,0));
      //cv::circle(mat, cv::Point2d(center[0]*(markerSize+gapSize)+gapSize+0.5*markerSize, center[1]*(markerSize+gapSize)+gapSize+0.5*markerSize),
      //	 static_cast<int>(r_out*(markerSize+gapSize)+0.5*markerSize), cv::Scalar(0,0,0));
      cv::imwrite("semi-annulus-layout.jpg", mat);
      
      // Generate a template experiment file
      mkr::EncodedSurface::CreateTemplate(expt_xmlfile);
    }
  else // execute
    {
      // check options
      optX->required(false);
      optY->required(false);
      optI->required(false);
      optO->required(false);
      optB->required(false);
      optM->required(false);
      optW->required(false);
      optC->required(false);
      optF->required()->check(CLI::ExistingFile);
      optS->required()->check(CLI::ExistingFile);
      optZ->required()->check(!CLI::ExistingFile);

      // encoded surface
      mkr::EncodedSurface surf(expt_xmlfile);
      
      // triangulator
      str::StereoRig srig(stereo_xmlfile);
      str::Triangulator tri(srig);

      // triangulate and merge poses
      surf.Triangulate(tri);

      // merged pose
      auto& merged_pose = surf.GetMergedPose();
      std::cout<< std::endl << "Number of markers encoded: "<<merged_pose.key2coords.size();

      // write to file
      std::fstream stream;
      stream.open(cloud_xyzfile.c_str(), std::ios::out);
      CV_Assert(stream.good());
      for(auto& it:merged_pose.key2coords)
	for(int c=0; c<4; ++c)
	  stream << it.second[c].x<<" "<<it.second[c].y<<" "<<it.second[c].z<< std::endl;
      stream.close();
    }

  // done
}
