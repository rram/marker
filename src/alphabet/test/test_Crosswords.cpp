// Sriramajayam

#include <iostream>
#include <random>
#include <mkr_OneSideRectangle.h>
#include <mkr_TwoSideRectangle.h>
#include <mkr_MobiusRectangle.h>
#include <mkr_OneSideLayout.h>
#include <mkr_TwoSideLayout.h>
#include <mkr_Crossword.h>
#include <opencv2/imgcodecs.hpp>

void TestCrossword(const mkr::Crossword& cword);

int main()
{
  // Random parameter choices
  std::random_device rd;
  std::mt19937 mt(rd());
  std::uniform_int_distribution<int> rand_dist(1, 2);  // word-distance
  std::uniform_int_distribution<int> rand_rows(5, 15); // Nx
  std::uniform_int_distribution<int> rand_cols(5, 15);   // Ny
  std::uniform_int_distribution<int> rand_bits(3,5);   // nbits
    
  const int Nx = rand_rows(mt);
  const int ny = rand_cols(mt);  
  const int word_dist = rand_dist(mt);
  const int nbits = rand_bits(mt);
  const int dict_size_guess = 2;
  const int nIters = 20;

  std::cout<< std::endl << "Board dimensions: "<<Nx<<" x "<<ny
	   << std::endl << "Word distance: "<<word_dist
	   << std::endl << "Num bits: "<<nbits
	   << std::endl;
  
  // One side rectange
  {
    std::cout<< std::endl << "Testing crossword for one-sided rectangle" << std::endl;
    const int Ny = ny;
    mkr::OneSideRectangle alph(Nx, Ny, word_dist, dict_size_guess, nbits);
    alph.Compute(nIters);
    alph.Save("coloring.xml");

    mkr::Crossword cword("coloring.xml");
    TestCrossword(cword);
  }

  // One side layout: circle
  {
    std::cout<< std::endl << "\nTesting crossword for one-sided circular layout" << std::endl;

    const int Ny = ny;
    const double center[] = {static_cast<double>(Nx/2), static_cast<double>(Ny/2)};
    const double rad = (Nx<Ny) ? Nx/3. : Ny/3.;
    std::vector<std::vector<bool>> layout(Nx, std::vector<bool>(Ny, false));
    for(int i=0; i<Nx; ++i)
      for(int j=0; j<Ny; ++j)
	if(std::sqrt((i-center[0])*(i-center[0])+(j-center[1])*(j-center[1]))<rad)
	  layout[i][j] = true;
    
    mkr::OneSideLayout alph(layout, word_dist, dict_size_guess, nbits);
    alph.Compute(nIters);
    alph.Save("coloring.xml");

    mkr::Crossword cword("coloring.xml");
    TestCrossword(cword);
  }
}


void TestCrossword(const mkr::Crossword& cword)
{
  const auto& coloring = cword.GetColoring();
  const int Nx = static_cast<int>(coloring.size());
  const int Ny = static_cast<int>(coloring[0].size());
  const int nsides = cword.GetNumSides();
  
  // Paint a crossword
  const int marker_size = 100;
  const int gap = 5;
  cv::Mat_<uchar> mat(Nx*(marker_size+gap)+5*gap, Ny*(marker_size+gap)+5*gap);
  for(int s=0; s<nsides; ++s)
    cword.Paint(marker_size, gap, std::pair<int,int>(gap,gap), s, mat);
  cv::imwrite("crossword.jpg", mat);
  
  // Detect mwords
  std::map<mkr::Mkey, mkr::Mcoords2D> key2coords;
  cword.Detect("crossword.jpg", key2coords);

  // Check that all m-words have been detected
  std::set<int> detected_mwords{};
  for(auto& it:key2coords)
    detected_mwords.insert(std::get<0>(it.first));
  CV_Assert(detected_mwords.size()==cword.GetWords().size());
}
