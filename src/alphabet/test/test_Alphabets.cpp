// Sriramajayam

#include <random>
#include <iostream>
#include <mkr_OneSideRectangle.h>
#include <mkr_TwoSideRectangle.h>
#include <mkr_MobiusRectangle.h>
#include <mkr_OneSideLayout.h>
#include <mkr_TwoSideLayout.h>

void TestCopy(const mkr::Alphabet& alph, const mkr::Alphabet& copy);

int main()
{
  // Random parameter choices
  std::random_device rd;
  std::mt19937 mt(rd());
  std::uniform_int_distribution<int> rand_dist(1, 2);  // word-distance
  std::uniform_int_distribution<int> rand_rows(5, 15); // Nx
  std::uniform_int_distribution<int> rand_cols(5, 15);   // Ny
  std::uniform_int_distribution<int> rand_bits(3,5);   // nbits
    
  const int Nx = rand_rows(mt);
  const int ny = rand_cols(mt);  
  const int word_dist = rand_dist(mt);
  const int nbits = rand_bits(mt);
  const int dict_size_guess = 2;
  const int nIters = 20;

  std::cout<< std::endl << "Board dimensions: "<<Nx<<" x "<<ny
	   << std::endl << "Word distance: "<<word_dist
	   << std::endl << "Num bits: "<<nbits
	   << std::endl;
  
  // One side rectange
  {
    std::cout<< std::endl << "Testing one-sided rectangle" << std::endl;
    const int Ny = ny;
    mkr::OneSideRectangle alph(Nx, Ny, word_dist, dict_size_guess, nbits);
    alph.Compute(nIters);

    assert(static_cast<int>(alph.GetWords().size())==(Nx-2)*(Ny-2));
    int encoded, unencoded;
    alph.Count(encoded, unencoded);
    assert(unencoded==4 && encoded==Nx*Ny-4);
    
    alph.Save("coloring.xml");
    decltype(alph) copy("coloring.xml");
    TestCopy(alph, copy);
  }

  // Two side rectangle
  {
    std::cout<< std::endl << "Testing two-sided rectangle" << std::endl;
    
    const int Ny = ny*2;
    mkr::TwoSideRectangle alph(Nx, Ny, word_dist, dict_size_guess, nbits);
    alph.Compute(nIters);

    assert(static_cast<int>(alph.GetWords().size())==2*(Nx-2)*(Ny/2-2));
    int encoded, unencoded;
    alph.Count(encoded, unencoded);
    assert(unencoded==8 && encoded==Nx*Ny-8);
    
    alph.Save("coloring.xml");
    decltype(alph) copy("coloring.xml");
    TestCopy(alph, copy);
  }

  
  // Mobius
  {
    std::cout<< std::endl << "Testing two-sided Mobius" << std::endl;
	
    const int Ny = ny*2;
    mkr::MobiusRectangle alph(Nx, Ny, word_dist, dict_size_guess, nbits);
    alph.Compute(nIters);

    assert(static_cast<int>(alph.GetWords().size())==(Nx-2)*Ny);
    int encoded, unencoded;
    alph.Count(encoded, unencoded);
    assert(unencoded==0 && encoded==Nx*Ny);

    alph.Save("coloring.xml");
    decltype(alph) copy("coloring.xml");
    TestCopy(alph, copy);
  }

  // One side layout: circle
  {
    std::cout<< std::endl << "\nTesting one-sided circular layout" << std::endl;

    const int Ny = ny;
    const double center[] = {static_cast<double>(Nx/2), static_cast<double>(Ny/2)};
    const double rad = (Nx<Ny) ? Nx/3. : Ny/3.;
    std::vector<std::vector<bool>> layout(Nx, std::vector<bool>(Ny, false));
    for(int i=0; i<Nx; ++i)
      for(int j=0; j<Ny; ++j)
	if(std::sqrt((i-center[0])*(i-center[0])+(j-center[1])*(j-center[1]))<rad)
	  layout[i][j] = true;
    
    // print the layout
    std::cout<<"\nLAYOUT: ";
    for(int i=0; i<Nx; ++i)
      {
	std::cout<<std::endl;
	for(int j=0; j<Ny; ++j)
	  std::cout<<layout[i][j]<<" ";
      }
    
    mkr::OneSideLayout alph(layout, word_dist, dict_size_guess, nbits);
    alph.Compute(nIters);
    alph.Save("coloring.xml");
    decltype(alph) copy("coloring.xml");
    TestCopy(alph, copy);
  }
  

  // Two sided layout: circle
  {
    std::cout<< std::endl << "\nTesting two-sided circular layout" << std::endl;

    const int Ny = 2*ny;
    const double center[] = {static_cast<double>(Nx/2), static_cast<double>(ny/2)};
    const double rad = (Nx<ny) ? Nx/3. : ny/3.;
    std::vector<std::vector<bool>> layout(Nx, std::vector<bool>(Ny, false));
    for(int i=0; i<Nx; ++i)
      for(int J=0; J<Ny; ++J)
	{
	  const int j = (J>=Ny/2) ? (J-Ny/2) : J;
	  if(std::sqrt((i-center[0])*(i-center[0])+(j-center[1])*(j-center[1]))<rad)
	    layout[i][J] = true;
	}

    // print the layout
    std::cout<<"\nLAYOUT: ";
    std::cout<<"\nFront: ";
    for(int i=0; i<Nx; ++i)
      {
	std::cout<<std::endl;
	for(int j=0; j<Ny/2; ++j)
	  std::cout<<layout[i][j]<<" ";
      }
    std::cout<<"\nRear: "<<std::endl;
    for(int i=0; i<Nx; ++i)
      {
	std::cout<<std::endl;
	for(int j=Ny/2; j<Ny; ++j)
	  std::cout<<layout[i][j]<<" ";
      }
    
    mkr::TwoSideLayout alph(layout, word_dist, dict_size_guess, nbits);
    alph.Compute(nIters);
    alph.Save("coloring.xml");
    decltype(alph) copy("coloring.xml");
    TestCopy(alph, copy);
  }
  
  // done
}


void TestCopy(const mkr::Alphabet& alph, const mkr::Alphabet& copy)
{
  assert(alph.GetAlphabetSize()==copy.GetAlphabetSize());
  assert(alph.GetColoring().size()==copy.GetColoring().size());
  assert(alph.GetColoring()[0].size()==copy.GetColoring()[0].size());
  assert(alph.GetColoring()==copy.GetColoring());
  assert(alph.GetNumBits()==copy.GetNumBits());
  assert(alph.GetInterwordDistance()==copy.GetInterwordDistance());
  assert(alph.GetWords().size()==copy.GetWords().size());
  assert(alph.GetWords()==copy.GetWords());
  assert(alph.GetLayout()==copy.GetLayout());
  assert(alph.GetNumSides()==copy.GetNumSides());
  
  int encoded, unencoded;
  alph.Count(encoded, unencoded);
  int copy_encoded, copy_unencoded;
  copy.Count(copy_encoded, copy_unencoded);
  assert(encoded==copy_encoded && unencoded==copy_unencoded);
   
  // histogram
  auto histogram = alph.GetInterwordDistanceHistogram();
  for(int i=0; i<=alph.GetInterwordDistance(); ++i)
    assert(histogram[i]==0);

  return;
}
