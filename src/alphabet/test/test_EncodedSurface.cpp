// Sriramajayam

#include <mkr_EncodedSurface.h>

int main()
{
  //mkr::EncodedSurface::CreateTemplate("template.xml");

  // Alphabet
  mkr::OneSideRectangle rect;
  rect.Read("rect-1side.xml");

  // Crossword
  mkr::Crossword cword(rect, 3);

  // triangulator

  
  // Encoded surface
  mkr::EncodedSurface("experiment.xml", cword, tri);
}
