// Sriramajayam

#ifndef MKR_ENCODED_SURFACE_H
#define MKR_ENCODED_SURFACE_H

#include <str_Triangulator.h>
#include <mkr_Crossword.h>
#include <fstream>

namespace mkr
{
  struct EncodedPose
  {
    cv::String left_img;
    cv::String right_img;
    std::map<Mkey, Mcoords2D> left_key2coords;
    std::map<Mkey, Mcoords2D> right_key2coords;
    std::map<Mkey, Mcoords3D> key2coords;
    inline EncodedPose()
      :left_img(""), right_img(""), left_key2coords{}, right_key2coords{}, key2coords{} {}
    void PaintWords(const std::map<int,cv::Scalar>& colors) const; // paints the list of common words
    friend std::ostream& operator<<(std::ostream&, const EncodedPose&);
  };


  struct Transformation
  {
    std::array<std::array<double,3>,3> Rot;
    std::array<double,3> Tvec;
    inline cv::Point3d Apply(const cv::Point3d& A)
    {
      cv::Point3d B;
      B.x = Rot[0][0]*A.x + Rot[0][1]*A.y + Rot[0][2]*A.z + Tvec[0];
      B.y = Rot[1][0]*A.x + Rot[1][1]*A.y + Rot[1][2]*A.z + Tvec[1];
      B.z = Rot[2][0]*A.x + Rot[2][1]*A.y + Rot[2][2]*A.z + Tvec[2];
      return B;
    }
  };
  
  struct MergedPose
  {
    std::vector<int> sequence;
    std::vector<std::set<Mkey>> commonKeys;
    std::vector<Transformation> transformations;
    std::map<Mkey, Mcoords3D>   key2coords;
    inline MergedPose()
      :sequence{}, commonKeys{}, transformations{}, key2coords{} {}
    friend std::ostream& operator<<(std::ostream&, const MergedPose&);
  };

 
  
  class EncodedSurface
  {
  public:
    //! Template XML file
    static void CreateTemplate(const cv::String xmlfile);

    //! Constructor
    EncodedSurface(const cv::String experiment_xml);

    //! Destructor
    inline virtual ~EncodedSurface() {}

    //! Disable copy and assignment
    EncodedSurface(const EncodedSurface&) = delete;
    EncodedSurface operator=(const EncodedSurface&) = delete;

    //! Triangulate and merge poses
    void Triangulate(const str::Triangulator& tri);
    
    //! Returns the number of poses
    inline int GetNumPoses() const
    { return numPoses; }
    
    //! Access a specific pose
    inline const EncodedPose& GetPose(const int pose_num) const
    {
      assert(isProcessed==true);
      CV_Assert(pose_num<numPoses);
      return poses[pose_num];
    }

    //! Access the merged pose
    inline const MergedPose& GetMergedPose() const
    {
      assert(isProcessed==true);
      return mergedPose;
    }
    
  private:

    static void ReadXML(const cv::String xmlfile,
			cv::String& coloring_xmlfile,
			std::vector<cv::String>& left_images,
			std::vector<cv::String>& right_images,
			std::vector<int>& merge_sequence);

    static void TriangulatePose(const str::Triangulator& tri, EncodedPose& pose);

    // Merge one pose to another
    static void MergePoses(const std::map<Mkey,Mcoords3D>& src,
			   std::map<Mkey,Mcoords3D>& tgt,
			   Transformation& transformation,
			   std::set<Mkey>& commonKeys);

    bool isProcessed;
    int numPoses;
    std::vector<EncodedPose> poses;
    MergedPose mergedPose;
  };

}

#endif
