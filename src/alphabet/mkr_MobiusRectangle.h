// Sriramajayam

#ifndef MKR_MOBIUS_RECTANGLE_H
#define MKR_MOBIUS_RECTANGLE_H

#include <mkr_TwoSideRectangle.h>

namespace mkr
{
  class MobiusRectangle: public Alphabet
  {
  public:
    //! Constructor
    inline MobiusRectangle(const std::string xmlfile)
      :Alphabet(xmlfile)
    {
      // Verify correctness
      VerifyCorrectness();
    }

    //! Constructor
    inline MobiusRectangle(const int rows, const int cols,
			   const int word_dist, int guess_alphabet_size, int nbits)
      :Alphabet(rows, cols, word_dist, guess_alphabet_size, nbits)
    {
      // require even number of columns
      // front:   cols 0 to Ny/2-1
      // reverse: cols Ny/2 to Ny
      assert(cols%2==0);
    }

    inline virtual ~MobiusRectangle() {}
  
    //! Disable copy and assignment
    MobiusRectangle(const MobiusRectangle&) = delete;
    MobiusRectangle operator=(const MobiusRectangle&) = delete;

    //! Main functionality: compute the coloring
    void Compute(int nIters) override;

    //! Number of sides being colored
    inline int GetNumSides() const override
    { return 2; }
    
  protected:

    //! Check if a word exists at a given location
    inline bool HasWord(int i, int j) const override
    {
      assert(i>=0 && i<Nx && j>=0 && j<Ny);
      return (i-1>=0 && i+1<Nx);
    }
    
    //! Access the indices defining a word
    std::array<std::pair<int,int>,5> GetWordIndices(int i, int j) const override;
    
    //! Check if there is a word defined for the neighbor to the North-East
    inline bool Has_NE_Neighbor_Word(int i, int j) const override 
    {
      assert(false && "Should not be here");
      return false;
    }

    //! Access the location of the neighbor to the North-East
    inline std::pair<int,int> Get_NE_Neighbor(int i, int j) const override
    {
      assert(false && "Should not be here");
      return std::make_pair(-1,-1);
    }
    
    // Attempt to revise the coloring
    inline bool ReviseColoring(const int i, const int j) override
    {
      assert(false && "Should not be here");
      return false;
    }
  };
}

#endif
