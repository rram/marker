// Sriramajayam

#ifndef MKR_ONE_SIDE_RECTANGLE_H
#define MKR_ONE_SIDE_RECTANGLE_H

#include <mkr_Alphabet.h>

namespace mkr
{
  class OneSideRectangle: public Alphabet
  {
  public:
    //! Constructor
    inline OneSideRectangle(const std::string xmlfile)
      :Alphabet(xmlfile)
    {
      // Verify correctness
      VerifyCorrectness();
    }
    
    //! Constructor
    inline OneSideRectangle(const int nx, const int ny, const int word_dist,
			    int guess_alphabet_size, int nbits)
      :Alphabet(nx, ny, word_dist, guess_alphabet_size, nbits) {}

    //! Destructor
    inline virtual ~OneSideRectangle() {}

    //! Disable copy and assignment
    OneSideRectangle(const OneSideRectangle&) = delete;
    OneSideRectangle operator=(const OneSideRectangle&) = delete;

    //! Number of sides being colored
    inline int GetNumSides() const override final
    { return 1; }
    
  protected:
    
    //! Check if a word exists at a given location
    inline bool HasWord(int i, int j) const override final
    {
      assert(i>=0 && i<Nx && j>=0 && j<Ny);
      return (i-1>=0 && i+1<Nx && j-1>=0 && j+1<Ny);
    }

    //! Access the word at a given location
    std::array<std::pair<int,int>,5> GetWordIndices(int i, int j) const override final
      {
	assert(i>=0 && i<Nx && j>=0 && j<Ny);
	return std::array<std::pair<int,int>,5>{
	  std::make_pair(i,j),
	    std::make_pair(i,j+1),
	    std::make_pair(i-1,j),
	    std::make_pair(i,j-1),
	    std::make_pair(i+1,j)};
      }

    //! Check if there is a word defined at the neighbor to the North-East
    bool Has_NE_Neighbor_Word(int i, int j) const final
    {
      assert(i>=0 && i<Nx && j>=0 && j<Ny);
      return (i>=2 && j<=Ny-3);
    }
    
    //! Access the location of the neighbor to the North-East
    virtual std::pair<int,int> Get_NE_Neighbor(int i, int j) const override final
    {
      assert(i>=0 && i<Nx && j>=0 && j<Ny);
      assert(Has_NE_Neighbor_Word(i,j));
      return std::make_pair(i-1,j+1);
    }
    
  };
}

#endif
