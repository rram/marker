// Sriramajayam

#include <mkr_EncodedSurface.h>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

namespace mkr
{
  void EncodedPose::PaintWords(const std::map<int,cv::Scalar>& colors) const
  {
    CV_Assert(!left_img.empty() && !right_img.empty());
    CV_Assert(!left_key2coords.empty() && !right_key2coords.empty());

    // List of common keys
    std::set<Mkey> left_keys{}, right_keys{};
    for(auto& it:left_key2coords)  left_keys.insert(it.first);
    for(auto& it:right_key2coords) right_keys.insert(it.first);

    std::vector<Mkey> common_keys{};
    std::set_intersection(left_keys.begin(),  left_keys.end(),
			  right_keys.begin(), right_keys.end(),
			  std::back_inserter(common_keys));

    std::map<Mkey, Mcoords2D> common_left_key2coords{}, common_right_key2coords{};
    for(auto& mkey:common_keys)
      {
	// left
	auto it = left_key2coords.find(mkey);
	CV_Assert(it!=left_key2coords.end());
	common_left_key2coords[it->first] = it->second;

	// right
	auto jt = right_key2coords.find(mkey);
	CV_Assert(jt!=right_key2coords.end());
	common_right_key2coords[jt->first] = jt->second;
      }

    
    // Left
    Crossword::PaintWords(colors, left_img, common_left_key2coords);

    // Right
    Crossword::PaintWords(colors, right_img, common_right_key2coords);
    
    // done
    return;
  }


  std::ostream& operator<<(std::ostream& stream, const EncodedPose& pose)
  {
    CV_Assert(stream.good());
    for(auto& it:pose.key2coords)
      for(int k=0; k<4; ++k)
	stream << it.second[k].x <<" "<<it.second[k].y <<" "<<it.second[k].z << std::endl;
    return stream;
  }

  std::ostream& operator<<(std::ostream& stream, const MergedPose& pose)
  {
    CV_Assert(stream.good());
    for(auto& it:pose.key2coords)
      for(int k=0; k<4; ++k)
	stream << it.second[k].x <<" "<<it.second[k].y <<" "<<it.second[k].z << std::endl;
    return stream;
  }
  
}
