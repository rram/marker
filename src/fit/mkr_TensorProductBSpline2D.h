// Sriramajayam

#ifndef MKR_TENSOR_PRODUCT_BSPLINE_2D_H
#define MKR_TENSOR_PRODUCT_BSPLINE_2D_H

#include <mkr_BSpline1D.h>

namespace mkr
{
  //! Class for tensor product b-spline basis functions
  template<int DEGREE>
    class TensorProductBSpline2D
    {
    public:
      //! Constructor
      //! \param kv1 Knot vector in the u direction, copied
      //! \param kv2 Knot vector in the v direction, copied
      TensorProductBSpline2D(const std::vector<double>&kv1,
			     const std::vector<double>& kv2);

      //! Destructor
      virtual ~TensorProductBSpline2D();

      //! Copy constructor, disabled
      TensorProductBSpline2D(const TensorProductBSpline2D<DEGREE>&) = delete;

      //! Returns the total number of basis functions
      int GetNumberOfFunctions() const;

      //! Returns the number of parametric variables
      int GetNumberOfVariables() const;
  
      //! Returns the degree of the splines
      int GetDegreeOfSpline() const;
  
      //! Returns the number of basis functions per patch
      int GetNumberOfFunctionsPerPatch() const;
  
      //! Returns the number of patches
      int GetNumberOfPatches() const;

      //! Returns the limits of a requested patch
      //! \param intnum Interval number whose limits to return
      //! \param uint Returned u-interval
      //! \param vint Returned v-interval
      void GetPatchIntervals(const int pnum, std::pair<double,double>& uint, std::pair<double,double>& vint) const;
  
      //! Returns the indices of active basis functions in a given patch
      //! \param patchnum Patch number 
      void GetActiveFunctions(const int patchnum, std::vector<int>& indx) const;
  
      //! Computes nonzero basis functions at given parametric coordinate and returns the
      //! basis function numbers
      //! \param xi Parametric coordinates
      //! \param vals Computed list of values for basis functions
      //! \param shpnum Numbers of nonzero basis functions
      void GetNonzeroFunctions(const double* xi, 
			       std::vector<int>& shpnum, 
			       std::vector<double>& vals) const;
  
      //! Computes derivatives wrt parameteric coordinates of 
      //! nonzero basis functions at given parametric coordinate and 
      //! returns the basis function numbers
      //! \param xi Parametric coordinates
      //! \param dvals Computed list of values for basis functions
      //! \param shpnum Numbers of nonzero basis functions
      void GetNonzeroDFunctions(const double* xi, 
				std::vector<int>& shpnum, 
				std::vector<std::vector<double>>& dvals) const;
  
      //! Computes 2nd derivatives wrt parametric coordinates of nonzero basis functions 
      //! at given parametric coordinate and 
      //! returns the basis function numbers
      //! \param xi Parametric coordinates
      //! \param d2vals Computed list of values for basis functions
      //! \param shpnum Numbers of nonzero basis functions
      void GetNonzeroD2Functions(const double* xi, 
				 std::vector<int>& shpnum, 
				 std::vector<std::vector<std::vector<double>>>& d2vals) const;

    protected:

      //! Maps interval number to patch number
      int IntervalToPatchMap(const int a, const int b) const;
      
      //! Maps patch number to pair of interval numbers
      //! \param patchnum Input patchnumber
      //! \param intX Computed interval number along first direction
      //! \param intY Computed interval number along second direction
      void PatchToIntervalMap(const  int patchnum, int &intX, int &intY) const;
  
  
      const BSpline1D<DEGREE> uShp;
      const BSpline1D<DEGREE> vShp;
      mutable std::vector<int> uShpIndx, vShpIndx;
      mutable std::vector<double> uShpVals, duShpVals, d2uShpVals;
      mutable std::vector<double> vShpVals, dvShpVals, d2vShpVals;
    };

  // Specializations
  using LinearBSpline2D    = TensorProductBSpline2D<1>;
  using QuadraticBSpline2D = TensorProductBSpline2D<2>;
  using CubicBSpline2D     = TensorProductBSpline2D<3>;
  using QuarticBSpline2D   = TensorProductBSpline2D<4>;
}

#endif

#include <mkr_TensorProductBSpline2D_impl.h>
