// Sriramajayam

#include <mkr_FitData.h>
#include <iostream>
#include <random>

int main(int argc, char** argv)
{
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Spline
  std::vector<double> kv1{}, kv2{};
  for(int i=0; i<10; ++i)
    {
      kv1.push_back( static_cast<double>(i) );
      kv2.push_back( std::sqrt(static_cast<double>(i)) );
    }
  kv2.push_back( std::sqrt(10.) );
  kv2.push_back( std::sqrt(11.) );
  mkr::CubicBSpline2D spline(kv1, kv2);

  // Interpolation data
  std::pair<double,double> uInt, vInt;
  spline.GetPatchIntervals(0, uInt, vInt);
  const double uMin = uInt.first;
  const double vMin = vInt.first;
  spline.GetPatchIntervals(spline.GetNumberOfPatches()-1, uInt, vInt);
  const double uMax = uInt.second;
  const double vMax = vInt.second;

    // Random data
  const int nPoints = 50;
  std::vector<mkr::CoordNormalPair> shape_data(nPoints);
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-1., 1.);
  const double gamma = 4.+dis(gen);
  
  // R-tree
  mkr::FitData::boost_rtree rtree;
  for(int p=0; p<nPoints; ++p)
    {
      double xyz[] = {dis(gen), dis(gen), dis(gen)};
      double norm = std::sqrt(xyz[0]*xyz[0] + xyz[1]*xyz[1] + xyz[2]*xyz[2]);
      xyz[0] /= norm;
      xyz[1] /= norm;
      xyz[2] /= norm;
      shape_data[p] = std::make_pair(std::array<double,3>{2.*xyz[0],2.*xyz[1],2.*xyz[2]},
				     std::array<double,3>{xyz[0],xyz[1],xyz[2]});
      rtree.insert(std::make_pair(mkr::FitData::boost_point(2.*xyz[0],2.*xyz[1],2.*xyz[2]), p));
    }

    // PETSc data structure
  const int ndofs = spline.GetNumberOfFunctions();
  const int ndofs_per_patch = spline.GetNumberOfFunctionsPerPatch();
  std::vector<int> nnz(ndofs, ndofs_per_patch);
  mkr::PetscData* PD = new mkr::PetscData(nnz);
  PetscErrorCode ierr;
  ierr = MatSetOption(PD->stiffnessMAT, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRQ(ierr);
  ierr = MatSetOption(PD->stiffnessMAT, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);  CHKERRQ(ierr);
  ierr = MatZeroEntries(PD->stiffnessMAT); CHKERRQ(ierr);
  ierr = VecZeroEntries(PD->resVEC);       CHKERRQ(ierr);
    
  // Random dofs
  std::array<std::vector<double>,3> dofs{};
  for(int i=0; i<3; ++i)
    dofs[i].resize(ndofs);
  for(int i=0; i<ndofs; ++i)
    {
      double xyz[] = {dis(gen), dis(gen), dis(gen)};
      double norm = std::sqrt(xyz[0]*xyz[0] + xyz[1]*xyz[1] + xyz[2]*xyz[2]);
      dofs[0][i] = xyz[0]/norm;
      dofs[1][i] = xyz[1]/norm;
      dofs[2][i] = xyz[2]/norm;
    }

  // component number
  const int compNum = 0;

  // Compute residual and stiffness
  mkr::FitData::AssembleShapeConstraints(compNum, dofs, shape_data, rtree, spline, gamma, PD->resVEC, PD->stiffnessMAT);

  // Consistency of the residual
  const double EPS = 1.e-5;
  for(int a=0; a<ndofs; ++a)
    {
      dofs[compNum][a] += EPS;
      double Hplus = mkr::FitData::ComputeShapeConstraintFunctional(shape_data,  rtree, dofs, spline, gamma);
      dofs[compNum][a] -= 2.*EPS;
      double Hminus = mkr::FitData::ComputeShapeConstraintFunctional(shape_data, rtree, dofs, spline, gamma);

      dofs[compNum][a] += EPS;
      double res;
      ierr = VecGetValues(PD->resVEC, 1, &a, &res); CHKERRQ(ierr);
      double num_res = (Hplus-Hminus)/(2.*EPS);
      assert(std::abs(res-num_res)<10.*EPS);
    }

  // Consistency of the stiffness
  Mat kMat;
  ierr = MatDuplicate(PD->stiffnessMAT, MAT_COPY_VALUES, &kMat); CHKERRQ(ierr);
  Vec rPlus, rMinus;
  ierr = VecDuplicate(PD->resVEC, &rPlus);  CHKERRQ(ierr);
  ierr = VecDuplicate(PD->resVEC, &rMinus); CHKERRQ(ierr);
  
  /*
  for(int a=0; a<ndofs; ++a)
    {
      dofs[compNum][a] += EPS;
      ierr = VecZeroEntries(rPlus); CHKERRQ(ierr);
      mkr::FitData::AssembleShapeConstraints(compNum, dofs, shape_data, rtree, spline, rPlus, PD->stiffnessMAT);

      dofs[compNum][a] -= 2.*EPS;
      ierr = VecZeroEntries(rMinus); CHKERRQ(ierr);
      mkr::FitData::AssembleShapeConstraints(compNum, dofs, shape_data, rtree, spline, rMinus, PD->stiffnessMAT);

      dofs[compNum][a] += EPS;

      double* plus;
      ierr = VecGetArray(rPlus, &plus);   CHKERRQ(ierr);
      double* minus;
      ierr = VecGetArray(rMinus, &minus); CHKERRQ(ierr);
      
      for(int b=0; b<ndofs; ++b)
	{
	  double Kab;
	  ierr = MatGetValues(kMat, 1, &b, 1, &a, &Kab); CHKERRQ(ierr);
	  double num_Kab = (plus[b]-minus[b])/(2.*EPS);
	  std::cout<< std::endl << Kab <<" ~ "<<num_Kab;
	  //assert(std::abs(Kab-num_Kab)<10.*EPS);
	}
      ierr = VecRestoreArray(rPlus, &plus);   CHKERRQ(ierr);
      ierr = VecRestoreArray(rMinus, &minus); CHKERRQ(ierr);
      }*/
	
  ierr = MatDestroy(&kMat);   CHKERRQ(ierr);
  ierr = VecDestroy(&rPlus);  CHKERRQ(ierr);
  ierr = VecDestroy(&rMinus); CHKERRQ(ierr);
  
  delete PD;
  PetscFinalize();
}
