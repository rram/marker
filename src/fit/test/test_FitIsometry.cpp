// Sriramajayam

#include <mkr_FitData.h>
#include <mkr_AnnulusMap.h>
#include <mkr_IdentityMap.h>
#include <random>
#include <iostream>

int main(int argc, char** argv)
{
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Spline
  std::vector<double> kv1{}, kv2{};
  for(int i=0; i<10; ++i)
    {
      kv1.push_back( 5.+static_cast<double>(i) );
      kv2.push_back( std::sqrt(static_cast<double>(i)) );
    }
  kv2.push_back( std::sqrt(10.) );
  kv2.push_back( std::sqrt(11.) );
  mkr::CubicBSpline2D spline(kv1, kv2);

  // Annular Cartesian to parametric map
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> cmap_dis(2.,5.);
  const double center[] = {cmap_dis(gen), cmap_dis(gen)};
  mkr::AnnulusMap cmap(center);

  // mu
  const double mu = std::sqrt(5.);

  // PETSc data structure
  const int ndofs = spline.GetNumberOfFunctions();
  const int ndofs_per_patch = spline.GetNumberOfFunctionsPerPatch();

  std::vector<int> nnz(ndofs, ndofs_per_patch);
  mkr::PetscData* PD = new mkr::PetscData(nnz);
  PetscErrorCode ierr;
  ierr = MatSetOption(PD->stiffnessMAT, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRQ(ierr);
  ierr = MatSetOption(PD->stiffnessMAT, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);  CHKERRQ(ierr);
  ierr = MatZeroEntries(PD->stiffnessMAT); CHKERRQ(ierr);
  ierr = VecZeroEntries(PD->resVEC);       CHKERRQ(ierr);
    
  // Random dofs
  std::array<std::vector<double>,3> dofs{};
  dofs[0].resize(ndofs);
  dofs[1].resize(ndofs);
  dofs[2].resize(ndofs);
  std::uniform_real_distribution<> dis(-1., 1.);
  for(int i=0; i<ndofs; ++i)
    {
      dofs[0][i] = dis(gen);
      dofs[1][i] = dis(gen);
      dofs[2][i] = dis(gen);
    }


  for(int compNum=0; compNum<3; ++compNum)
    {
      // Compute residual and stiffness component-wise
      ierr = VecZeroEntries(PD->resVEC);       CHKERRQ(ierr);
      ierr = MatZeroEntries(PD->stiffnessMAT); CHKERRQ(ierr);
      mkr::FitData::AssembleIsometry(compNum, dofs, cmap, spline, mu, PD->resVEC, PD->stiffnessMAT);

      // Consistency of residual
      const double EPS = 1.e-5;
      for(int a=0; a<ndofs; ++a)
	{
	  dofs[compNum][a] += EPS;
	  double Hplus = mkr::FitData::ComputeIsometryFunctional(dofs, cmap, spline, mu);
	  dofs[compNum][a] -= 2.*EPS;
	  double Hminus = mkr::FitData::ComputeIsometryFunctional(dofs, cmap, spline, mu);
	  dofs[compNum][a] += EPS;

	  double res;
	  ierr = VecGetValues(PD->resVEC, 1, &a, &res); CHKERRQ(ierr);
	  double num_res = (Hplus-Hminus)/(2.*EPS);
	  assert(std::abs(res-num_res)<10.*EPS);
	}

      // Consistency of the stiffness
      Mat kMat;
      ierr = MatDuplicate(PD->stiffnessMAT, MAT_COPY_VALUES, &kMat); CHKERRQ(ierr);
      Vec rPlus, rMinus;
      ierr = VecDuplicate(PD->resVEC, &rPlus);  CHKERRQ(ierr);
      ierr = VecDuplicate(PD->resVEC, &rMinus); CHKERRQ(ierr);
      for(int a=0; a<ndofs; ++a)
	{
	  dofs[compNum][a] += EPS;
	  ierr = VecZeroEntries(rPlus); CHKERRQ(ierr);
	  mkr::FitData::AssembleIsometry(compNum, dofs, cmap, spline, mu, rPlus, PD->stiffnessMAT);

	  dofs[compNum][a] -= 2.*EPS;
	  ierr = VecZeroEntries(rMinus); CHKERRQ(ierr);
	  mkr::FitData::AssembleIsometry(compNum, dofs, cmap, spline, mu, rMinus, PD->stiffnessMAT);

	  dofs[compNum][a] += EPS;

	  double* plus;
	  ierr = VecGetArray(rPlus, &plus);   CHKERRQ(ierr);
	  double* minus;
	  ierr = VecGetArray(rMinus, &minus); CHKERRQ(ierr);
      
	  for(int b=0; b<ndofs; ++b)
	    {
	      double Kab;
	      ierr = MatGetValues(kMat, 1, &b, 1, &a, &Kab); CHKERRQ(ierr);
	      double num_Kab = (plus[b]-minus[b])/(2.*EPS);
	      assert(std::abs(Kab-num_Kab)<10.*EPS);
	    }
	  ierr = VecRestoreArray(rPlus, &plus);   CHKERRQ(ierr);
	  ierr = VecRestoreArray(rMinus, &minus); CHKERRQ(ierr);
	}
  
      ierr = MatDestroy(&kMat);   CHKERRQ(ierr);
      ierr = VecDestroy(&rPlus);  CHKERRQ(ierr);
      ierr = VecDestroy(&rMinus); CHKERRQ(ierr);
    } // compNum (component number)
  
  delete PD;
  PetscFinalize();
}
