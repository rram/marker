// Sriramajayam

#include <mkr_FitData.h>
#include <mkr_IdentityMap.h>
#include <iostream>
#include <fstream>
#include <random>

void Plot(const mkr::FitData& fit, const std::string filename);

int main(int argc, char** argv)
{
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Identity map
  const double origin[] = {0.,0.};
  mkr::IdentityMap cmap(origin);

  // B-spline knots
  std::vector<double> knots{};
  for(int i=-3; i<=13; ++i)
    knots.push_back( static_cast<double>(i)*M_PI/10. );
  
  
  // B spline
  mkr::CubicBSpline2D spline(knots, knots);

  // Generate interpolation data over [0,pi] x [0,pi];
  std::vector<mkr::InterpolationPair> interp_data{};
  mkr::InterpolationPair ipair;
  std::random_device rd;  
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(0.01, 0.99*M_PI);
  std::fstream pfile;
  pfile.open((char*)"samples.xyz", std::ios::out);
  for(int i=0; i<100; ++i)
    {
      ipair.first[0] = dis(gen);
      ipair.first[1] = dis(gen);
      ipair.second[0] = std::sin(ipair.first[0])*std::cos(ipair.first[1]);
      ipair.second[1] = std::sin(ipair.first[0])*std::sin(ipair.first[1]);
      ipair.second[2] = std::cos(ipair.first[0]);
      interp_data.push_back( ipair );
      pfile << ipair.second[0]<<" "<<ipair.second[1]<<" "<<ipair.second[2]<<std::endl;
    }
  pfile.close();
    
  // No shape data
  std::vector<mkr::CoordNormalPair> shape_data{};
  pfile.open((char*)"shape.xyz", std::ios::out);
  for(int i=0; i<5000; ++i)
    {
      double phi = dis(gen);
      double theta = dis(gen);
      std::array<double,3> pt{std::sin(phi)*std::cos(theta), std::sin(phi)*std::sin(theta), std::cos(phi)};
      shape_data.push_back( std::make_pair(pt,pt) );
      pfile << pt[0] <<" "<<pt[1]<<" "<<pt[2]<<std::endl;
    }
  pfile.close();

  
  // Normalizing factors
  const double alpha = 10.;
  const double beta = 1.e-7;
  const double gamma = 1.e-5;
  mkr::FitData* fit = new mkr::FitData(shape_data, interp_data, cmap, spline);

  // Initial guess for the solution
  fit->InitializeFit(alpha, beta);
  Plot(*fit, "fit-0.xyz");

  // Update the guess
  for(int iter=0; iter<10; ++iter)
    {
      std::cout << std::endl <<"Update iteration "<<iter;
      fit->UpdateShapeFit(alpha, beta, gamma);
      Plot(*fit, "fit-"+std::to_string(iter+1)+".xyz");
    }
  
  delete fit;
  PetscFinalize();
}


void Plot(const mkr::FitData& fit, const std::string filename)
{
  // Evaluate fit
  std::fstream pfile;
  pfile.open(filename.c_str(), std::ios::out);
  for(int i=1; i<50; ++i)
    for(int j=1; j<50; ++j)
      {
	double uv[] = {M_PI*static_cast<double>(i)/50., M_PI*static_cast<double>(j)/50.};
	double phi[3];
	fit.Evaluate(uv, phi, nullptr, nullptr);
	pfile << phi[0] <<" "<<phi[1]<<" "<<phi[2] << std::endl;
      }
  pfile.close();
  
  pfile.open((char*)"exact.xyz", std::ios::out);
  for(int i=1; i<99; ++i)
    for(int j=1; j<99; ++j)
      {
	double uv[] = {M_PI*static_cast<double>(i)/100., M_PI*static_cast<double>(j)/100.};
	double phi[] = {std::sin(uv[0])*std::cos(uv[1]),
			std::sin(uv[0])*std::sin(uv[1]),
			std::cos(uv[0])};
	pfile << phi[0] <<" "<<phi[1]<<" "<<phi[2] << std::endl;
      }
  pfile.close();
  return;
}
