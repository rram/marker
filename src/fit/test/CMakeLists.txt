# Sriramajayam

# Project
project(fit-tests)

# Identical for all targets
foreach(TARGET
    test_CubicBSpline1D
    test_CubicBSpline2D
    test_FitHessian
    test_FitPointConstraint
    test_FitShape
    test_FitData
    test_FitIsometry)
    #sa_fit)
  
  # Add this target
  add_executable(${TARGET}  ${TARGET}.cpp)
  
  # Link
  target_link_libraries(${TARGET} PUBLIC marker_fit)

  # choose appropriate compiler flags
  target_compile_features(${TARGET} PUBLIC ${mkr_COMPILE_FEATURES})
  
  add_test(NAME ${TARGET} COMMAND ${TARGET})
  
endforeach()
