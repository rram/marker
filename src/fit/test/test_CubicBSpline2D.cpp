// Sriramajayam

#include <mkr_TensorProductBSpline2D.h>
#include <iostream>
#include <cassert>
#include <cmath>
#include <random>

std::vector<double> k1{}, k2{};

void Test(const mkr::CubicBSpline2D& shp);

int main()
{
  int nknots = 10;
  for(int k=0; k<nknots; ++k)
    {
      k1.push_back(static_cast<double>(k));
      k2.push_back(std::sqrt(static_cast<double>(k)));
    }
  k2.push_back(std::sqrt(11.));
  k2.push_back(std::sqrt(12.));
    
  mkr::CubicBSpline2D Shp(k1, k2);
  Test(Shp);
}


void Test(const mkr::CubicBSpline2D& shp)
{
  mkr::CubicBSpline1D uShp(k1), vShp(k2);
  const int nuFuncs = uShp.GetNumberOfFunctions();
  const int nvFuncs = vShp.GetNumberOfFunctions();
  assert(shp.GetNumberOfFunctions()==nuFuncs*nvFuncs);
  assert(shp.GetNumberOfVariables()==2);
  assert(shp.GetDegreeOfSpline()==3);
  assert(shp.GetNumberOfFunctionsPerPatch()==uShp.GetNumberOfFunctionsPerInterval()*vShp.GetNumberOfFunctionsPerInterval());
  const int nPatches = shp.GetNumberOfPatches();
  const int nuInt = uShp.GetNumberOfIntervals();
  const int nvInt = vShp.GetNumberOfIntervals();
  assert(nPatches==nuInt*nvInt);
  
  // Check active basis functions
  for(int p=0; p<nPatches; ++p)
    {
      std::vector<int> indx(4*4);
      shp.GetActiveFunctions(p, indx);
      const int uInt = p/nvInt;
      const int vInt = p%nvInt;
      std::vector<int> uindx(4), vindx(4);
      uShp.GetActiveFunctions(uInt, uindx);
      uShp.GetActiveFunctions(vInt, vindx);
      assert(indx.size()==uindx.size()*vindx.size());
      int count = 0;
      for(int i=0; i<static_cast<int>(uindx.size()); ++i)
	for(int j=0; j<static_cast<int>(vindx.size()); ++j)
	  assert(indx[count++]==nvFuncs*uindx[i]+vindx[j]);
    }

  // Check partition of unity
  const double EPS = 1.e-5;
  {
    std::vector<int> indx(4*4), dindx(4*4), d2indx(4*4);
    std::vector<double> vals(4*4);
    std::vector<std::vector<double>> dvals(2, std::vector<double>(4*4));
    std::vector<std::vector<std::vector<double>>> d2vals(2, std::vector<std::vector<double>>(2, std::vector<double>(4*4)));
    std::vector<int> pindx(4*4), mindx(4*4);
    std::vector<double> pvals(4*4), mvals(4*4);
    std::vector<std::vector<double>> dpvals(2,std::vector<double>(4*4)), dmvals(2,std::vector<double>(4*4));

    std::random_device rd;  
    std::mt19937 gen(rd()); 
    std::uniform_real_distribution<> dis(0.,1.);
    for(int ui=0; ui<nuInt; ++ui)
      {
	auto uvec = uShp.GetInterval(ui);
	for(int vj=0; vj<nvInt; ++vj)
	  {
	    auto vvec = vShp.GetInterval(vj);
	    for(int k=0; k<10; ++k) // 10 random points in this patch
	      {
		double lambda[] = {dis(gen), dis(gen)};
		double xi[] = {uvec.first*lambda[0] + uvec.second*(1.-lambda[0]),
			       vvec.first*lambda[1] + vvec.second*(1.-lambda[1])};
		shp.GetNonzeroFunctions(xi, indx, vals);
		shp.GetNonzeroDFunctions(xi, dindx, dvals);
		shp.GetNonzeroD2Functions(xi, d2indx, d2vals);
		const int nshp = static_cast<int>(indx.size());
		assert(indx==dindx && indx==d2indx);
		assert(static_cast<int>(dvals.size())==2 &&
		       static_cast<int>(d2vals.size())==2 &&
		       static_cast<int>(d2vals.size())==2);
		assert(static_cast<int>(d2vals[0].size())==2 && static_cast<int>(d2vals[1].size())==2);
		assert(static_cast<int>(vals.size())==nshp &&
		       static_cast<int>(dvals[0].size())==nshp &&
		       static_cast<int>(dvals[1].size())==nshp &&
		       static_cast<int>(d2vals[0][0].size())==nshp &&
		       static_cast<int>(d2vals[0][1].size())==nshp &&
		       static_cast<int>(d2vals[1][0].size())==nshp &&
		       static_cast<int>(d2vals[1][1].size())==nshp);
		
		// partition of unity
		double sum = 0.;
		double dsum[2] = {0.,0.};
		double d2sum[2][2] = {{0.,0.},{0.,0.}};
		for(int a=0; a<nshp; ++a)
		  {
		    sum += vals[a];
		    for(int i=0; i<2; ++i)
		      {
			dsum[i] += dvals[i][a];
			for(int j=0; j<2; ++j)
			  d2sum[i][j] += d2vals[i][j][a];
		      }
		  }
		assert(std::abs(sum-1.)<EPS);
		assert(std::abs(dsum[0])+std::abs(dsum[1])<EPS);
		assert(std::abs(d2sum[0][0])+std::abs(d2sum[0][1]) +
		       std::abs(d2sum[1][0])+std::abs(d2sum[1][1])<EPS);
		
		// Consistency of first derivatives
		double xi_uplus[] = {xi[0]+EPS, xi[1]};
		double xi_uminus[] = {xi[0]-EPS, xi[1]};
		shp.GetNonzeroFunctions(xi_uplus, pindx, pvals);
		shp.GetNonzeroFunctions(xi_uminus, mindx, mvals);
		assert(pindx==indx && mindx==indx);
		for(int a=0; a<nshp; ++a)
		  {
		    double deriv = dvals[0][a];
		    double nderiv = (pvals[a]-mvals[a])/(2.*EPS);
		    assert(std::abs(deriv-nderiv)<10.*EPS);
		  }

		double xi_vplus[] = {xi[0], xi[1]+EPS};
		double xi_vminus[] = {xi[0], xi[1]-EPS};
		shp.GetNonzeroFunctions(xi_vplus, pindx, pvals);
		shp.GetNonzeroFunctions(xi_vminus, mindx, mvals);
		assert(pindx==indx && mindx==indx);
		for(int a=0; a<nshp; ++a)
		  {
		    double deriv = dvals[1][a];
		    double nderiv = (pvals[a]-mvals[a])/(2.*EPS);
		    assert(std::abs(deriv-nderiv)<10.*EPS);
		  }
		
		// Consistency of second derivatives
		shp.GetNonzeroDFunctions(xi_uplus, pindx, dpvals);
		shp.GetNonzeroDFunctions(xi_uminus, mindx, dmvals);
		assert(pindx==indx && mindx==indx);
		for(int a=0; a<nshp; ++a)
		  {
		    // Na,uu
		    double deriv = d2vals[0][0][a];
		    double nderiv = (dpvals[0][a]-dmvals[0][a])/(2.*EPS);
		    assert(std::abs(deriv-nderiv)<10.*EPS);
		    // Na,vu
		    deriv = d2vals[1][0][a];
		    nderiv = (dpvals[1][a]-dmvals[1][a])/(2.*EPS);
		    assert(std::abs(deriv-nderiv)<10.*EPS);
		  }
		
		shp.GetNonzeroDFunctions(xi_vplus, pindx, dpvals);
		shp.GetNonzeroDFunctions(xi_vminus, mindx, dmvals);
		assert(pindx==indx && mindx==indx);
		for(int a=0; a<nshp; ++a)
		  {
		    // Na,uv
		    double deriv = d2vals[0][1][a];
		    double nderiv = (dpvals[0][a]-dmvals[0][a])/(2.*EPS);
		    assert(std::abs(deriv-nderiv)<10.*EPS);
		    // Na,vv
		    deriv = d2vals[1][1][a];
		    nderiv = (dpvals[1][a]-dmvals[1][a])/(2.*EPS);
		    assert(std::abs(deriv-nderiv)<10.*EPS);
		  }

	      }
	  }
      }
  }
  
  return;
}
