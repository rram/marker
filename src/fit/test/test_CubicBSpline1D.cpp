// Sriramajayam

#include <mkr_BSpline1D.h>
#include <iostream>
#include <cmath>

void Test(const mkr::CubicBSpline1D &Shp);

int main()
{
  int nknots = 10;
  std::vector<double> knots{};
  for(int k=0; k<nknots; ++k)
    knots.push_back( static_cast<double>(k) );
  
  std::cout<<"\n\nTesting object: "
	   <<"\n===================";
  mkr::CubicBSpline1D Shp(knots);
  Test(Shp);
  
  std::cout<<"\n\nDONE\n\n";
}


void Test(const mkr::CubicBSpline1D &Shp)
{
  std::cout<<"\nNumber of knots: "<<Shp.GetNumberOfKnots();
  std::cout<<"\nKnot vector: ";
  for(int k=0; k<Shp.GetNumberOfKnots(); k++)
    std::cout<<Shp.GetKnot(k)<<", ";
  std::cout<<"\nDegree of spline: "<<Shp.GetDegreeOfSpline();
  std::cout<<"\nTotal number of functions: "<<Shp.GetNumberOfFunctions();
  std::cout<<"\nNumber of functions per interval: "<<Shp.GetNumberOfFunctionsPerInterval();
  std::cout<<"\nNumber of intervals: "<<Shp.GetNumberOfIntervals();
  std::cout<<"\nLimits of interval: ("<<Shp.GetInterval(0).first<<","<<Shp.GetInterval(0).second<<")";
  std::vector<int> activeshapes(4);
  Shp.GetActiveFunctions(0,activeshapes);
  std::cout<<"\nNumber of active shape functions in interval: "<<activeshapes.size();
  std::cout<<"\nActive shape function numbers in interval: "
	   <<activeshapes[0]<<", "
	   <<activeshapes[1]<<", "
	   <<activeshapes[2]<<", "
	   <<activeshapes[3];
  std::cout<<"\nNumber of variables: "<<Shp.GetNumberOfVariables();
  
  // Interval in which all basis functions are nonzero
  double tmin = Shp.GetKnot(3);
  double tmax = Shp.GetKnot(Shp.GetNumberOfFunctions());
  std::cout<<"\nInterval: "<<tmin<<", "<<tmax;
  
  const int N = 50;
  for(int i=1; i<N; i++)
    {
      double tval = tmin + (tmax-tmin)*static_cast<double>(i)/static_cast<double>(N);
      std::vector<double> shp(4), dshp(4), d2shp(4);
      std::vector<int> indx(4), dindx(4), d2indx(4);
      Shp.GetNonzeroFunctions(tval, indx, shp);
      Shp.GetNonzeroDFunctions(tval, dindx, dshp);
      Shp.GetNonzeroD2Functions(tval, d2indx, d2shp);
      assert(indx==dindx && indx==d2indx);

      // Check partition of unity
      double sum = 0.;
      double dsum = 0.;
      double d2sum = 0.;
      for(auto& x:shp) sum += x;
      for(auto& x:dshp) dsum += x;
      for(auto& x:d2shp) d2sum += x;
      assert(std::abs(sum-1.)<1.e-6);
      assert(std::abs(dsum)<1.e-6);
      assert(std::abs(d2sum)<1.e-6);
      
      // Test numerical derivatives
      const double eps = 1.e-4;
      const double tplus = tval+eps;
      const double tminus = tval-eps;
      std::vector<double> plus(4), minus(4);
      std::vector<int> indx_plus(4), indx_minus(4);
      Shp.GetNonzeroFunctions(tplus, indx_plus, plus);
      assert(indx==indx_plus);
      Shp.GetNonzeroFunctions(tminus, indx_minus, minus);
      assert(indx==indx_minus);
      const int nshapes = static_cast<int>(indx.size());
      for(int j=0; j<nshapes; ++j)
	{
	  double num = dshp[j];
	  double dnum = (plus[j]-minus[j])/(2.*eps);
	  assert(std::abs(num-dnum)<10.*eps);
	}

      // Test numerical second derivatives
      Shp.GetNonzeroDFunctions(tplus, indx_plus, plus);
      assert(indx==indx_plus);
      Shp.GetNonzeroDFunctions(tminus, indx_minus, minus);
      assert(indx==indx_minus);      
      for(int j=0; j<nshapes; ++j)
	{
	  double num = d2shp[j];
	  double dnum = (plus[j]-minus[j])/(2.*eps);
	  assert(std::abs(num-dnum)<10.*eps);
	}
    }
  return;
}
