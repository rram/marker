// Sriramajayam

#include <mkr_FitData.h>
#include <random>
#include <iostream>

int main(int argc, char** argv)
{
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Spline
  std::vector<double> kv1{}, kv2{};
  for(int i=0; i<10; ++i)
    {
      kv1.push_back( static_cast<double>(i) );
      kv2.push_back( std::sqrt(static_cast<double>(i)) );
    }
  kv2.push_back( std::sqrt(10.) );
  kv2.push_back( std::sqrt(11.) );
  mkr::CubicBSpline2D spline(kv1, kv2);

  // Interpolation data
  std::pair<double,double> uInt, vInt;
  spline.GetPatchIntervals(0, uInt, vInt);
  const double uMin = uInt.first;
  const double vMin = vInt.first;
  spline.GetPatchIntervals(spline.GetNumberOfPatches()-1, uInt, vInt);
  const double uMax = uInt.second;
  const double vMax = vInt.second;

  // Random data
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> udis(uMin, uMax);
  std::uniform_real_distribution<> vdis(vMin, vMax);
  std::uniform_real_distribution<> dis(-1., 1.);
  const int nPoints = 50;
  std::vector<mkr::InterpolationPair> interp_data(nPoints);
  for(int p=0; p<nPoints; ++p)
    {
      interp_data[p].first  = std::array<double,2>{udis(gen), vdis(gen)};
      interp_data[p].second = std::array<double,3>{dis(gen), dis(gen), dis(gen)};
      const auto& data = interp_data[p];
    }
  const double alpha = 4.+dis(gen);
  
  // PETSc data structure
  const int ndofs = spline.GetNumberOfFunctions();
  const int ndofs_per_patch = spline.GetNumberOfFunctionsPerPatch();
  std::vector<int> nnz(ndofs, ndofs_per_patch);
  mkr::PetscData* PD = new mkr::PetscData(nnz);
  PetscErrorCode ierr;
  ierr = MatSetOption(PD->stiffnessMAT, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRQ(ierr);
  ierr = MatSetOption(PD->stiffnessMAT, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);  CHKERRQ(ierr);
  ierr = MatZeroEntries(PD->stiffnessMAT); CHKERRQ(ierr);
  ierr = VecZeroEntries(PD->resVEC);       CHKERRQ(ierr);
    
  // Random dofs
  std::vector<double> dofs(ndofs);
  for(int i=0; i<ndofs; ++i)
    dofs[i] = dis(gen);

  // Component number
  const int compNum = 0;
  
  // Compute residual and stiffness
  mkr::FitData::AssemblePointConstraints(compNum, dofs, interp_data, spline, alpha, PD->resVEC, PD->stiffnessMAT);

  // Consistency of residual
  const double EPS = 1.e-5;
  for(int a=0; a<ndofs; ++a)
    {
      dofs[a] += EPS;
      double Hplus = mkr::FitData::ComputePointConstraintFunctional(compNum, interp_data, dofs, spline, alpha);
      dofs[a] -= 2.*EPS;
      double Hminus = mkr::FitData::ComputePointConstraintFunctional(compNum, interp_data, dofs, spline, alpha);

      dofs[a] += EPS;
      double res;
      ierr = VecGetValues(PD->resVEC, 1, &a, &res); CHKERRQ(ierr);
      double num_res = (Hplus-Hminus)/(2.*EPS);
      assert(std::abs(res-num_res)<10.*EPS);
    }

  
  // Consistency of stiffness
  Mat kMat;
  ierr = MatDuplicate(PD->stiffnessMAT, MAT_COPY_VALUES, &kMat); CHKERRQ(ierr);
  Vec rPlus, rMinus;
  ierr = VecDuplicate(PD->resVEC, &rPlus);  CHKERRQ(ierr);
  ierr = VecDuplicate(PD->resVEC, &rMinus); CHKERRQ(ierr);
  
  for(int a=0; a<ndofs; ++a)
    {
      dofs[a] += EPS;
      ierr = VecZeroEntries(rPlus); CHKERRQ(ierr);
      mkr::FitData::AssemblePointConstraints(compNum, dofs, interp_data, spline, alpha, rPlus, PD->stiffnessMAT);

      dofs[a] -= 2.*EPS;
      ierr = VecZeroEntries(rMinus); CHKERRQ(ierr);
      mkr::FitData::AssemblePointConstraints(compNum, dofs, interp_data, spline, alpha, rMinus, PD->stiffnessMAT);

      dofs[a] += EPS;

      double* plus;
      ierr = VecGetArray(rPlus, &plus);   CHKERRQ(ierr);
      double* minus;
      ierr = VecGetArray(rMinus, &minus); CHKERRQ(ierr);
      
      for(int b=0; b<ndofs; ++b)
	{
	  double Kab;
	  ierr = MatGetValues(kMat, 1, &b, 1, &a, &Kab); CHKERRQ(ierr);
	  double num_Kab = (plus[b]-minus[b])/(2.*EPS);
	  assert(std::abs(Kab-num_Kab)<10.*EPS);
	}
      ierr = VecRestoreArray(rPlus, &plus);   CHKERRQ(ierr);
      ierr = VecRestoreArray(rMinus, &minus); CHKERRQ(ierr);
    }
	

  ierr = MatDestroy(&kMat);   CHKERRQ(ierr);
  ierr = VecDestroy(&rPlus);  CHKERRQ(ierr);
  ierr = VecDestroy(&rMinus); CHKERRQ(ierr);
  delete PD;
  PetscFinalize();
}
