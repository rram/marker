// Sriramajayam

#ifndef MKR_FIT_DATA_H
#define MKR_FIT_DATA_H

#include <mkr_TensorProductBSpline2D.h>
#include <mkr_ParametricToCartesianMap.h>
#include <mkr_PetscData.h>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/index/rtree.hpp>


namespace mkr
{
  //! parametric coordinates to 3-component function value
  using InterpolationPair = std::pair<std::array<double,2>, std::array<double,3>>; //!< parameteric, cartesian

  //! Shape data
  using CoordNormalPair = std::pair<std::array<double,3>, std::array<double,3>>; //!< coordinates, normal

  class FitData
  {
  public:

    // boost R-tree for closest point search
    typedef boost::geometry::model::point<double, 3, boost::geometry::cs::cartesian> boost_point;
    typedef std::pair<boost_point, int> boost_pointID;
    typedef boost::geometry::index::rtree<boost_pointID, boost::geometry::index::quadratic<8>> boost_rtree;
    
    //! Constructor
    FitData(const std::vector<CoordNormalPair>& sp_data,  
	    const std::vector<InterpolationPair>& int_data,
	    const ParametricToCartesianMap& cmap, 
	    const CubicBSpline2D& basis);
    

    //! Destructor
    virtual ~FitData();

    //! Disable copy and assignment
    FitData(const FitData&) = delete;
    FitData& operator=(const FitData&) = delete;

    //! Evaluates the fit at a point
    void Evaluate(const double* X, double* val, double* dval, double* d2val) const;

    //! Initialize solution guess
    void InitializeFit(const double alpha, const double beta);

    //! Update solution guess using the isometry constraint
    void UpdateFit(const double alpha, const double beta, const double mu);

    //! Update solution guess using shape constraints
    void UpdateShapeFit(const double alpha, const double beta, const double gamma);
    
    //! Solve component-wise
    /*static void Solve(const int nIters,
      const std::vector<InterpolationPair>& interp_data,
      const std::vector<CoordNormalPair>& shape_data,
      const ParametricToCartesianMap& cmap,
      const CubicBSpline2D& spline,
      const double alpha, const double beta, const double gamma, 
      std::array<std::vector<double>,3>& dofs);*/

    // Evaluate functionals
    static double ComputePointConstraintFunctional(const int compNum,
						   const std::vector<InterpolationPair>& interp_data,
						   const std::vector<double>& dofs,
						   const CubicBSpline2D& spline,
						   const double alpha);
    
    static double ComputeHessianFunctional(const std::vector<double>& dofs,
					   const ParametricToCartesianMap& cmap,
					   const CubicBSpline2D& spline,
					   const double beta);

    static double ComputeShapeConstraintFunctional(const std::vector<CoordNormalPair>& shape_data,
						   const boost_rtree& rtree, 
						   const std::array<std::vector<double>,3>& dofs,
						   const CubicBSpline2D& spline,
						   const double gamma);

    static double ComputeIsometryFunctional(const std::array<std::vector<double>,3>& dofs,
					    const ParametricToCartesianMap& cmap,
					    const CubicBSpline2D& spline,
					    const double mu);
											   
    //! Assemble point constraints
    static void AssemblePointConstraints(const int compNum,
					 const std::vector<double>& dofs,
					 const std::vector<InterpolationPair>& interp_data,
					 const CubicBSpline2D& spline,
					 const double alpha,
					 Vec& resVec, Mat& kMat);

    //! Assemble the Hessian
    static void AssembleHessian(const std::vector<double>& dofs,
				const ParametricToCartesianMap& cmap,
				const CubicBSpline2D& spline,
				const double beta,
				Vec& resVec, Mat& kMat);
    
    //! Assemble shape constraints
    static void AssembleShapeConstraints(const int compNum,
					 const std::array<std::vector<double>,3>& dofs,
					 const std::vector<CoordNormalPair>& shape_data,
					 const boost_rtree& rtree,
					 const CubicBSpline2D& spline,
					 const double gamma,
					 Vec& resVec, Mat& kMat);


    //! Assemble isometry constraints
    static void AssembleIsometry(const int compNum,
				 const std::array<std::vector<double>,3>& dofs,
				 const ParametricToCartesianMap& cmap,
				 const CubicBSpline2D& spline,
				 const double mu,
				 Vec& resVec, Mat& kMat);

  private:
    const std::vector<CoordNormalPair>& shape_data;
    const std::vector<InterpolationPair>& interp_data;
    const ParametricToCartesianMap& cartMap;
    const CubicBSpline2D& spline;
    const int ndofs;
    std::array<std::vector<double>,3> dofs;
    boost::geometry::index::rtree<boost_pointID, boost::geometry::index::quadratic<8>>* rtree;
    PetscData* PD;
    static const int nQuad1D;
    static const std::vector<double> qCoords1D;
    static const std::vector<double> qWeights1D;

  
  };
}

#endif
