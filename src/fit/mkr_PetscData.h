// Sriramajayam

#ifndef MKR_PETSC_DATA_H
#define MKR_PETSC_DATA_H

#include <vector>
#include <petscmat.h>
#include <petscvec.h>
#include <petscksp.h>

namespace mkr
{
  class PetscData
  {
  public:
    //! Constructor
    PetscData(const std::vector<int>& nz);

    //! Destructor
    virtual ~PetscData();

    //! Solve
    void Solve(Vec& b, Vec& x);

    //! members
    Vec  resVEC;
    Vec  solutionVEC;
    Mat  stiffnessMAT;
    KSP  kspSOLVER;
  };

}

#endif
