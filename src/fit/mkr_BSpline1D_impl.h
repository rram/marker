// Sriramajayam

#ifndef MKR_BSPLINE_1D_IMPL_H
#define MKR_BSPLINE_1D_IMPL_H

#include <mkr_BSpline1D.h>

namespace mkr
{
  
  // Implementation of the class

  // Constructor
  // GSL computes knots from a given set of break points by repeating 
  // break points at the ends.
  // We will not do so here. Instead, we will directly set the knot vector
  // that has been provided.
  // The only place where the break points terminolofy is useful is to 
  // set the correct size of data structures when allocating workspaces


  // FYI: From GSL's b spline documentation
  /*
   *  gsl_bspline_knots()
   *  Compute the knots from the given breakpoints:
   *
   *  knots(1:k) = breakpts(1) 
   *  knots(k+1:k+l-1) = breakpts(i), i = 2 .. l
   * knots(n+1:n+k) = breakpts(l + 1)
   *  
   *  where l is the number of polynomial pieces (l = nbreak - 1) and
   *  n = k + l - 1
   *  (using matlab syntax for the arrays)
   *  
   *  The repeated knots at the beginning and end of the interval
   *  correspond to the continuity condition there. See pg. 119
   *  of [1]
   *  Inputs: breakpts - breakpoints
   *  w        - bspline workspace
   *  
   * Return: success or error
   */

  // Constructor
  template<int DEGREE>
    BSpline1D<DEGREE>::BSpline1D(const std::vector<double>& kv)
    {
      // Number of knots
      const int nknots = int(kv.size());
  
      // Order of the spline
      const int order = DEGREE+1;
  
      // Number of control points (eg: 4 fewer than the number of knots for a cubic spline)
      const int ncontrol = nknots-order;
  
      // Number of break points (convention used by GSL)
      const int nbreak = ncontrol-order+2;
  
      // Allocate workspace for bsplines
      bw = gsl_bspline_alloc(order, nbreak);
      nz_shp   = gsl_vector_calloc(order);
      nz_dshp  = gsl_matrix_calloc(order, 2);
      nz_d2shp = gsl_matrix_calloc(order, 3);
      
      // Set the knot vector for the workspace
      for(int k=0; k<nknots; ++k)
	gsl_vector_set(bw->knots, k, kv[k]);
      
    }


  // Destructor
  template< int DEGREE>
    BSpline1D<DEGREE>::~BSpline1D()
    {
      // Free allocated memory
      gsl_bspline_free(bw);
      gsl_vector_free(nz_shp);
      gsl_matrix_free(nz_dshp);
      gsl_matrix_free(nz_d2shp);
    }


  // Return the number of knots
  template<int DEGREE>
    int BSpline1D<DEGREE>::GetNumberOfKnots() const
    {
      return bw->knots->size;
    }


  // Returns the requested knot
  template<int DEGREE>
    double BSpline1D<DEGREE>::GetKnot(const int k) const
    {
      return gsl_vector_get(bw->knots, k);
    }


  // Returns the knot vector
  template<int DEGREE>
    std::vector<double> BSpline1D<DEGREE>::GetKnotVector() const
    {
      const int nknots = GetNumberOfKnots();
      std::vector<double> kv(nknots);
      for( int i=0; i<nknots; ++i)
	kv[i] = GetKnot(i);
      return kv;
    }


  // Returns the degree of spline
  template<int DEGREE>
    int BSpline1D<DEGREE>::GetDegreeOfSpline() const
    {
      return DEGREE;
    }

  // Returns the number of break points
  template<int DEGREE>
    int BSpline1D<DEGREE>::GetNumberOfBreakPoints() const
    {
      return bw->nbreak;
    }


  // Returns the order of the spline
  template<int DEGREE>
    int BSpline1D<DEGREE>::GetOrderOfSpline() const
    {
      return bw->k;
    }


  // Returns the number of (basis) shape functions
  template<int DEGREE>
    int BSpline1D<DEGREE>::GetNumberOfFunctions() const
    {
      return bw->n;
    }

  // Returns the number of basis functions per interval
  template<int DEGREE>
    int BSpline1D<DEGREE>::GetNumberOfFunctionsPerInterval() const
    {
      return GetOrderOfSpline();
    }


  // Returns the number of pieces (elements)
  template<int DEGREE>
    int BSpline1D<DEGREE>::GetNumberOfIntervals() const
    {
      return bw->l;
    }


  // Returns the limits of a requested interval
  template<int DEGREE>
    std::pair<double,double> BSpline1D<DEGREE>::GetInterval(const int intnum) const
  {
    return std::make_pair(GetKnot(intnum+DEGREE), GetKnot(intnum+DEGREE+1));
  }


  // Returns the indices of shape functions active on a given interval
  template<int DEGREE>
    void BSpline1D<DEGREE>::
    GetActiveFunctions(const int intnum, std::vector<int>& activeshapes) const
    {
      assert(static_cast<int>(activeshapes.size())==DEGREE+1);
      for( int j=0; j<=DEGREE; j++)
	activeshapes[j] = intnum+j;
      return;
    }


  // Returns the number of parametric variables
  template< int DEGREE>
    int BSpline1D<DEGREE>::GetNumberOfVariables() const
    { return 1; }


  // Evaluate nonzero shape functions at given parametric coordinate
  template<int DEGREE>
    void BSpline1D<DEGREE>::GetNonzeroFunctions(const double xi, 
						std::vector< int>& shpnum, 
						std::vector<double>& vals) const
    {
      assert(static_cast<int>(shpnum.size())==DEGREE+1);
      assert(static_cast<int>(vals.size())==DEGREE+1);
  
      // Evaluate nonzero shape functions
      size_t istart, iend;
      gsl_bspline_eval_nonzero(xi, nz_shp, &istart, &iend, bw);
      for(size_t i=istart; i<=iend; i++)
	{
	  shpnum[i-istart] = i;
	  vals[i-istart]   = gsl_vector_get(nz_shp, i-istart);
	}

      //assert(bw->k==static_cast<int>(shpnum.size()));
      return;
    }
					      
  

  // Evaluate derivatives of nonzero shape functions at given parametric coordinate
  template< int DEGREE>
    void BSpline1D<DEGREE>::GetNonzeroDFunctions(const double xi, 
						 std::vector< int>& shpnum, 
						 std::vector<double>& dvals) const
    {
      assert(static_cast<int>(shpnum.size())==DEGREE+1);
      assert(static_cast<int>(dvals.size())==DEGREE+1);
  
      // Evaluate derivatives of nonzero shape functions
      size_t istart, iend;
      gsl_bspline_deriv_eval_nonzero(xi, 1, nz_dshp, &istart, &iend, bw);
      for(size_t i=istart; i<=iend; i++)
	{
	  shpnum[i-istart] = i;
	  dvals[i-istart]  = gsl_matrix_get(nz_dshp, i-istart, 1);
	}

      //assert(bw->k==static_cast<int>(shpnum.size()));
      return;
    }


  // Evaluate 2nd-derivatives of nonzero shape functions at given parametric coordinate
  template< int DEGREE>
    void BSpline1D<DEGREE>::GetNonzeroD2Functions(const double xi, 
						  std::vector<int>& shpnum, 
						  std::vector<double>& d2vals) const
    {
      assert(static_cast<int>(shpnum.size())==DEGREE+1);
      assert(static_cast<int>(d2vals.size())==DEGREE+1);
  
      if( DEGREE<=1 ) return;
  
      // Evaluate derivatives of nonzero shape functions
      size_t istart, iend;
      gsl_bspline_deriv_eval_nonzero(xi, 2, nz_d2shp, &istart, &iend, bw);
      for(size_t i=istart; i<=iend; ++i)
	{
	  shpnum[i-istart] = i;
	  d2vals[i-istart] = gsl_matrix_get(nz_d2shp, i-istart, 2);
	}

      //assert(bw->k==static_cast<int>(shpnum.size()));
      return;
    }

}
#endif
