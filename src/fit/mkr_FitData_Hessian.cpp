// Sriramajayam

#include <mkr_FitData.h>

namespace mkr
{
  // Compute the Hessian
  double FitData::ComputeHessianFunctional(const std::vector<double>& dofs,
					   const ParametricToCartesianMap& cmap,
					   const CubicBSpline2D& spline,
					   const double beta)
  {
    double Functional = 0.;
    const int ndofs_per_patch = spline.GetNumberOfFunctionsPerPatch();
    const int nPatches = spline.GetNumberOfPatches();
    std::pair<double,double> uInt, vInt;
    std::vector<int> shpnums(ndofs_per_patch);
    std::vector<std::vector<double>> dshpvals(2, std::vector<double>(ndofs_per_patch));
    std::vector<std::vector<std::vector<double>>> d2shpvals(2, std::vector<std::vector<double>>(2, std::vector<double>(ndofs_per_patch)));
    
    // quadrature point/weights
    double xi[2], qwt, Jac;
    double df[2], d2f[2][2], Hess[2][2];
    
    for(int e=0; e<nPatches; ++e)
      {
	spline.GetPatchIntervals(e, uInt, vInt);

	for(int p=0; p<nQuad1D; ++p)
	  {
	    xi[0] = uInt.first*qCoords1D[p] + uInt.second*(1.-qCoords1D[p]);
	    qwt   = (uInt.second-uInt.first)*qWeights1D[p];

	    for(int q=0; q<nQuad1D; ++q)
	      {
		xi[1] = vInt.first*qCoords1D[q] + vInt.second*(1.-qCoords1D[q]);
		qwt  *= (vInt.second-vInt.first)*qWeights1D[q];

		// Jacobian here
		Jac = cmap.Jacobian(xi);

		// df here:
		spline.GetNonzeroDFunctions(xi, shpnums, dshpvals);
		for(int i=0; i<2; ++i)
		  {
		    df[i] = 0.;
		    for(int a=0; a<ndofs_per_patch; ++a)
		      df[i] += dofs[shpnums[a]]*dshpvals[i][a];
		  }
		
		// d2f here:
		spline.GetNonzeroD2Functions(xi, shpnums, d2shpvals);
		for(int i=0; i<2; ++i)
		  for(int j=0; j<2; ++j)
		    {
		      d2f[i][j] = 0.;
		      for(int a=0; a<ndofs_per_patch; ++a)
			d2f[i][j] += dofs[shpnums[a]]*d2shpvals[i][j][a];
		    }

		// Compute Hessian
		cmap.Hessian(xi, df, d2f, Hess);
		
		// update functional
		for(int i=0; i<2; ++i)
		  for(int j=0; j<2; ++j)
		    Functional += 0.5*beta*Jac*qwt*Hess[i][j]*Hess[i][j];

	      } // q (1D quadrature)
	  } // p (1D quadrature)
      } // e (patch num)
    
    return Functional;
  }


  // Assemble the Hessian
  void FitData::AssembleHessian(const std::vector<double>& dofs,
				const ParametricToCartesianMap& cmap,
				const CubicBSpline2D& spline,
				const double beta,
				Vec& resVec, Mat& kMat)
  {
    PetscErrorCode ierr;
    const int ndofs_per_patch = spline.GetNumberOfFunctionsPerPatch();
    const int nPatches = spline.GetNumberOfPatches();
    std::vector<int> shpnums(ndofs_per_patch);
    std::vector<std::vector<double>> dshpvals(2, std::vector<double>(ndofs_per_patch));
    std::vector<std::vector<std::vector<double>>> d2shpvals(2, std::vector<std::vector<double>>(2, std::vector<double>(ndofs_per_patch)));
    std::vector<double> colvalues(ndofs_per_patch);
    std::pair<double,double> ucoord;
    std::pair<double,double> vcoord;


    // quadrature point/weights in a patch
    double xi[2];
    double qwt, Jac;
    double df[2], d2f[2][2], Hess[2][2];
    double dN[2], d2N[2][2], var_Hess[2][2], VAR_Hess[2][2];
    for(int e=0; e<nPatches; ++e)
      {
	spline.GetPatchIntervals(e, ucoord, vcoord);

	for(int p=0; p<nQuad1D; ++p)
	  {
	    xi[0] = ucoord.first*qCoords1D[p] + ucoord.second*(1.-qCoords1D[p]);
	    qwt   = qWeights1D[p]*(ucoord.second-ucoord.first);
	    
	    for(int q=0; q<nQuad1D; ++q)
	      {
		xi[1] = vcoord.first*qCoords1D[q] + vcoord.second*(1.-qCoords1D[q]);
		qwt  *= qWeights1D[q]*(vcoord.second-vcoord.first);

		// Jacobian
		Jac = cmap.Jacobian(xi);
		
		// 1st derivatives
		spline.GetNonzeroDFunctions(xi, shpnums, dshpvals);
		for(int i=0; i<2; ++i)
		  {
		    df[i] = 0.;
		    for(int a=0; a<ndofs_per_patch; ++a)
		      df[i] += dofs[shpnums[a]]*dshpvals[i][a];
		  }
		
		// 2nd derivatives 
		spline.GetNonzeroD2Functions(xi, shpnums, d2shpvals);
		for(int i=0; i<2; ++i)
		  for(int j=0; j<2; ++j)
		    {
		      d2f[i][j] = 0.;
		      for(int a=0; a<ndofs_per_patch; ++a)
			d2f[i][j] += dofs[shpnums[a]]*d2shpvals[i][j][a];
		    }

		// Compute the Hessian
		cmap.Hessian(xi, df, d2f, Hess);
			     
		// update the residual and stiffness
		for(int a=0; a<ndofs_per_patch; ++a)
		  {
		    const int row = shpnums[a];

		    // Hessian of this variation
		    for(int i=0; i<2; ++i)
		      {
			dN[i] = dshpvals[i][a];
			for(int j=0; j<2; ++j)
			  d2N[i][j] = d2shpvals[i][j][a];
		      }
		    cmap.Hessian(xi, dN, d2N, var_Hess);
		    
		    // update residual: H(f):H(Na)
		    double res = 0.;
		    for(int i=0; i<2; ++i)
		      for(int j=0; j<2; ++j)
			res += beta*Jac*qwt*Hess[i][j]*var_Hess[i][j];
		    
		    // update stiffness: H(Na)*H(Nb)
		    for(int b=0; b<ndofs_per_patch; ++b)
		      {
			// Hessian of this variation
			for(int i=0; i<2; ++i)
			  {
			    dN[i] = dshpvals[i][b];
			    for(int j=0; j<2; ++j)
			      d2N[i][j] = d2shpvals[i][j][b];
			  }
			cmap.Hessian(xi, dN, d2N, VAR_Hess);
			
			colvalues[b] = 0.;
			for(int i=0; i<2; ++i)
			  for(int j=0; j<2; ++j)
			    colvalues[b] += beta*Jac*qwt*var_Hess[i][j]*VAR_Hess[i][j];
		      }

		    ierr = VecSetValues(resVec, 1, &row, &res, ADD_VALUES);                                        CHKERRV(ierr);
		    ierr = MatSetValues(kMat,   1, &row, ndofs_per_patch, &shpnums[0], &colvalues[0], ADD_VALUES); CHKERRV(ierr); 

		  } // loop over 'a' (row)
	      } // loop over 'q' (1d quadrature)
	  } // loop over 'p' (1d quadrature)
      } // loop over 'e' (patch)

    // Finish up assembly
    ierr = MatAssemblyBegin(kMat, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(kMat, MAT_FINAL_ASSEMBLY);   CHKERRV(ierr);
    ierr = VecAssemblyBegin(resVec);                   CHKERRV(ierr);
    ierr = VecAssemblyEnd(resVec);                     CHKERRV(ierr);
    
    // done
    return;
  }

}
