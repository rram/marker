// Sriramajayam

#ifndef TENSOR_PRODUCT_BSPLINE_2D_IMPL_H
#define TENSOR_PRODUCT_BSPLINE_2D_IMPL_H

#include <cassert>

namespace mkr
{
  // Constructor
  template<int DEGREE> TensorProductBSpline2D<DEGREE>::
    TensorProductBSpline2D(const std::vector<double>&kv1,
			   const std::vector<double>& kv2)
    :uShp(kv1),
    vShp(kv2)
    {
      const int nuShp = uShp.GetNumberOfFunctionsPerInterval();
      uShpIndx.resize(nuShp);
      uShpVals.resize(nuShp);
      duShpVals.resize(nuShp);
      d2uShpVals.resize(nuShp);
      
      const int nvShp = vShp.GetNumberOfFunctionsPerInterval();
      vShpIndx.resize(nvShp);
      vShpVals.resize(nvShp);
      dvShpVals.resize(nvShp);
      d2vShpVals.resize(nvShp);
    }

  
  // Destructor
  template<int DEGREE> TensorProductBSpline2D<DEGREE>::
    ~TensorProductBSpline2D() {}

  // Returns the total number of basis functions
  template<int DEGREE> int TensorProductBSpline2D<DEGREE>::
    GetNumberOfFunctions() const
    {
      return uShp.GetNumberOfFunctions()*vShp.GetNumberOfFunctions();
    }
  
  // Returns the number of parametric variables
  template<int DEGREE> int TensorProductBSpline2D<DEGREE>::
    GetNumberOfVariables() const
    {
      return 2;
    }


  // Returns the degree of the splines
  template<int DEGREE> int TensorProductBSpline2D<DEGREE>::
    GetDegreeOfSpline() const
    {
      return DEGREE;
    }


  // Returns the number of basis functions per patch
  template<int DEGREE> int TensorProductBSpline2D<DEGREE>::
    GetNumberOfFunctionsPerPatch() const
    {
      return
	uShp.GetNumberOfFunctionsPerInterval()*
	vShp.GetNumberOfFunctionsPerInterval();
    }


  // Returns the number of patches
  template<int DEGREE> int TensorProductBSpline2D<DEGREE>::
    GetNumberOfPatches() const
    {
      return
	uShp.GetNumberOfIntervals()*
	vShp.GetNumberOfIntervals();
    }


  // Returns the limits of a requested patch
  template<int DEGREE> void TensorProductBSpline2D<DEGREE>::
    GetPatchIntervals(const int pnum,
		      std::pair<double,double>& uint,
		      std::pair<double,double>& vint) const
    {
      int unum, vnum;
      PatchToIntervalMap(pnum, unum, vnum);
      uint = uShp.GetInterval(unum);
      vint = vShp.GetInterval(vnum);
      return;
    }

  // Returns the indices of active basis functions in a given patch
  template<int DEGREE>
    void TensorProductBSpline2D<DEGREE>::
    GetActiveFunctions(const int patchnum, std::vector<int>& shpnum) const
    {
      assert(static_cast<int>(shpnum.size())==(DEGREE+1)*(DEGREE+1));
      
      int uint, vint; // u and v intervals
      PatchToIntervalMap(patchnum, uint, vint);
      uShp.GetActiveFunctions(uint, uShpIndx);
      vShp.GetActiveFunctions(vint, vShpIndx);

      int count = 0;
      for(auto& a:uShpIndx)
	for(auto& b:vShpIndx)
	  shpnum[count++] = IntervalToPatchMap(a,b);
  
      return;
    }


  // Computes nonzero  functions at given parametric coordinate and returns the
  // basis function numbers
  template<int DEGREE> void TensorProductBSpline2D<DEGREE>::
    GetNonzeroFunctions(const double* xi, 
			std::vector<int>& shpnum, 
			std::vector<double>& vals) const
    {
      
      
      // basis functions along u, v
      uShp.GetNonzeroFunctions(xi[0], uShpIndx, uShpVals);
      vShp.GetNonzeroFunctions(xi[1], vShpIndx, vShpVals);

      // Global basis functions
      const int nu = static_cast<int>(uShpIndx.size());
      const int nv = static_cast<int>(vShpIndx.size());
      
      assert(static_cast<int>(shpnum.size())==nu*nv);
      assert(static_cast<int>(vals.size())==nu*nv);

      int count = 0;
      for(int i=0; i<nu; ++i)
	for(int j=0; j<nv; ++j)
	  {
	    shpnum[count] = IntervalToPatchMap(uShpIndx[i], vShpIndx[j]);
	    vals[count]   = uShpVals[i]*vShpVals[j];
	    ++count;
	  }
      return;
    }


  // Computes derivatives wrt parameteric coordinates of 
  // nonzero basis functions at given parametric coordinate and 
  template<int DEGREE> void TensorProductBSpline2D<DEGREE>::
    GetNonzeroDFunctions(const double* xi, 
			 std::vector<int>& shpnum, 
			 std::vector<std::vector<double>>& dvals) const
    {
      // u-direction
      uShp.GetNonzeroFunctions(xi[0], uShpIndx, uShpVals);
      uShp.GetNonzeroDFunctions(xi[0], uShpIndx, duShpVals);

      // v-direction
      vShp.GetNonzeroFunctions(xi[1], vShpIndx, vShpVals);
      vShp.GetNonzeroDFunctions(xi[1], vShpIndx, dvShpVals);

      // Sizes
      const int nu = static_cast<int>(uShpIndx.size());
      const int nv = static_cast<int>(vShpIndx.size());
      assert(static_cast<int>(shpnum.size())==nu*nv);
      assert(static_cast<int>(dvals.size())==2);
      assert(static_cast<int>(dvals[0].size())==nu*nv && static_cast<int>(dvals[1].size())==nu*nv);
      if(static_cast<int>(dvals.size())<2) dvals.resize(2);
      
      int count = 0;
      for(int a=0; a<nu; ++a)
	for(int b=0; b<nv; ++b)
	  {
	    shpnum[count] = IntervalToPatchMap(uShpIndx[a], vShpIndx[b]);
	    dvals[0][count] = duShpVals[a]*vShpVals[b];
	    dvals[1][count] = uShpVals[a]*dvShpVals[b];
	    ++count;
	  }
      return;
    }


  // Computes 2nd derivatives wrt parametric coordinates of nonzero basis functions 
  // at given parametric coordinate and returns the basis function numbers
  template<int DEGREE> void TensorProductBSpline2D<DEGREE>::
    GetNonzeroD2Functions(const double* xi, 
			  std::vector< int>& shpnum, 
			  std::vector<std::vector<std::vector<double>>>& d2vals) const
    {
      // u-direction
      uShp.GetNonzeroFunctions(xi[0], uShpIndx, uShpVals);
      uShp.GetNonzeroDFunctions(xi[0], uShpIndx, duShpVals);
      uShp.GetNonzeroD2Functions(xi[0], uShpIndx, d2uShpVals);

      // v-direction
      vShp.GetNonzeroFunctions(xi[1], vShpIndx, vShpVals);
      vShp.GetNonzeroDFunctions(xi[1], vShpIndx, dvShpVals);
      vShp.GetNonzeroD2Functions(xi[1], vShpIndx, d2vShpVals);

      // Check sizes
      const int nu = static_cast<int>(uShpIndx.size());
      const int nv = static_cast<int>(vShpIndx.size());
      assert(static_cast<int>(shpnum.size())==nu*nv);
      assert(static_cast<int>(d2vals.size())==2);
      for(int i=0; i<2; ++i)
	{
	  assert(static_cast<int>(d2vals[i].size())==2);
	  assert(static_cast<int>(d2vals[i][0].size())==nu*nv && static_cast<int>(d2vals[i][1].size())==nu*nv);
	}
      
      // Compute derivatives
      int count = 0;
      for(int a=0; a<nu; ++a)
	for(int b=0; b<nv; ++b)
	  {
	    shpnum[count] = IntervalToPatchMap(uShpIndx[a], vShpIndx[b]);
	    d2vals[0][0][count] = d2uShpVals[a]*vShpVals[b];
	    d2vals[0][1][count] = duShpVals[a]*dvShpVals[b];
	    d2vals[1][0][count] = duShpVals[a]*dvShpVals[b];
	    d2vals[1][1][count] = uShpVals[a]*d2vShpVals[b];
	    ++count;
	  }

      return;
    }



  // Maps patch number to pair of interval numbers
  template<int DEGREE>
    void TensorProductBSpline2D<DEGREE>::
    PatchToIntervalMap(const int patchnum, int &intU, int &intV) const
    {
      const int nv = vShp.GetNumberOfIntervals();
      intV = patchnum%nv;
      intU = patchnum/nv;
      return;
    }


  // Maps pair of interval basis functions to global basis function
  template<int DEGREE>
    int TensorProductBSpline2D<DEGREE>::
    IntervalToPatchMap(const int a, const int b) const
    { 
      const int nv = vShp.GetNumberOfFunctions();
      return nv*a + b;
    }

}
#endif
