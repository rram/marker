// Sriramajayam

#ifndef MKR_BSPLINE_1D_H
#define MKR_BSPLINE_1D_H 

#include <vector>
#include <gsl/gsl_bspline.h>
#include <gsl/gsl_errno.h>
#include <cassert>

namespace mkr
{
  
  template<int DEGREE>
    class BSpline1D
    {
    public:
      //! Constructor
      //! \param kv Knot vector, copied
      BSpline1D(const std::vector<double>& kv);
  
      //! Destructor
      virtual ~BSpline1D();

      //! disable copy constructor
      BSpline1D(const BSpline1D<DEGREE>& Obj) = delete;
  
      //! Returns the number of knots
      int GetNumberOfKnots() const;
  
      //! Returns the requested knot
      //! \param k knot number
      double GetKnot(const int k) const;
  
      //! Returns the knot vector
      std::vector<double> GetKnotVector() const;
  
      //! Returns the degree of spline
      int GetDegreeOfSpline() const;
  
      //! Returns the total number of basis functions
      int GetNumberOfFunctions() const;
  
      //! Returns the number of basis functions per interval
      int GetNumberOfFunctionsPerInterval() const;
  
      //! Returns the number of pieces (elements)
      int GetNumberOfIntervals() const;

      //! Returns the limits of a requested interval
      //! \param intnum Interval number whose limits to return
      std::pair<double,double> GetInterval(const int intnum) const;
    
      //! Returns the number of parametric variables
      int GetNumberOfVariables() const;

      //! Returns the indices of shape functions active on a given interval
      void GetActiveFunctions(const int intnum, std::vector<int>& shpnum) const;
  
      //! Computes nonzero shape functions at given parametric coordinate and returns the
      //! shape function numbers
      //! \param xi Parametric coordinate
      //! \param vals Computed list of values for shape functions
      //! \param shpnum Numbers of nonzero shape functions
      void GetNonzeroFunctions(const double xi, 
			       std::vector<int>& shpnum, 
			       std::vector<double>& vals) const;

      void GetNonzeroDFunctions(const double xi, 
				std::vector<int>& shpnum, 
				std::vector<double>& dvals) const;

      void GetNonzeroD2Functions(const double xi, 
				 std::vector<int>& shpnum, 
				 std::vector<double>& d2vals) const;

    protected:

      //! Returns the number of break points
      int GetNumberOfBreakPoints() const;
  
      //! Returns the order of the spline
      int GetOrderOfSpline() const;
  
      //! Workspace for b-spline
      gsl_bspline_workspace *bw;
      gsl_vector* nz_shp;
      gsl_matrix* nz_dshp;
      gsl_matrix* nz_d2shp;
    };

  //! Linear B-spline functions
  using LinearBSpline1D = BSpline1D<1>;

  //! Quadratic B-spline functions
  using QuadraticBSpline1D = BSpline1D<2>;

  //! Cubic B-spline functions
  using CubicBSpline1D = BSpline1D<3>;
}

#endif

#include <mkr_BSpline1D_impl.h>
