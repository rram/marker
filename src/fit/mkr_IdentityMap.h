// Sriramajayam

#ifndef MKR_IDENTITY_MAP_H
#define MKR_IDENTITY_MAP_H

namespace mkr
{
  class IdentityMap: public ParametricToCartesianMap
  {

  public:
    //! Constructor
    inline IdentityMap(const double* orig)
      :origin{orig[0], orig[1]}
    {}

    //! Destructor
    inline virtual ~IdentityMap()
    {}

    //! Evaluate the parametric to cartesian map
    inline void Evaluate(const double* xi, double* phi) const override
    {
      phi[0] = xi[0];
      phi[1] = xi[1];
    }
    
    // Evaluate the Jacobian
    inline void Jacobian(const double* xi, double Jac[][2]) const override
    {
      Jac[0][0] = 1.;
      Jac[0][1] = 0.;
      Jac[1][0] = 0.;
      Jac[1][1] = 1.;
      return;
    }
    
    inline double Jacobian(const double* xi) const override
    { return 1.0; }
    
    // Evaluate the Hessian of a given function
    inline void Hessian(const double* xi, const double* df, const double d2f[][2], double Hess[][2]) const override
    {
      for(int i=0; i<2; ++i)
	for(int j=0; j<2; ++j)
	  Hess[i][j] = d2f[i][j];
    }
    
  private:
    const double origin[2];
  };
}

#endif
