// Sriramajayam

#include <mkr_FitData.h>

namespace mkr
{
  // Compute the pointwise-constraint functional
  double FitData::ComputePointConstraintFunctional(const int compNum,
						   const std::vector<InterpolationPair>& interp_data,
						   const std::vector<double>& dofs,
						   const CubicBSpline2D& spline,
						   const double alpha)
  {
    double Functional = 0.;
    const int ndofs_per_patch = spline.GetNumberOfFunctionsPerPatch();
    std::vector<int> shpnums(ndofs_per_patch);
    std::vector<double> shpvals(ndofs_per_patch);
    const int nPoints = static_cast<int>(interp_data.size());
    double fval = 0.;

    for(int p=0; p<nPoints; ++p)
      {
	const auto& data   = interp_data[p];
	const auto& xi     = data.first;
	const double& gval = data.second[compNum];
	
	// Evaluate function value here
	fval = 0.;
	spline.GetNonzeroFunctions(&xi[0], shpnums, shpvals);
	for(int a=0; a<ndofs_per_patch; ++a)
	  fval += dofs[shpnums[a]]*shpvals[a];

	// Update functional
	Functional += 0.5*alpha*(fval-gval)*(fval-gval);
      }

    // done
    return Functional;
  }
  

  // Assemble point constraints
  void FitData::AssemblePointConstraints(const int compNum,
					 const std::vector<double>& dofs,
					 const std::vector<InterpolationPair>& interp_data,
					 const CubicBSpline2D& spline,
					 const double alpha,
					 Vec& resVec, Mat& kMat)
  {
    PetscErrorCode ierr;
    const int nPoints = static_cast<int>(interp_data.size());
    const int ndofs_per_patch = spline.GetNumberOfFunctionsPerPatch();
    std::vector<int>    shpnums(ndofs_per_patch);
    std::vector<double> shpvals(ndofs_per_patch);
    std::vector<double> colvalues(ndofs_per_patch);
    double fval;
    
    for(int p=0; p<nPoints; ++p)
      {
	// This constraint
	const auto& data = interp_data[p];
	const auto& xi   = data.first;           // parametric coordinates 
	const auto& gval = data.second[compNum]; // value to fit

	// Evaluate the active basis functions here
	spline.GetNonzeroFunctions(&xi[0], shpnums, shpvals);

	// Function value at the current solution guess
	fval = 0.;
	for(int a=0; a<ndofs_per_patch; ++a)
	  fval += dofs[shpnums[a]]*shpvals[a];

	// update residual & stiffness
	for(int a=0; a<ndofs_per_patch; ++a)
	  {
	    const int& row = shpnums[a];

	    // residual
	    const double res = alpha*(fval-gval)*shpvals[a];

	    // stiffness
	    for(int b=0; b<ndofs_per_patch; ++b)
	      colvalues[b] = alpha*shpvals[a]*shpvals[b];

	    // assemble
	    ierr = VecSetValues(resVec, 1, &row, &res, ADD_VALUES);                                        CHKERRV(ierr);
	    ierr = MatSetValues(kMat,   1, &row, ndofs_per_patch, &shpnums[0], &colvalues[0], ADD_VALUES); CHKERRV(ierr);
	  }
      }

    // Finish up assembly
    ierr = MatAssemblyBegin(kMat, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(kMat,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = VecAssemblyBegin(resVec);                   CHKERRV(ierr);
    ierr = VecAssemblyEnd(resVec);                     CHKERRV(ierr);
    
    // done
    return;
  }

}
