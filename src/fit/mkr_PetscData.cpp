// Sriramajayam

#include <mkr_PetscData.h>

namespace mkr
{
  // Constructor: allocate
  PetscData::PetscData(const std::vector<int>& nz)
  {
    const int ndof = static_cast<int>(nz.size());
    
    // Set the sizes, and options
    PetscErrorCode ierr;
    PetscBool flag;
    ierr = PetscInitialized(&flag); CHKERRV(ierr);
    assert(flag==PETSC_TRUE);

    ierr = MatCreateSeqAIJ(PETSC_COMM_WORLD, ndof, ndof, PETSC_DEFAULT, &nz[0], &stiffnessMAT); CHKERRV(ierr);
    ierr = VecCreate(PETSC_COMM_WORLD,&(resVEC)); CHKERRV(ierr);
    ierr = VecSetSizes(resVEC,PETSC_DECIDE, ndof); CHKERRV(ierr);
    ierr = VecSetFromOptions(resVEC); CHKERRV(ierr);
    ierr = VecDuplicate(resVEC,&(solutionVEC)); CHKERRV(ierr);

    // Options: maintain sparsity
    ierr = MatSetOption(stiffnessMAT, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE); CHKERRV(ierr);
    
    // initialize to zero
    ierr = MatZeroEntries(stiffnessMAT); CHKERRV(ierr);
    ierr = VecZeroEntries(resVEC);       CHKERRV(ierr);
    ierr = VecZeroEntries(solutionVEC);  CHKERRV(ierr);

    // linear solver
    ierr = KSPCreate(PETSC_COMM_WORLD, &(kspSOLVER)); CHKERRV(ierr);
    ierr = KSPSetFromOptions(kspSOLVER); CHKERRV(ierr);
    
    // Preconditioner
    PC prec;  
    ierr = KSPGetPC(kspSOLVER, &prec); CHKERRV(ierr);
    ierr = PCSetType(prec, PCLU);      CHKERRV(ierr);
    ierr = PCSetFromOptions(prec);     CHKERRV(ierr);
    
    // done
  }

  // Destructor: deallocate
  PetscData::~PetscData()
  {
    PetscErrorCode ierr;
    PetscBool flag;
    ierr = PetscFinalized(&flag);     CHKERRV(ierr);
    assert(flag==PETSC_FALSE);
    
    ierr = VecDestroy(&solutionVEC);  CHKERRV(ierr);
    ierr = VecDestroy(&resVEC);       CHKERRV(ierr);
    ierr = MatDestroy(&stiffnessMAT); CHKERRV(ierr);
    ierr = KSPDestroy(&kspSOLVER);    CHKERRV(ierr);

    // done
  }

  
  // Solve Ax = b
  void PetscData::Solve(Vec& RHS, Vec& X)
  {
    // Check assembly status
    PetscErrorCode ierr;
    PetscBool flag;
    ierr = MatAssembled(stiffnessMAT, &flag); CHKERRV(ierr);
    assert(flag==PETSC_TRUE);
    
    // Set operators
    ierr = KSPSetOperators(kspSOLVER, stiffnessMAT, stiffnessMAT); CHKERRV(ierr);
    
    // Initialize solution vector
    ierr = VecZeroEntries(X); CHKERRV(ierr);
    
    // Solve
    ierr = KSPSolve(kspSOLVER, RHS, X); CHKERRV(ierr);

    // How did it go?
    KSPConvergedReason reason;
    ierr = KSPGetConvergedReason(kspSOLVER, &reason); CHKERRV(ierr);
    if(reason<0)
      {
	PetscPrintf(PETSC_COMM_WORLD, "KSPConvergedReason: %d\n", reason);
	assert(false && "Solution has diverged.");
      }

    return;
  }

}
