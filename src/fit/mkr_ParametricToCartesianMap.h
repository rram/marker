// Sriramajayam

#ifndef MKR_PARAMETRIC_TO_CARTESIAN_MAP_H
#define MKR_PARAMETRIC_TO_CARTESIAN_MAP_H

namespace mkr
{
  class ParametricToCartesianMap
  {
  public:
    inline ParametricToCartesianMap() {}
    inline virtual ~ParametricToCartesianMap() {}

    //! Evaluate the parametric to cartesian map
    virtual void Evaluate(const double* xi, double* phi) const = 0;

    //! Evaluate the Jacobian
    virtual void Jacobian(const double* xi, double Jac[][2]) const = 0;
    virtual double Jacobian(const double* xi) const = 0;

    //! Evaluate the Hessian of a given function
    virtual void Hessian(const double* xi, const double* df, const double d2f[][2], double Hess[][2]) const = 0;
  };
}

#endif
