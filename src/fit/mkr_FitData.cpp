// Sriramajayam

#include <mkr_FitData.h>
#include <iostream>

namespace mkr
{
  const int FitData::nQuad1D = 4;
  const std::vector<double> FitData::qCoords1D = std::vector<double>{0.93056815579e0, 0.66999052179e0,
								     0.33000947821e0, 0.06943184421e0};
  
  const std::vector<double> FitData::qWeights1D = std::vector<double>{0.5*0.34785484513e0, 0.5*0.65214515486e0,
								      0.5*0.65214515486e0, 0.5*0.34785484513e0};
  
  // Constructor
  FitData::FitData(const std::vector<CoordNormalPair>& shp_data, 
		   const std::vector<InterpolationPair>& int_data,
		   const ParametricToCartesianMap& cmap, 
		   const CubicBSpline2D& basis)
    :shape_data(shp_data),
     interp_data(int_data),
     cartMap(cmap),
     spline(basis),
     ndofs(spline.GetNumberOfFunctions())
  {
    // Is Petsc initialized
    PetscBool flag;
    PetscInitialized(&flag);
    assert(flag==PETSC_TRUE);

    // Initialize all dofs to 0
    for(int c=0; c<3; ++c)
      {
	dofs[c].resize(ndofs);
	std::fill(dofs[c].begin(), dofs[c].end(), 0.);
      }

    // setup r-tree for shape-data queries
    const int nPoints = static_cast<int>(shape_data.size());
    std::vector<boost_pointID> tree_data(nPoints);
    for(int n=0; n<nPoints; ++n)
      {
	const auto& X = shape_data[n].first;
	tree_data[n] = std::make_pair(boost_point(X[0], X[1], X[2]), n);
      }

    // 1-stroke creation is faster than individual insertions
    rtree = new boost::geometry::index::rtree<boost_pointID, boost::geometry::index::quadratic<8>>(tree_data);

    // Create PETSc data structure
    const int ndofs = spline.GetNumberOfFunctions();
    const int ndofs_per_patch = spline.GetNumberOfFunctionsPerPatch();
    std::vector<int> nnz(ndofs, ndofs_per_patch);
    PD = new PetscData(nnz);

    // don't permit new nonzeros, keep nonzero pattern
    PetscErrorCode ierr;
    ierr = MatSetOption(PD->stiffnessMAT, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
    ierr = MatSetOption(PD->stiffnessMAT, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);  CHKERRV(ierr);
    
    // done
  }

  
  // Destructor
  FitData::~FitData()
  {
    delete rtree;
    delete PD;
  }


  // Initialize solution guess
  void FitData::InitializeFit(const double alpha, const double beta)
  {
    // access
    auto& kMat   = PD->stiffnessMAT;
    auto& resVec = PD->resVEC;
    auto& solVec = PD->solutionVEC;
    PetscErrorCode ierr;
        
    // compute component-wise guesses
    for(int comp=0; comp<3; ++comp)
      {
	std::cout << std::endl <<"Component "<<comp;

	// zero all entries
	ierr = VecZeroEntries(resVec); CHKERRV(ierr);
	ierr = VecZeroEntries(solVec); CHKERRV(ierr);
	ierr = MatZeroEntries(kMat);   CHKERRV(ierr);

	// Assemble contributions from the Hessian. This first to ensure complete fill in
	std::cout<< std::endl << "Assembling the Hessian ";
	AssembleHessian(dofs[comp], cartMap, spline, beta, resVec, kMat);
	
	// Assemble contributions from pointwise constraints
	std::cout<< std::endl << "Assembling point constraints ";
	AssemblePointConstraints(comp, dofs[comp], interp_data, spline, alpha, resVec, kMat);

	// Solve
	std::cout<< std::endl << "Solving ";
	PD->Solve(resVec, solVec);

	// Save the solution
	ierr = VecScale(solVec, -1.);         CHKERRV(ierr);
	double* sol;
	ierr = VecGetArray(solVec, &sol);     CHKERRV(ierr);
	for(int i=0; i<ndofs; ++i)
	  dofs[comp][i] = sol[i];
	ierr = VecRestoreArray(solVec, &sol); CHKERRV(ierr);
      }

    // done
    return;
  }

  
  // Update solution guess using the isometry constraint
  void FitData::UpdateFit(const double alpha, const double beta, const double mu)
  {
    // access
    auto& kMat   = PD->stiffnessMAT;
    auto& resVec = PD->resVEC;
    auto& solVec = PD->solutionVEC;
    PetscErrorCode ierr;

    // update component-wise
    for(int comp=0; comp<3; ++comp)
      {
	std::cout<< std::endl << "Component "<<comp;
	ierr = VecZeroEntries(resVec); CHKERRV(ierr);
	ierr = VecZeroEntries(solVec); CHKERRV(ierr);
	ierr = MatZeroEntries(kMat);   CHKERRV(ierr);

	// zero all entries
	ierr = VecZeroEntries(resVec); CHKERRV(ierr);
	ierr = VecZeroEntries(solVec); CHKERRV(ierr);
	ierr = MatZeroEntries(kMat);   CHKERRV(ierr);

	// Assemble contributions from the Hessian. This first to ensure complete fill in
	std::cout<< std::endl << "Assembling the Hessian ";
	AssembleHessian(dofs[comp], cartMap, spline, beta, resVec, kMat);

	// Assemble contributions from isometry. This will ensure fill in
	std::cout<< std::endl << "Assembling isometry ";
	AssembleIsometry(comp, dofs, cartMap, spline, mu, resVec, kMat);
	
	// Assemble contributions from pointwise constraints
	std::cout<< std::endl << "Assembling point constraints ";
	AssemblePointConstraints(comp, dofs[comp], interp_data, spline, alpha, resVec, kMat);
	
	// Solve
	PD->Solve(resVec, solVec);
	
	// Update the solution
	ierr = VecScale(solVec, -1.);         CHKERRV(ierr);
	double* sol;
	ierr = VecGetArray(solVec, &sol);     CHKERRV(ierr);
	for(int i=0; i<ndofs; ++i)
	  dofs[comp][i] += sol[i];
	ierr = VecRestoreArray(solVec, &sol); CHKERRV(ierr);
      }

    // done
    return;
  }


  // Update solution guess using shape constraints
  void FitData::UpdateShapeFit(const double alpha, const double beta, const double gamma)
  {
    double func = ComputeShapeConstraintFunctional(shape_data, *rtree, dofs, spline, gamma);
      
    // access
    auto& kMat   = PD->stiffnessMAT;
    auto& resVec = PD->resVEC;
    auto& solVec = PD->solutionVEC;
    PetscErrorCode ierr;

    // update component-wise
    for(int comp=0; comp<3; ++comp)
      {
	std::cout<< std::endl << "Component "<<comp;
	
	// zero all entries
	ierr = VecZeroEntries(resVec); CHKERRV(ierr);
	ierr = VecZeroEntries(solVec); CHKERRV(ierr);
	ierr = MatZeroEntries(kMat);   CHKERRV(ierr);

	// Assemble contributions from the Hessian. This first to ensure complete fill in
	std::cout<< std::endl << "Assembling the Hessian ";
	AssembleHessian(dofs[comp], cartMap, spline, beta, resVec, kMat);
	
	// Assemble contributions from pointwise constraints
	std::cout<< std::endl << "Assembling point constraints ";
	AssemblePointConstraints(comp, dofs[comp], interp_data, spline, alpha, resVec, kMat);

	// Assemble contributions from shape constraints
	std::cout<< std::endl << "Assembling shape constraints ";
	AssembleShapeConstraints(comp, dofs, shape_data, *rtree, spline, gamma, resVec, kMat);
	
	// Solve
	PD->Solve(resVec, solVec);
	
	// Update the solution
	ierr = VecScale(solVec, -1.);         CHKERRV(ierr);
	double* sol;
	ierr = VecGetArray(solVec, &sol);     CHKERRV(ierr);
	for(int i=0; i<ndofs; ++i)
	  dofs[comp][i] += sol[i];
	
	ierr = VecRestoreArray(solVec, &sol); CHKERRV(ierr);
      }

    // done
    return;

  }

  
  // Evaluates the fit at a point
  void FitData::Evaluate(const double* X, double* val, double* dval, double* d2val) const
  {
    const int ndofs_per_patch = spline.GetNumberOfFunctionsPerPatch();
    std::vector<int> shpnums(ndofs_per_patch);
    std::vector<double> shpvals(ndofs_per_patch);

    if(val!=nullptr)
      {
	spline.GetNonzeroFunctions(X, shpnums, shpvals);
	for(int k=0; k<3; ++k)
	  {
	    val[k] = 0.;
	    for(int a=0; a<ndofs_per_patch; ++a)
	      val[k] += dofs[k][shpnums[a]]*shpvals[a];
	  }
      }
    return;			       
  }
  
}


/*
// Next, include contributions from the shape constraints.
// The problem is noe nonlinear. Iterate component-wise
for(int iter=1; iter<=nIters; ++iter)
for(int comp=0; comp<3; ++comp)
{
// zero


// Assemble contributions from the Hessian
AssembleHessian(dofs[comp], cmap, spline, beta, resVec, kMat);
	
// Assemble contributions from pointwise constraints
AssemblePointConstraints(comp, dofs[comp], interp_data, spline, alpha, resVec, kMat);

// Assemble contributions from shape constraints
//AssembleShapeConstraints(comp, dofs, shape_data, spline, gamma, resVec, kMat);

}
*/
