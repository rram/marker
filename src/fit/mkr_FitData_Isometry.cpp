// Sriramajayam

#include <mkr_FitData.h>

namespace mkr
{
  double FitData::ComputeIsometryFunctional(const std::array<std::vector<double>,3>& dofs,
					    const ParametricToCartesianMap& cmap,
					    const CubicBSpline2D& spline,
					    const double mu)
  {
    double Functional = 0.;
    const int ndofs_per_patch = spline.GetNumberOfFunctionsPerPatch();
    const int nPatches = spline.GetNumberOfPatches();
    std::pair<double,double> uInt, vInt;
    std::vector<int> shpnums(ndofs_per_patch);
    std::vector<std::vector<double>> dshpvals(2, std::vector<double>(ndofs_per_patch));

    // quadrature point/weights
    double xi[2], qwt, detJ;
    double Jac[2][2], adJ[2][2];
    double dPhi_dX[3], dPhi_dY[3]; // grad Phi: ref->def
    double dphi_du[3], dphi_dv[3]; // grad phi: param->def
    double epsilon[2][2];          // membrane strain

    for(int e=0; e<nPatches; ++e)
      {
	spline.GetPatchIntervals(e, uInt, vInt);

	for(int p=0; p<nQuad1D; ++p)
	  {
	    xi[0] = uInt.first*qCoords1D[p] + uInt.second*(1.-qCoords1D[p]);
	    qwt   = (uInt.second-uInt.first)*qWeights1D[p];

	    for(int q=0; q<nQuad1D; ++q)
	      {
		xi[1] = vInt.first*qCoords1D[q] + vInt.second*(1.-qCoords1D[q]);
		qwt  *= (vInt.second-vInt.first)*qWeights1D[q];

		// Jacobian here
		cmap.Jacobian(xi, Jac);
		
		// detJ
		detJ = Jac[0][0]*Jac[1][1]-Jac[0][1]*Jac[1][0];

		// J^-T
		adJ[0][0] =  Jac[1][1]/detJ;
		adJ[1][1] =  Jac[0][0]/detJ;
		adJ[0][1] = -Jac[1][0]/detJ;
		adJ[1][0] = -Jac[0][1]/detJ;

		// shape function derivatives
		spline.GetNonzeroDFunctions(xi, shpnums, dshpvals);

		// tangents of the deformation map
		for(int i=0; i<3; ++i)
		  {
		    dPhi_dX[i] = 0.;
		    dPhi_dY[i] = 0.;
		    dphi_du[i] = 0.;
		    dphi_dv[i] = 0.;
		  }
		
		// component-wise derivatives wrt parametric coordinates (u, v)
		for(int compNum=0; compNum<3; ++compNum)
		  for(int a=0; a<ndofs_per_patch; ++a)
		    {
		      dphi_du[compNum] += dofs[compNum][shpnums[a]]*dshpvals[0][a];
		      dphi_dv[compNum] += dofs[compNum][shpnums[a]]*dshpvals[1][a];
		    }
		
		// update dPhi/dx, dPhi/dy
		for(int i=0; i<3; ++i)
		  {
		    dPhi_dX[i] += adJ[0][0]*dphi_du[i] + adJ[0][1]*dphi_dv[i];
		    dPhi_dY[i] += adJ[1][0]*dphi_du[i] + adJ[1][1]*dphi_dv[i];
		  }
		
		// strains: 1/2 (phi,i.phi,j - delta_ij)
		for(int i=0; i<2; ++i)
		  for(int j=0; j<2; ++j)
		    epsilon[i][j] = 0.;
		
		for(int i=0; i<3; ++i)
		  {
		    epsilon[0][0] += 0.5*dPhi_dX[i]*dPhi_dX[i];
		    epsilon[0][1] += 0.5*dPhi_dX[i]*dPhi_dY[i];
		    epsilon[1][0] += 0.5*dPhi_dY[i]*dPhi_dX[i];
		    epsilon[1][1] += 0.5*dPhi_dY[i]*dPhi_dY[i];
		  }
		epsilon[0][0] -= 0.5;
		epsilon[1][1] -= 0.5;

		// Update the functional
		for(int i=0; i<2; ++i)
		  for(int j=0; j<2; ++j)
		    Functional += 0.5*mu*detJ*qwt*epsilon[i][j]*epsilon[i][j];

	      } // q (1D quadrature)
	  } // p (1D quadrature)
      } // e (patch number)

    return Functional;
  }


  // Assemble the residual and stiffness from isometry functional
  void FitData::AssembleIsometry(const int compNum,
				 const std::array<std::vector<double>,3>& dofs,
				 const ParametricToCartesianMap& cmap,
				 const CubicBSpline2D& spline,
				 const double mu,
				 Vec& resVec, Mat& kMat)
  {
    PetscErrorCode ierr;
    const int ndofs_per_patch = spline.GetNumberOfFunctionsPerPatch();
    const int nPatches = spline.GetNumberOfPatches();
    std::vector<int> shpnums(ndofs_per_patch);
    std::vector<std::vector<double>> dshpvals(2, std::vector<double>(ndofs_per_patch));
    std::vector<std::vector<double>> Dshpvals(2, std::vector<double>(ndofs_per_patch));
    std::vector<double> colvalues(ndofs_per_patch);
    
    // quadrature/weights
    double xi[2];
    double Jac[2][2], adJ[2][2];
    double qwt, detJ;
    double dPhi_dX[3], dPhi_dY[3];
    double dN_dX[3], dN_dY[3], dM_dX[3], dM_dY[3];
    double epsilon[2][2], var_epsilon[2][2], VAR_epsilon[2][2], VAR_var_epsilon[2][2];
    std::pair<double,double> uInt, vInt;
    double res;
    
    for(int e=0; e<nPatches; ++e)
      {
	spline.GetPatchIntervals(e, uInt, vInt);

	for(int p=0; p<nQuad1D; ++p)
	  {
	    xi[0] = uInt.first*qCoords1D[p] + uInt.second*(1.-qCoords1D[p]);
	    qwt   = (uInt.second-uInt.first)*qWeights1D[p];

	    for(int q=0; q<nQuad1D; ++q)
	      {
		xi[1] = vInt.first*qCoords1D[q] + vInt.second*(1.-qCoords1D[q]);
		qwt  *= (vInt.second-vInt.first)*qWeights1D[q];

		// jacobian, det, adjoint
		cmap.Jacobian(xi, Jac);
		detJ = Jac[0][0]*Jac[1][1]-Jac[0][1]*Jac[1][0];
		adJ[0][0] = Jac[1][1]/detJ;
		adJ[1][1] = Jac[0][0]/detJ;
		adJ[0][1] = -Jac[1][0]/detJ;
		adJ[1][0] = -Jac[0][1]/detJ;

		// spline derivatives wrt u,v
		spline.GetNonzeroDFunctions(xi, shpnums, dshpvals);

		// spline derivatives wrt X,Y
		for(int a=0; a<ndofs_per_patch; ++a)
		  {
		    const double& du = dshpvals[0][a];
		    const double& dv = dshpvals[1][a];
		    double& dX = Dshpvals[0][a];
		    double& dY = Dshpvals[1][a];

		    dX = adJ[0][0]*du + adJ[0][1]*dv;
		    dY = adJ[1][0]*du + adJ[1][1]*dv;
		  }

		
		// derivatives of the deformation mapping
		for(int i=0; i<3; ++i)
		  {
		    dPhi_dX[i] = 0.;
		    dPhi_dY[i] = 0.;
		    for(int a=0; a<ndofs_per_patch; ++a)
		      {
			dPhi_dX[i] += dofs[i][shpnums[a]]*Dshpvals[0][a];
			dPhi_dY[i] += dofs[i][shpnums[a]]*Dshpvals[1][a];
		      }
		  }

		// strain
		for(int i=0; i<2; ++i)
		  for(int j=0; j<2; ++j)
		    epsilon[i][j] = 0.;
		for(int i=0; i<3; ++i)
		  {
		    epsilon[0][0] += 0.5*dPhi_dX[i]*dPhi_dX[i];
		    epsilon[1][1] += 0.5*dPhi_dY[i]*dPhi_dY[i];
		    epsilon[0][1] += 0.5*dPhi_dX[i]*dPhi_dY[i];
		    epsilon[1][0] += 0.5*dPhi_dY[i]*dPhi_dX[i];
		  }
		epsilon[0][0] -= 0.5;
		epsilon[1][1] -= 0.5;
		
		// update the residual and stiffness
		for(int a=0; a<ndofs_per_patch; ++a)
		  {
		    const int row = shpnums[a];

		    // this variation in the tangents
		    for(int i=0; i<3; ++i)
		      {
			dN_dX[i] = 0.;
			dN_dY[i] = 0.;
		      }
		    dN_dX[compNum] = Dshpvals[0][a];
		    dN_dY[compNum] = Dshpvals[1][a];

		    // this variation in strain
		    for(int i=0; i<2; ++i)
		      for(int j=0; j<2; ++j)
			var_epsilon[i][j] = 0.;
		    for(int i=0; i<3; ++i)
		      {
			var_epsilon[0][0] += dN_dX[i]*dPhi_dX[i];
			var_epsilon[1][1] += dN_dY[i]*dPhi_dY[i];
			var_epsilon[0][1] += 0.5*(dN_dX[i]*dPhi_dY[i] + dPhi_dX[i]*dN_dY[i]);
			var_epsilon[1][0] += 0.5*(dN_dY[i]*dPhi_dX[i] + dPhi_dY[i]*dN_dX[i]);
		      }

		    // update residual
		    res = 0.;
		    for(int i=0; i<2; ++i)
		      for(int j=0; j<2; ++j)
			res += mu*qwt*detJ*epsilon[i][j]*var_epsilon[i][j];
		    
		    ierr = VecSetValues(resVec, 1, &row, &res, ADD_VALUES); CHKERRV(ierr);

		    // update the jacobian
		    for(int b=0; b<ndofs_per_patch; ++b)
		      {
			// this variation in the tangents
			for(int i=0; i<3; ++i)
			  {
			    dM_dX[i] = 0.;
			    dM_dY[i] = 0.;
			  }
			dM_dX[compNum] = Dshpvals[0][b];
			dM_dY[compNum] = Dshpvals[1][b];

			// this variation in strain
			for(int i=0; i<2; ++i)
			  for(int j=0; j<2; ++j)
			    VAR_epsilon[i][j] = 0.;
			for(int i=0; i<3; ++i)
			  {
			    VAR_epsilon[0][0] += dM_dX[i]*dPhi_dX[i];
			    VAR_epsilon[1][1] += dM_dY[i]*dPhi_dY[i];
			    VAR_epsilon[0][1] += 0.5*(dM_dX[i]*dPhi_dY[i] + dPhi_dX[i]*dM_dY[i]);
			    VAR_epsilon[1][0] += 0.5*(dM_dY[i]*dPhi_dX[i] + dPhi_dY[i]*dM_dX[i]);
			  }

			// second variation of the strain
			for(int i=0; i<2; ++i)
			  for(int j=0; j<2; ++j)
			    VAR_var_epsilon[i][j] = 0.;
			for(int i=0; i<3; ++i)
			  {
			    VAR_var_epsilon[0][0] += dN_dX[i]*dM_dX[i];
			    VAR_var_epsilon[1][1] += dN_dY[i]*dM_dY[i];
			    VAR_var_epsilon[0][1] += 0.5*(dN_dX[i]*dM_dY[i] + dM_dX[i]*dN_dY[i]);
			    VAR_var_epsilon[1][0] += 0.5*(dN_dX[i]*dM_dY[i] + dM_dX[i]*dN_dY[i]);
			  }

			// update the stiffness
			colvalues[b] = 0.;
			for(int i=0; i<2; ++i)
			  for(int j=0; j<2; ++j)
			    colvalues[b] += mu*qwt*detJ*(var_epsilon[i][j]*VAR_epsilon[i][j] +
							  epsilon[i][j]*VAR_var_epsilon[i][j]);
		      }
		    
		    ierr = MatSetValues(kMat, 1, &row, ndofs_per_patch, &shpnums[0], &colvalues[0], ADD_VALUES); CHKERRV(ierr);
		    
		  } // a (residual dofs)
	      } // q (1D quadrature)
	  } // p (1D quadrature)
      } // e (patches)

    // Finish up assembly
    ierr = VecAssemblyBegin(resVec); CHKERRV(ierr);
    ierr = VecAssemblyEnd(resVec);   CHKERRV(ierr);
    ierr = MatAssemblyBegin(kMat, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(kMat,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    
    // done
    return;
  }

} // mkr::

			
