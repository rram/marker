// Sriramajayam

#include <mkr_AnnulusMap.h>
#include <cmath>
#include <cassert>

namespace mkr
{
  //! Evaluate the parametric to cartesian map
  void AnnulusMap::Evaluate(const double* xi, double* phi) const
  {
    const double R     = xi[0];
    const double theta = xi[1];
					
    phi[0] = center[0] + R*std::cos(theta);
    phi[1] = center[1] + R*std::sin(theta);

    return;
  }
  
      
  // Evaluate the Jacobian
  void AnnulusMap::Jacobian(const double* xi, double Jac[][2]) const
  {
    const double& R = xi[0];
    const double& theta = xi[1];
    Jac[0][0] =    std::cos(theta); Jac[0][1] = std::sin(theta);
    Jac[1][0] = -R*std::sin(theta); Jac[1][1] = R*std::cos(theta);
    return;
  }
  
  double AnnulusMap::Jacobian(const double* xi) const
  {
    const double& R     = xi[0];
    const double& theta = xi[1];
    return R;
  }
  
  
  // Evaluate the Hessian of a given function
  void AnnulusMap::Hessian(const double* xi, const double* df, const double d2f[][2], double Hess[][2]) const
  {
    const double& R     = xi[0];
    const double& theta = xi[1];

    const double& fr  = df[0];
    const double& ft  = df[1]; 
    const double& frr = d2f[0][0];
    const double& frt = d2f[0][1];
    const double& ftt = d2f[1][1];

    Hess[0][0] = frr;
    Hess[0][1] = frt - ft/R;
    Hess[1][0] = Hess[0][1];
    Hess[1][1] = ftt + R*fr;

    return;
  }
  
}
