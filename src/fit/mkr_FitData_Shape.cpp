// Sriramajayam

#include <mkr_FitData.h>

namespace mkr
{
  // Compute the shape constraint functional
  double FitData::ComputeShapeConstraintFunctional(const std::vector<CoordNormalPair>& shape_data,
						   const boost_rtree& rtree, 
						   const std::array<std::vector<double>,3>& dofs,
						   const CubicBSpline2D& spline, const double gamma)
  {
    double Functional = 0.;
    const int ndofs_per_patch = spline.GetNumberOfFunctionsPerPatch();
    const int nPatches = spline.GetNumberOfPatches();
    std::pair<double,double> uInt, vInt;
    std::vector<int> shpnums(ndofs_per_patch);
    std::vector<double> shpvals(ndofs_per_patch);
    double xi[2], qwt;
    double phi[3];
    const auto& xdofs = dofs[0];
    const auto& ydofs = dofs[1];
    const auto& zdofs = dofs[2];

    std::vector<boost_pointID> result{};
    double dist2;
      
    for(int e=0; e<nPatches; ++e)
      {
	spline.GetPatchIntervals(e, uInt, vInt);

	for(int p=0; p<nQuad1D; ++p)
	  {
	    xi[0] = uInt.first*qCoords1D[p] + uInt.second*(1.-qCoords1D[p]);
	    qwt   = (uInt.second-uInt.first)*qWeights1D[p];

	    for(int q=0; q<nQuad1D; ++q)
	      {
		xi[1] = vInt.first*qCoords1D[q] + vInt.second*(1.-qCoords1D[q]);
		qwt  *= (vInt.second-vInt.first)*qWeights1D[q];

		// this point
		spline.GetNonzeroFunctions(xi, shpnums, shpvals);
		phi[0] = phi[1] = phi[2] = 0.;
		for(int a=0; a<ndofs_per_patch; ++a)
		  {
		    phi[0] += xdofs[shpnums[a]]*shpvals[a];
		    phi[1] += ydofs[shpnums[a]]*shpvals[a];
		    phi[2] += zdofs[shpnums[a]]*shpvals[a];
		  }

		// closest point to phi in the shape cloud
		result.clear();
		rtree.query(boost::geometry::index::nearest(boost_point(phi[0],phi[1],phi[2]), 1), std::back_inserter(result));
		assert(static_cast<int>(result.size())==1);
		const int& indx = result[0].second;
		const auto& cpt  = shape_data[indx].first;
		const auto& nvec = shape_data[indx].second;
		//double norm = std::sqrt(phi[0]*phi[0] + phi[1]*phi[1] + phi[2]*phi[2]);
		//double cpt[] = {phi[0]/norm, phi[1]/norm, phi[2]/norm};
		//double nvec[] = {cpt[0], cpt[1], cpt[2]};

		// distance^2
		dist2 = (phi[0]-cpt[0])*(phi[0]-cpt[0]) + (phi[1]-cpt[1])*(phi[1]-cpt[1]) + (phi[2]-cpt[2])*(phi[2]-cpt[2]);
		
		// update the functional
		Functional += 0.5*gamma*qwt*dist2;

	      } // q (1d quadrature)
	  } // p (1d quadrature)
      } // e (patches)

    // done
    return Functional;
  }


  // Assemble shape constraints
  void FitData::AssembleShapeConstraints(const int compNum,
					 const std::array<std::vector<double>,3>& dofs,
					 const std::vector<CoordNormalPair>& shape_data,
					 const boost_rtree& rtree,
					 const CubicBSpline2D& spline,
					 const double gamma,
					 Vec& resVec, Mat& kMat)
  {
    PetscErrorCode ierr;
    
    const int ndofs_per_patch = spline.GetNumberOfFunctionsPerPatch();
    const int nPatches = spline.GetNumberOfPatches();
    std::pair<double,double> uInt, vInt;
    std::vector<int> shpnums(ndofs_per_patch);
    std::vector<double> shpvals(ndofs_per_patch);
    std::vector<double> colvals(ndofs_per_patch);
    
    double xi[2], qwt;
    double phi[3];
    const auto& xdofs = dofs[0];
    const auto& ydofs = dofs[1];
    const auto& zdofs = dofs[2];

    std::vector<boost_pointID> result{};
    double res;

    for(int e=0; e<nPatches; ++e)
      {
	spline.GetPatchIntervals(e, uInt, vInt);

	for(int p=0; p<nQuad1D; ++p)
	  {
	    xi[0] = uInt.first*qCoords1D[p] + uInt.second*(1.-qCoords1D[p]);
	    qwt   = qWeights1D[p]*(uInt.second-uInt.first);

	    for(int q=0; q<nQuad1D; ++q)
	      {
		xi[1] = vInt.first*qCoords1D[q] + vInt.second*(1.-qCoords1D[q]);
		qwt  *= qWeights1D[q]*(vInt.second-vInt.first);

		// This point
		spline.GetNonzeroFunctions(xi, shpnums, shpvals);
		phi[0] = phi[1] = phi[2] = 0.;
		for(int a=0; a<ndofs_per_patch; ++a)
		  {
		    phi[0] += xdofs[shpnums[a]]*shpvals[a];
		    phi[1] += ydofs[shpnums[a]]*shpvals[a];
		    phi[2] += zdofs[shpnums[a]]*shpvals[a];
		  }

		// Its closest point, normal
		result.clear();
		rtree.query(boost::geometry::index::nearest(boost_point(phi[0],phi[1],phi[2]), 1), std::back_inserter(result));
		assert(static_cast<int>(result.size())==1);
		const int& indx = result[0].second;
		const auto& cpt = shape_data[indx].first;
		const auto& nvec = shape_data[indx].second;
		//double norm = std::sqrt(phi[0]*phi[0]+phi[1]*phi[1]+phi[2]*phi[2]);
		//double cpt[] = {phi[0]/norm, phi[1]/norm, phi[2]/norm};
		//double nvec[] = {cpt[0], cpt[1], cpt[2]};

		// update residual and stiffness
		for(int a=0; a<ndofs_per_patch; ++a)
		  {
		    const int& row = shpnums[a];
		    res = gamma*qwt*(phi[compNum]-cpt[compNum])*shpvals[a];

		    // update residual
		    ierr = VecSetValues(resVec, 1, &row, &res, ADD_VALUES); CHKERRV(ierr);

		    // assemble stiffness (approximate: ignore Hessian of signed distance)
		    for(int b=0; b<ndofs_per_patch; ++b)
		      colvals[b] = gamma*qwt*nvec[compNum]*nvec[compNum]*shpvals[a]*shpvals[b];

		    // update stiffness
		    ierr = MatSetValues(kMat, 1, &row, ndofs_per_patch, &shpnums[0], &colvals[0], ADD_VALUES); CHKERRV(ierr);
		  }
		
	      } // q (1D quadrature)
	  } //p (1D quadrature)
      } // e (patches)

    // done
    ierr = VecAssemblyBegin(resVec); CHKERRV(ierr);
    ierr = VecAssemblyEnd(resVec);   CHKERRV(ierr);
    ierr = MatAssemblyBegin(kMat, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(kMat,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    return;
  }

} // mkr::
		
