// Sriramajayam

#ifndef MKR_ANNULUS_MAP_H
#define MKR_ANNULUS_MAP_H

#include <mkr_ParametricToCartesianMap.h>

namespace mkr
{
  class AnnulusMap: public ParametricToCartesianMap
  {
  public:

    //! Constructor
    inline AnnulusMap(const double* icenter)
      :center{icenter[0],icenter[1]}
      {}

	//! Destructor
	inline virtual ~AnnulusMap() {}

	//! Evaluate the parametric to cartesian map
	void Evaluate(const double* xi, double* phi) const override;
      
	// Evaluate the Jacobian
	void Jacobian(const double* xi, double Jac[][2]) const override;
	double Jacobian(const double* xi) const override;
      
	// Evaluate the Hessian of a given function
	void Hessian(const double* xi, const double* df, const double d2f[][2], double Hess[][2]) const override;

  private:
	const double center[2];
  };
}

#endif
