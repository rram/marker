# Sriramajayam

add_library(${PROJECT_NAME}_icp STATIC
  mkr_ICP.cpp)

# include
target_include_directories(${PROJECT_NAME}_icp PUBLIC
  ${OpenCV_INCLUDE_DIRS}
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>)

# link
target_link_libraries(${PROJECT_NAME}_icp PUBLIC ${OpenCV_LIBS})

# Add required flags
target_compile_features(${PROJECT_NAME}_icp PUBLIC ${mkr_COMPILE_FEATURES})

# install header files
install(FILES
  mkr_ICP.h
  DESTINATION ${PROJECT_NAME}/include)
