// Sriramajayam

#ifndef MKR_ARUCO_BOARD_H
#define MKR_ARUCO_BOARD_H

#include <opencv2/core.hpp>
#include <opencv2/aruco.hpp>
#include <map>
#include <mkr_ArucoStructs.h>
#include <str_Triangulator.h>
 
namespace mkr
{
  //! Class for creating an aruco dictionary and operating on images of aruco markers
  //! There are two main functionalities: detecting markers in a given image
  //! and matching markers in a given pair of images of the same scene from two view points.
  class ArucoSet
  {
  public:
    //! Default constructor
    //! \param[in] name Name of the dictionary to use
    ArucoSet(const cv::String name);

    //! Default destructor
    virtual ~ArucoSet();

    //! Disable copy
    ArucoSet(const ArucoSet&) = delete;
    
    //! Returns the name of the dictionary
    inline cv::String GetDictionaryName() const
    { return dictionaryName; }
    
    //! Main functionality. Creates a marker from the dictionary with the specified specs
    //! \param[in] mid Marker id requested
    //! \param[in] markerSize  Size of the marker in pixels
    //! \param[out] markerImg Output matrix
    //! \param[in] borderBits Number of bits to buffer the border
    void MakeMarker(const int mid, const int imgPixels, cv::Mat& img, const int borderBits) const;
    
    //! Main functionality. Takes an image and detectes the set of markers in it
    //! \param[in] imgName Name of the image
    //! \param[out] imgSet Detected details in the image
    void Detect(const cv::String imgName, ArucoSetImage& imgSet) const;
    
    //! Main functionality. Matches markers in a pair of images
    //! \param[in] A Image set 1
    //! \param[in] B Image set 2
    //! \param[in] tri Triangulator
    //! \param[in[ tmethod Triangulation method- symmetric or unsymmetric
    //! \param[out] AB Information about overlapping markers in the two images
    static void Match(const ArucoSetImage& A, const ArucoSetImage& B,
		      const str::Triangulator& tri,  str::TriMethod tmethod,
		      ArucoSetImagePair& AB);
    
    //! Functionality to print the set of markers and pixels to an xml file
    //! \param[in] filename Filename to print in
    //! \param[in] imgsetpair Image set pair for which to print details
    static void PrintXML(const cv::String filename, const ArucoSetImagePair& imgset);
    
    //! Visualize detected markers
    //! \param[in] imgSet Processed image set
    //! \param[in] filename Image to be printed
    static void Visualize(const ArucoSetImage& imgSet, cv::String filename);
    
    //! Visualize overlapping markers
    //! \param[in] imgPair Processed image pair
    //! \param[in] file1 Image 1 in which to print
    //! \param[in] file2 Image 2 in which to print
    //! \param[in] file3 File in which to print triangulated XYZ coordinates
    static void Visualize(const ArucoSetImagePair& imgPair,
			  const cv::String file1,
			  const cv::String file2,
			  const cv::String file3);

  private:
    cv::String dictionaryName;        //!< Name of the dictionary
    cv::Ptr<cv::aruco::Dictionary> dictionary; //!< Aruco dictionary

    //! For convenience, use direct string names for marker dictionaries instead of dictionary ids
    static const std::map<cv::String, cv::aruco::PredefinedDictionaryType> dictionaryMap;
  };
}


#endif
