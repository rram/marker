// Sriramajayam

#ifndef MKR_ARUCO_STRUCTS_H
#define MKR_ARUCO_STRUCTS_H

#include <opencv2/core.hpp>
#include <vector>
#include <array>
#include <str_Triangulator.h>
 
namespace mkr
{
  //! Helper struct to collate information about an image of aruco markers
  struct ArucoSetImage
  {
    cv::String imgName; //!< Name of the image
    cv::String dictionaryName; //!< Dictionary id for aruco markers
    std::vector<int> markerIDs; //!< List of markers detected in this image
    std::vector<std::vector<cv::Point2f>> markerCorners; //!< Matrix of detected corners
    int nMarkers; //!< Number of markers detected

    //! Default constructor
    inline ArucoSetImage()
      :imgName(""), dictionaryName(""),
      markerIDs({}), markerCorners({}),
      nMarkers(0) {}
    
    //! Check parameters
    inline void Check() const
    {
      CV_Assert(nMarkers>0);
      CV_Assert(nMarkers==static_cast<int>(markerIDs.size()) &&
		nMarkers==static_cast<int>(markerCorners.size()));
      for(auto& m:markerCorners)
	CV_Assert(static_cast<int>(m.size())==4);
      return;
    }
  };

  //! Helper struct to collate information about a pair of images of aruco markers
  struct ArucoSetImagePair
  {
    cv::String AimgName; //!< Name of image 1
    cv::String BimgName; //!< Name of image 2
    cv::String dictionaryName; //!< Dictionary used for the two
    std::vector<int> markerIDs; //!< Set of overlapping markers
    std::vector<std::vector<cv::Point2f>> Acorners; //!< Corresponding corners in image A
    std::vector<std::vector<cv::Point2f>> Bcorners; //!< Corresponding corners in image B
    const str::Triangulator* Tri; //!< Triangulator
    std::vector<std::vector<cv::Point3d>> CornerPts; //!< Triangulated points
    int nMarkers; //!< Number of overlapping markers

    //! Default constructor
    inline ArucoSetImagePair()
      :AimgName(""), BimgName(""), dictionaryName(""),
      markerIDs({}), Acorners({}), Bcorners({}), 
      Tri(nullptr), CornerPts({}), nMarkers(0) {}
    
    //! Check parameters
    inline void Check() const
    {
      CV_Assert(nMarkers>0);
      CV_Assert(nMarkers==static_cast<int>(markerIDs.size()) &&
		nMarkers==static_cast<int>(Acorners.size()) &&
		nMarkers==static_cast<int>(Bcorners.size()) &&
		nMarkers==static_cast<int>(CornerPts.size()));
      for(int n=0; n<nMarkers; ++n)
	CV_Assert(static_cast<int>(Acorners[n].size())==4 &&
		  static_cast<int>(Bcorners[n].size())==4 &&
		  static_cast<int>(CornerPts[n].size()));
      CV_Assert(Tri!=nullptr);
      return;
    }
      
  };

  //! Helper struct to encapsulate one pose of an aruco surface.
  //! The image may contain surface and background markers
  struct ArucoPose
  {
    ArucoSetImagePair surf; //!< Information about surface markers
    ArucoSetImagePair bg;   //!< Information about background markers

    //! Check integratity of members
    inline void Check() const
    { surf.Check();
      bg.Check();
      return; }
  };

  //! Struct representing an isometry
  struct Isometry
  {
    double Rot[3][3]; //!< Rotation matrix
    double Tvec[3];   //!< Translation vector

    //! Constructor.
    //! Initializes to the identity transformation
    inline Isometry()
    { for(int i=0; i<3; ++i)
	{ Tvec[i] = 0.;
	  for(int j=0; j<3; ++j)
	    Rot[i][j] = 0.;
	  Rot[i][i] = 1.; } }
  };
  
  //! Helper struct to encapsulate marker coordinates and IDs on the surface and background
  struct MergedArucoPose
  {
    std::vector<std::vector<cv::Point3d>> surfMarkerCoords;
    std::vector<int> surfMarkerIDs;
    std::vector<std::vector<cv::Point3d>> bgMarkerCoords;
    std::vector<int> bgMarkerIDs;
    int nSurfMarkers, nbgMarkers;

    //! Constructor
    inline MergedArucoPose()
      :surfMarkerCoords({}), surfMarkerIDs({}),
      bgMarkerCoords({}), bgMarkerIDs({}),
      nSurfMarkers(0), nbgMarkers(0) {}

    //! Checks integrity of parameters
    inline void Check()
    { CV_Assert(nSurfMarkers==static_cast<int>(surfMarkerCoords.size()) &&
		nSurfMarkers==static_cast<int>(surfMarkerIDs.size()));
      CV_Assert(nbgMarkers==static_cast<int>(bgMarkerCoords.size()) &&
		nbgMarkers==static_cast<int>(bgMarkerIDs.size()));
      for(auto& m:surfMarkerCoords)
	CV_Assert(static_cast<int>(m.size())==4);
      for(auto& m:bgMarkerCoords)
	CV_Assert(static_cast<int>(m.size())==4);
      return; }
  };
  
}

#endif
