// Sriramajayam

#include <mkr_ArucoSet.h>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <algorithm>
#include <set>
#include <iostream>

namespace mkr
{
  // Map from dictionary names to dictionary ids
  const std::map<cv::String, cv::aruco::PredefinedDictionaryType>
  ArucoSet::dictionaryMap =
    {{"DICT_4X4_50",   cv::aruco::DICT_4X4_50},
     {"DICT_4X4_100",  cv::aruco::DICT_4X4_100},
     {"DICT_4X4_250",  cv::aruco::DICT_4X4_250},
     {"DICT_4X4_1000", cv::aruco::DICT_4X4_1000},
     {"DICT_5X5_50",   cv::aruco::DICT_5X5_50},
     {"DICT_5X5_100",  cv::aruco::DICT_5X5_100}, 	
     {"DICT_5X5_250",  cv::aruco::DICT_5X5_250}, 	
     {"DICT_5X5_1000", cv::aruco::DICT_5X5_1000},
     {"DICT_6X6_50",   cv::aruco::DICT_6X6_50}, 	
     {"DICT_6X6_100",  cv::aruco::DICT_6X6_100}, 	
     {"DICT_6X6_250",  cv::aruco::DICT_6X6_250}, 	
     {"DICT_6X6_1000", cv::aruco::DICT_6X6_1000}, 	
     {"DICT_7X7_50",   cv::aruco::DICT_7X7_50}, 	
     {"DICT_7X7_100",  cv::aruco::DICT_7X7_100}, 	
     {"DICT_7X7_250",  cv::aruco::DICT_7X7_250}, 	
     {"DICT_7X7_1000", cv::aruco::DICT_7X7_1000}}; 

  // Constructor
  ArucoSet::ArucoSet(const cv::String dname)
    :dictionaryName(dname)
  {
    // get the dictionary id
    auto it = dictionaryMap.find(dname);
    CV_Assert(it!=dictionaryMap.end() && "ArucoSet- unexpected dictionary name");

    // Create the dictionary
    dictionary = cv::Ptr<cv::aruco::Dictionary>(new cv::aruco::Dictionary(cv::aruco::getPredefinedDictionary(it->second)));
  }

  // Destructor. Does nothing
  ArucoSet::~ArucoSet() {}

  // Main functionality. Creates a marker from the dictionary with the specified specs
  void ArucoSet::MakeMarker(const int mid, const int markerSize, 
			    cv::Mat& markerImg, const int borderBits) const
  {
    cv::aruco::generateImageMarker(*dictionary, mid, markerSize, markerImg, borderBits);
    return;
  }


  // Main functionality. Detects markers in a given image
  void ArucoSet::Detect(const cv::String imgName, ArucoSetImage& imgSet) const
  {
    imgSet.imgName = imgName;
    imgSet.dictionaryName = dictionaryName;
    imgSet.markerIDs.clear();
    imgSet.markerCorners.clear();

    // Load the image
    cv::Mat imgMat = cv::imread(imgSet.imgName, cv::IMREAD_GRAYSCALE);
    CV_Assert(imgMat.empty()==false && "ArucoSet::Detect- could not open image");
    
    // Detector parameters. Use corner refinement
    cv::Ptr<cv::aruco::DetectorParameters> detectorParams(new cv::aruco::DetectorParameters());
    detectorParams->cornerRefinementMethod = cv::aruco::CORNER_REFINE_SUBPIX;
    detectorParams->adaptiveThreshWinSizeMax = 100;
    //detectorParams->minMarkerPerimeterRate = 0.01;
    //detectorParams->maxMarkerPerimeterRate = 0.25;

    // Detect
    cv::aruco::detectMarkers(imgMat, dictionary, imgSet.markerCorners, imgSet.markerIDs, detectorParams);
    imgSet.nMarkers = static_cast<int>(imgSet.markerIDs.size());
    imgSet.Check();

    // done
    return;
  }


  // Main functionality. Matches markers in a pair of images
  void ArucoSet::Match(const ArucoSetImage& A, const ArucoSetImage& B,
		       const str::Triangulator& Tri, str::TriMethod tmethod,
		       ArucoSetImagePair& AB)
  {
    // Sanity checks
    A.Check();
    B.Check();
    CV_Assert(A.dictionaryName==B.dictionaryName  && "ArucoSet::Match- incorrect dictionary");

    // Assign the common dictionary name 
    AB.dictionaryName = A.dictionaryName;

    // Assign image names
    AB.AimgName = A.imgName;
    AB.BimgName = B.imgName;
    
    // Markers are not necessarily in sorted order. Use sets.
    std::set<int> Amarkers(A.markerIDs.begin(), A.markerIDs.end());
    std::set<int> Bmarkers(B.markerIDs.begin(), B.markerIDs.end());
    
    // Overlapping set of markers
    AB.markerIDs.clear();
    std::set_intersection(Amarkers.begin(), Amarkers.end(), 
			  Bmarkers.begin(), Bmarkers.end(),
			  std::back_inserter(AB.markerIDs));
    AB.nMarkers = static_cast<int>(AB.markerIDs.size());
    CV_Assert(AB.nMarkers>0 && "ArucoSet::Match- no overlapping markers found");
    
    // Copy the set of matching corners.
    AB.Acorners.clear();
    AB.Bcorners.clear();
    
    // Create map for markerID->index for the two images
    std::map<int, int> Ainvmap, Binvmap;
    for(int i=0; i<A.nMarkers; ++i) Ainvmap[A.markerIDs[i]] = i;
    for(int i=0; i<B.nMarkers; ++i) Binvmap[B.markerIDs[i]] = i;
    for(auto& markernum:AB.markerIDs)
      {
	// Find the index of this marker in image A
	auto itA = Ainvmap.find(markernum);
	CV_Assert(itA!=Ainvmap.end() && "ArucoSet::Match- could not find marker in image A");
	int aindx = itA->second;

	// Find the index of this marker in image B
	auto itB = Binvmap.find(markernum);
	CV_Assert(itB!=Binvmap.end() && "ArucoSet::Match- could not find marker in image B");
	int bindx = itB->second;

	// Copy corners for images A and B
	AB.Acorners.push_back( A.markerCorners[aindx] );
	AB.Bcorners.push_back( B.markerCorners[bindx] );
      }


    // Triangulate
    AB.Tri = &Tri;
    AB.CornerPts.resize(AB.nMarkers, std::vector<cv::Point3d>(4));
    for(int i=0; i<AB.nMarkers; ++i)
      {
	// This marker in A
	const auto& mA = AB.Acorners[i];
	
	// This marker in B
	const auto& mB = AB.Bcorners[i];

	// This triangulated marker
	auto& mAB = AB.CornerPts[i];
	
	// Triangulate each corner
	for(int j=0; j<4; ++j)
	  if(tmethod==str::TriMethod::Sym) // Aid template deduction 
	    Tri.Triangulate<str::TriMethod::Sym>(mA[j], mB[j], mAB[j]);
	  else
	    Tri.Triangulate<str::TriMethod::Unsym>(mA[j], mB[j], mAB[j]);
      }
    AB.Check();
      
    // done
    return;
  }


  
  // Functionality to print the set of markers and pixels in an aruco set image pair to an xml file
  void ArucoSet::PrintXML(const cv::String filename, const ArucoSetImagePair& imgset)
  {
    imgset.Check();
    const auto& nMarkers = imgset.nMarkers;
    
    // Convert 2D vector of corners to Mat form
    cv::Mat_<double> Amat(4*nMarkers, 2);
    cv::Mat_<double> Bmat(4*nMarkers, 2);
    cv::Mat_<double> ABmat(4*nMarkers, 3);
    cv::Mat_<int> ids(nMarkers,1);
    for(int i=0; i<nMarkers; ++i)
      {
	ids.at<int>(i,0) = imgset.markerIDs[i];
	for(int j=0; j<4; ++j)
	  {
	    Amat.at<double>(4*i+j,0) = imgset.Acorners[i][j].x;
	    Amat.at<double>(4*i+j,1) = imgset.Acorners[i][j].y;
	    Bmat.at<double>(4*i+j,0) = imgset.Bcorners[i][j].x;
	    Bmat.at<double>(4*i+j,1) = imgset.Bcorners[i][j].y;
	    ABmat.at<double>(4*i+j,0) = imgset.CornerPts[i][j].x;
	    ABmat.at<double>(4*i+j,1) = imgset.CornerPts[i][j].y;
	    ABmat.at<double>(4*i+j,2) = imgset.CornerPts[i][j].z;
	  }
      }
	     
    cv::FileStorage fs(filename, cv::FileStorage::WRITE);
    CV_Assert(fs.isOpened() && "ArucoSet::Print- Could not open file to write");
    fs.writeComment("Pixel set 1", 0);
    fs<<"pixel_set_1"<<Amat;
    fs.writeComment("Pixel set 2", 0);
    fs<<"pixel_set_2"<<Bmat;
    fs.writeComment("Triangulated points", 0);
    fs<<"points"<<ABmat;
    fs.writeComment("IDs of markers for corners listed above", 0);
    fs<<"marker_ids"<<ids;
    fs.release();
    return;
  }


  // Visualize detected markers
  void ArucoSet::Visualize(const ArucoSetImage& imgSet, cv::String filename)
  {
    // Sanity checks
    imgSet.Check();

    // Visualize
    cv::Mat img = cv::imread(imgSet.imgName, cv::IMREAD_GRAYSCALE);
    CV_Assert(img.empty()==false && "ArucoSet::Detect- could not open image");
    cv::cvtColor(img, img, cv::COLOR_GRAY2BGR);
    cv::aruco::drawDetectedMarkers(img, imgSet.markerCorners, imgSet.markerIDs);
    cv::imwrite(filename, img);
    return;
  }


  // Visualize overlapping markers
  void ArucoSet::Visualize(const ArucoSetImagePair& imgPair,
			   const cv::String file1, const cv::String file2,
			   const cv::String file3)
  {
    // Sanity checks
    imgPair.Check();
    
    // Create dummy image sets for visualizing each image
    ArucoSetImage imgSet;
    imgSet.dictionaryName = imgPair.dictionaryName;
    imgSet.markerIDs = imgPair.markerIDs;
    imgSet.nMarkers = imgPair.nMarkers;

    // Visualize image set 1
    imgSet.imgName = imgPair.AimgName;
    imgSet.markerCorners = imgPair.Acorners;
    ArucoSet::Visualize(imgSet, file1);

    // Visualize image set 2
    imgSet.imgName = imgPair.BimgName;
    imgSet.markerCorners = imgPair.Bcorners;
    ArucoSet::Visualize(imgSet, file2);

    // Print triangulated points to file
    std::fstream pfile;
    pfile.open(file3.c_str(), std::ios::out);
    CV_Assert(pfile.good());
    for(auto& m:imgPair.CornerPts)
      {
	for(auto& c:m)
	  pfile<<c.x<<" "<<c.y<<" "<<c.z<<"\n";
	pfile.flush();
      }
    pfile.close();

    // done
    return;
  }


}
