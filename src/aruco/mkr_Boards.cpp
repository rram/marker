// Sriramajayam

#include <mkr_Boards.h>
#include <mkr_ArucoSet.h>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>

namespace mkr
{
  // Checks specification of a rectangular board
  void RectangularBoardSpecs::Check() const
  {
    CV_Assert(dictionaryName.size()>0);
    CV_Assert(nX>0 && nY>0);
    CV_Assert(markerSize>0 && borderBits>0 && 2*borderBits<markerSize);
    CV_Assert(spacing>0);
  }
  

  // Anonymous namespaces for specific oracles
  namespace
  {
    // Trivial oracle: always returns true
    bool TrivialOracle(const int* cnrs, const int N, const int M, void* usr)
    { return true; }

    
    // Oracle for an annulus
    bool AnnulusOracle(const int* cnrs, const int N, const int M, void* usr)
    {
      CV_Assert(usr!=nullptr);
      const int* radii = static_cast<const int*>(usr); // Inner and outer radius
      for(int c=0; c<4; ++c)
	{
	  int rad2 = (cnrs[2*c]-N/2)*(cnrs[2*c]-N/2) + (cnrs[2*c+1]-M/2)*(cnrs[2*c+1]-M/2);
	  if(rad2<radii[0]*radii[0] || rad2>radii[1]*radii[1])
	    return false;
	}
      return true;
    } 
  }


  
  // Function to create a rectangular grid of aruco markers
  void CreateBoard(const RectangularBoardSpecs& specs,
		   const cv::String imgName)
  {
    MarkerBoardOracle oracle(TrivialOracle);
    CreateBoard(specs, oracle, nullptr, imgName);
  }
  
  
  // Function to create an annular board of aruco markers
  void CreateBoard(const RectangularBoardSpecs& specs,
		   const int inRad, const int outRad,
		   const cv::String imgName)
  {
    int radii[] = {inRad, outRad};
    MarkerBoardOracle oracle(AnnulusOracle);
    CreateBoard(specs, oracle, radii, imgName);
  }

  
  // Function to create a customized board of aruco markers
  void CreateBoard(const RectangularBoardSpecs& specs,
		   MarkerBoardOracle oracle, void* usr,
		   const cv::String imgName)
  {
    // Check the specs
    specs.Check();

    // Compute the size of the image required
    const int N = (specs.markerSize+specs.spacing)*specs.nX + specs.spacing;
    const int M = (specs.markerSize+specs.spacing)*specs.nY + specs.spacing;
    cv::Mat_<uchar> img(N, M, static_cast<uchar>(255));
    cv::Mat markerImage;

    // ArucoSet object to access marker dictionary
    ArucoSet aset(specs.dictionaryName);

    // Print markers to image row by row
    int origin[2] = {specs.spacing, specs.spacing}; // Coordinates of the top left corner of markers
    int markerID = 0;
    for(int i=0; i<specs.nX; ++i)
      {
	for(int j=0; j<specs.nY; ++j)
	  {
	    // Corners of this propsective marker
	    int cnrs[] = {origin[0],origin[1],
			  origin[0]+specs.markerSize,origin[1]+specs.markerSize,
			  origin[0]+specs.markerSize,origin[1],
			  origin[0],origin[1]+specs.markerSize};
	    
	    // Request the oracle for permission
	    if(oracle(cnrs, N, M, usr))
	      {
		// Get a marker for this row, column
		aset.MakeMarker(markerID++, specs.markerSize, markerImage, specs.borderBits);
		// Place the marker into the parent image
		for(int a=0; a<specs.markerSize; ++a)
		  for(int b=0; b<specs.markerSize; ++b)
		    img.at<uchar>(origin[0]+a, origin[1]+b) = markerImage.at<uchar>(a, b);
	      }

	    // Update the origin
	    origin[1] += specs.markerSize+specs.spacing;
	  }
	
	// Update the origin
	origin[0] += specs.markerSize + specs.spacing;
	origin[1] = specs.spacing;
      }
    
    // Save the final image
    cv::imwrite(imgName, img);
    std::cout<<"\nNumber of markers used: "<<markerID<<std::flush;
    
    // --done--
    return;
  }
  
}
