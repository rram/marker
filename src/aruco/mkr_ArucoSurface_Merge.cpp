// Sriramajayam

#include <mkr_ArucoSurface.h>
#include <mkr_ICP.h>
#include <map>
#include <set>
#include <algorithm>
#include <iostream>

namespace mkr
{
  // anonymous namespace for graph-related functionalities
  namespace
  {
    // Convert the merge sequence into a directed graph (map from source pose to target pose)
    void MakeGraph(const std::vector<std::pair<int,int>>& mergeSeq, std::map<int,int>& graph)
    {
      graph.clear();
      for(auto& edge:mergeSeq)
	{
	  const int& src = edge.first;
	  const int& dest = edge.second;
	  CV_Assert(graph.find(src)==graph.end());
	  graph[src] = dest;
	}
    }

    // Identify a terminal node in a graph (a node that is not a destination)
    int GetTerminalNode(const std::map<int,int>& graph)
    {
      // Collect the set of all sources and destinations
      std::set<int> src({});
      std::set<int> dest({});
      for(auto& edge:graph)
	{ src.insert(edge.first);
	  dest.insert(edge.second); }
      CV_Assert(src.size()==graph.size());
      
      // Identify src nodes that are not in the destination list
      std::set<int> leafNodes({});
      std::set_difference(src.begin(), src.end(), dest.begin(), dest.end(),
			  std::inserter(leafNodes, leafNodes.begin()));

      // Return the first leaf node
      return *leafNodes.begin();
    }

    // Remove a node from a graph
    void PruneGraph(const int key, std::map<int,int>& graph)
    {
      auto it = graph.find(key);
      CV_Assert(it!=graph.end());
      graph.erase(it);
      return;
    }

    // Check a graph:
    // (i) the target pose should not be a source
    // (ii) all poses except the target pose should be a source node
    // (iii) all poses should be connected to the target pose
    void CheckGraph(const std::map<int,int>& graph, const int nPoses, const int tgtPose)
    {
      // Nothing to check if the graph is empty
      if(graph.empty()) return;

      std::cout<<"\nChecking graph of merge sequence..."<<std::flush;
      
      // Target pose should not be a source node
      CV_Assert(graph.find(tgtPose)==graph.end() && "Inconsistent merge sequence");

      // All poses except the target pose should be a source node
      std::set<int> sources({});
      for(auto& edge:graph)
	{
	  CV_Assert((edge.first>=0 && edge.first<nPoses) && "Inconsistent merge sequence");
	  CV_Assert((edge.second>=0 && edge.second<nPoses) && "Inconsistent merge sequence");
	  CV_Assert(edge.first!=edge.second && "Inconsistent merge sequence");
	  sources.insert(edge.first);
	}
      CV_Assert(static_cast<int>(sources.size())==nPoses-1 && "Inconsistent merge sequence");

      // Check connectedness to the target pose & for loops
      for(int pose=0; pose<nPoses; ++pose)
	if(pose!=tgtPose)
	  {
	    auto it = graph.find(pose);
	    bool flag = false;
	    while(it!=graph.end())
	      {
		CV_Assert(it->second!=pose && "Inconsistent merge sequence"); // This is a loop
		if(it->second==tgtPose) flag = true; // this pose is connected to the target
		it = graph.find(it->second); // Proceed to the next node
	      }
	    // Terminal node should be the target pose
	    CV_Assert(flag==true && "Inconsistent merge sequence");
	  }
      
      // done
      return;
    }


    // Convert an ArucoPose object to a MergedArucoPose object
    void ConvertArucoPoseToMergedArucoPose(const ArucoPose& apose,
					   MergedArucoPose& mpose)
    {
      // Copy surface markers
      mpose.surfMarkerCoords = apose.surf.CornerPts;
      mpose.surfMarkerIDs = apose.surf.markerIDs;
      mpose.nSurfMarkers = apose.surf.nMarkers;

      // Copy bg markers
      mpose.bgMarkerCoords = apose.bg.CornerPts;
      mpose.bgMarkerIDs = apose.bg.markerIDs;
      mpose.nbgMarkers = apose.bg.nMarkers;

      mpose.Check();
      return;
    }


    // Helper method to identify common markers
    // A[Aindx[i]] = B[Bindx[i]].
    void GetMarkerCorrespondences(const std::vector<int>& A, const std::vector<int>& B,
				  std::vector<int>& Aindx, std::vector<int>& Bindx)
    {
      Aindx.clear(); Bindx.clear();
      const int nAmarkers = static_cast<int>(A.size());
      const int nBmarkers = static_cast<int>(B.size());

      //CV_Assert(nAmarkers+nBmarkers>0 && "GetMarkerCorrespondence- Unexpected number of markers");
      if(nAmarkers==0 || nBmarkers==0) return;
    
      // Create mapping from markers->index for list B
      std::map<int, int> Bmarker2indx;
      for(int i=0; i<nBmarkers; ++i)
	Bmarker2indx[B[i]] = i;

      // Detect coincident markers
      for(int i=0; i<nAmarkers; ++i)
	{
	  int  mnum = A[i];
	  // Does 'mnum' appear in list B?
	  auto it = Bmarker2indx.find(mnum);
	  if(it!=Bmarker2indx.end())
	    {
	      Aindx.push_back( i );
	      Bindx.push_back( it->second );
	    }
	}

      // Sanity check
      const int nCommon = static_cast<int>(Aindx.size());
      assert(static_cast<int>(Bindx.size())==nCommon &&
	     "GetMarkerCorrespondences- Unexpected scenario");
      
      for(int i=0; i<nCommon; ++i)	
	assert(A[Aindx[i]]==B[Bindx[i]] &&
	       "GetMarkerCorrespondences- Unexpected scenario");

      // Done
      return;
    }

    
    // Compute the transformation mapping a source pose to a target pose
    // 1. Identify surf/bg markers of source pose in common with the target
    // 2. Collate the list of coordinates of common marker corners
    // 3. Use ICP to compute the isometry mapping the source to the target
    void ComputePoseTransformation(const MergedArucoPose& srcPose,
				   const MergedArucoPose& tgtPose,
				   Isometry& iso)
    {
      // Identify common markers
      std::vector<int> src_surf_indx({}), tgt_surf_indx({});
      GetMarkerCorrespondences(srcPose.surfMarkerIDs, tgtPose.surfMarkerIDs,
			       src_surf_indx, tgt_surf_indx);
      std::vector<int> src_bg_indx({}), tgt_bg_indx({});
      GetMarkerCorrespondences(srcPose.bgMarkerIDs, tgtPose.bgMarkerIDs,
			       src_bg_indx, tgt_bg_indx);

      // Collate a list of corresponding source and target coordinates for ICP
      std::vector<cv::Point3d> src_Pts({}), tgt_Pts({});

      // Surface markers
      const int nSurfMarkers = static_cast<int>(src_surf_indx.size());
      for(int i=0; i<nSurfMarkers; ++i)
	{
	  const int& n = src_surf_indx[i]; // Index in srcPose
	  const int& m = tgt_surf_indx[i]; // Index in tgtPose
	  CV_Assert(srcPose.surfMarkerIDs[n]==tgtPose.surfMarkerIDs[m]); // This marker
	  for(int j=0; j<4; ++j)
	    {
	      src_Pts.push_back( srcPose.surfMarkerCoords[n][j] );
	      tgt_Pts.push_back( tgtPose.surfMarkerCoords[m][j] );
	    }
	}

      // Background markers
      const int nbgMarkers = static_cast<int>(src_bg_indx.size());
      for(int i=0; i<nbgMarkers; ++i)
	{
	  const int& n = src_bg_indx[i]; // Index in srcPose
	  const int& m = tgt_bg_indx[i]; // Index in tgtPose
	  CV_Assert(srcPose.bgMarkerIDs[n]==tgtPose.bgMarkerIDs[m]); // This marker
	  for(int j=0; j<4; ++j)
	    {
	      src_Pts.push_back( srcPose.bgMarkerCoords[n][j] );
	      tgt_Pts.push_back( tgtPose.bgMarkerCoords[m][j] );
	    }
	}

      // Use ICP to map srcPose to tgtPose
      ComputeIsometry(tgt_Pts, src_Pts, iso.Rot, iso.Tvec);

      // Print the number of common markers for reference
      std::cout<<"\nNumber of common markers: surf: "<<nSurfMarkers<<", bg: "<<nbgMarkers<<std::flush;
      // -- done --
      return;
    }
    
    
    // Merge one pose into another
    // Compute the transformation from srcPose to tgtPose
    // Transform all surface and background markers of srcPose
    // Append all transformed srcPose markers into the target
    void MergePose(const MergedArucoPose& srcPose,
    		   MergedArucoPose& tgtPose)
    {
      // Get the transformation from srcPose to tgtPose
      Isometry iso;
      ComputePoseTransformation(srcPose, tgtPose, iso);
      
      // Map from marker -> index in the target pose
      std::map<int, int> tgt_surf_marker2IndexMap({});
      for(int i=0; i<tgtPose.nSurfMarkers; ++i)
	tgt_surf_marker2IndexMap[tgtPose.surfMarkerIDs[i]] = i;
      std::map<int, int> tgt_bg_marker2IndexMap({});
      for(int i=0; i<tgtPose.nbgMarkers; ++i)
	tgt_bg_marker2IndexMap[tgtPose.bgMarkerIDs[i]] = i;

      // Transform srcPose markers and append them to the tgtPose

      // Surface markers
      for(int i=0; i<srcPose.nSurfMarkers; ++i)
	{
	  // Transform coordinates of corners of this marker in srcPose
	  std::vector<cv::Point3d> scoords(4);
	  for(int j=0; j<4; ++j)
	    {
	      // This corner of the marker in srcPose
	      const auto& pt = srcPose.surfMarkerCoords[i][j];
	      const double X[] = {pt.x, pt.y, pt.z};

	      // Transform coordinates
	      double Y[] = {0., 0., 0.};
	      for(int k=0; k<3; ++k)
		{ for(int L=0; L<3; ++L)
		    Y[k] += iso.Rot[k][L]*X[L];
		  Y[k] += iso.Tvec[k]; }

	      scoords[j].x = Y[0];
	      scoords[j].y = Y[1];
	      scoords[j].z = Y[2];
	    }

	  // Append or update in tgtPose?
	  const int& marker = srcPose.surfMarkerIDs[i];
	  auto it = tgt_surf_marker2IndexMap.find(marker);
	  if(it==tgt_surf_marker2IndexMap.end()) // Append
	    {
	      ++tgtPose.nSurfMarkers;
	      tgtPose.surfMarkerIDs.push_back(marker);
	      tgtPose.surfMarkerCoords.push_back(scoords);
	    }
	  else // Update, i.e., average
	    {
	      const int& indx = it->second;
	      const auto& tcoords = tgtPose.surfMarkerCoords[indx];
	      for(int j=0; j<4; ++j)
		{
		  tgtPose.surfMarkerCoords[indx][j].x = 0.5*(scoords[j].x+tcoords[j].x);
		  tgtPose.surfMarkerCoords[indx][j].y = 0.5*(scoords[j].y+tcoords[j].y);
		  tgtPose.surfMarkerCoords[indx][j].z = 0.5*(scoords[j].z+tcoords[j].z);
		}
	    }
	}

      // Background markers
      for(int i=0; i<srcPose.nbgMarkers; ++i)
	{
	  // Transform coordinates of corners of this marker in srcPose
	  std::vector<cv::Point3d> scoords(4);
	  for(int j=0; j<4; ++j)
	    {
	      const auto& pt = srcPose.bgMarkerCoords[i][j];
	      const double X[] = {pt.x, pt.y, pt.z};

	      // Transform
	      double Y[] = {0.,0.,0.};
	      for(int k=0; k<3; ++k)
		{ for(int L=0; L<3; ++L)
		    Y[k] += iso.Rot[k][L]*X[L];
		  Y[k] += iso.Tvec[k]; }

	      scoords[j].x = Y[0];
	      scoords[j].y = Y[1];
	      scoords[j].z = Y[2];
	    }

	  // Append or update in tgtPose
	  const int& marker = srcPose.bgMarkerIDs[i]; // This marker
	  auto it = tgt_bg_marker2IndexMap.find(marker);
	  if(it==tgt_bg_marker2IndexMap.end()) // Append
	    {
	      ++tgtPose.nbgMarkers;
	      tgtPose.bgMarkerIDs.push_back(marker);
	      tgtPose.bgMarkerCoords.push_back(scoords);
	    }
	  else // Update
	    {
	      const int& indx = it->second;
	      const auto& tcoords = tgtPose.bgMarkerCoords[indx];
	      for(int j=0; j<4; ++j)
		{
		  tgtPose.bgMarkerCoords[indx][j].x = 0.5*(scoords[j].x+tcoords[j].x);
		  tgtPose.bgMarkerCoords[indx][j].y = 0.5*(scoords[j].y+tcoords[j].y);
		  tgtPose.bgMarkerCoords[indx][j].z = 0.5*(scoords[j].z+tcoords[j].z);
		}
	    }
	}

      // -- done --
      return;
    }
   
    
  } // End anonymous namespace



  // Merge all poses
  // Create a "MergedPose" object for each pose
  // Create a graph of the merge sequence
  // Recursive loop until the graph is empty:
  // (i) Identify a terminal node in the current graph
  // (ii) Merge source pose into the destination pose
  // (iii) Erase the <source,destination> edge from the graph
  void ArucoSurface::MergePoses()
  {
    // merge only once
    CV_Assert(is_merged==false && "ArucoSurface::MergePoses- Cannot merge poses again");
    
    // Convert merge sequence into a graph format
    std::map<int,int> graph;
    MakeGraph(mergeSequence, graph);

    // Sanity check
    CheckGraph(graph, nPoses, tgtPose);

    // Create a "MergedPose" version of each pose
    std::vector<MergedArucoPose> mposes(nPoses);
    for(int p=0; p<nPoses; ++p)
      ConvertArucoPoseToMergedArucoPose(arPoses[p], mposes[p]);
    
    // Merge 1-by-1
    while(!graph.empty())
      {
	// Identify a terminal node
	const int src_pose_num = GetTerminalNode(graph);

	// Identify the destination node
	auto it = graph.find(src_pose_num);
	CV_Assert(it!=graph.end());
	const int dest_pose_num = it->second;

	// Merge source pose into the destination pose
	std::cout<<"\n\nMerging pose "<<src_pose_num<<" into "<<dest_pose_num<<std::flush;
	MergePose(mposes[src_pose_num], mposes[dest_pose_num]);
	
	// Erase the src_pose node from the graph
	PruneGraph(src_pose_num, graph);

	// Visualization for debugging
	if(debug_folder!="")
	  {
	    cv::String fname;
	    std::fstream pfile;
	    
	    // surface
	    fname = debug_folder + "/surf-" + std::to_string(src_pose_num) + "-" + std::to_string(dest_pose_num) + ".xyz";
	    pfile.open(fname.c_str(), std::ios::out);
	    for(auto& ptvec:mposes[dest_pose_num].surfMarkerCoords)
	      for(auto& pt:ptvec)
		pfile<<pt.x<<" "<<pt.y<<" "<<pt.z<<"\n";
	    pfile.flush();
	    pfile.close();

	    // background
	    fname = debug_folder + "/bg-" + std::to_string(src_pose_num) + "-" + std::to_string(dest_pose_num) + ".xyz";
	    pfile.open(fname.c_str(), std::ios::out);
	    for(auto& ptvec:mposes[dest_pose_num].bgMarkerCoords)
	      for(auto& pt:ptvec)
		pfile<<pt.x<<" "<<pt.y<<" "<<pt.z<<"\n";
	    pfile.flush();
	    pfile.close();
	  }
      }

    // Save the final merged pose
    mergedPose.nSurfMarkers = mposes[tgtPose].nSurfMarkers;
    mergedPose.nbgMarkers = mposes[tgtPose].nbgMarkers;
    mergedPose.surfMarkerIDs = mposes[tgtPose].surfMarkerIDs;
    mergedPose.bgMarkerIDs = mposes[tgtPose].bgMarkerIDs;
    mergedPose.surfMarkerCoords = mposes[tgtPose].surfMarkerCoords;
    mergedPose.bgMarkerCoords = mposes[tgtPose].bgMarkerCoords;
    mergedPose.Check();

    // Note that poses have been merged
    is_merged = true;
    
    // -- done --
  }
  
} // End namespace mkr
    
