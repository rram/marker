// Sriramajayam

#ifndef MKR_ARUCO_SURFACE_H
#define MKR_ARUCO_SURFACE_H

#include <mkr_ArucoSet.h>
#include <set>
#include <str_Triangulator.h>

namespace mkr
{
  //! An aruco surface is an object whose surface is marked by aruco markers.
  //! The images used to reconstruct the object may also have aruco markers.
  //! An aruco surface may be imaged from a number of angles. Therefore, a number of
  //! poses can be specified. Each pose is assumed to be imaged with the same stereo setup.
  //! An aruco surface is reconstructed by merging marker coordinates from all poses available
  //! Merging of pose data is done with reference to the first pose
  class ArucoSurface
  {
  public:
    //! Template xml file
    static void CreateTemplate(const cv::String xmlfile);
    
    //! Constructor
    //! \param[in] xmlfile Template file with parameters
    //! \param[in] tri Triangulator
    //! \param[in] tmethod Triangulation method
    ArucoSurface(const cv::String xmlfile, const str::Triangulator& tri, const str::TriMethod tmethod);
    
    //! Destructor
    inline virtual ~ArucoSurface() {}

    //! Disable copy and assignment
    ArucoSurface(const ArucoSurface&) = delete;
    ArucoSurface& operator=(const ArucoSurface&) = delete;

    //! Returns the number of poses
    inline int GetNumPoses() const
    { return static_cast<int>(arPoses.size()); }

    //! Returns a specific pose
    //! \param[in] pnum Pose number
    inline const ArucoPose& GetPose(const int pnum) const
    { CV_Assert(pnum<static_cast<int>(arPoses.size()));
      return arPoses[pnum]; }

    //! Merge all poses into one
    void MergePoses();
    
    //! Returns the merged pose
    inline const MergedArucoPose& GetMergedPose() const
    { CV_Assert(is_merged);
      return mergedPose; }

    //! Transforms the merged pose with the given map
    void TransformMergedPose(const double Rot[][3], const double* tvec);

    //! Returns the reference pose number
    inline int GetReferencePoseNumber() const
    { CV_Assert(is_merged);
      return tgtPose; }

    //! Projects all markers to a Z=0 plane
    //! This is useful when reconstructing planar surfaces
    //! \param[out] Rot Computed rotation matrix
    //! \param[out] tvec Computed translation vector
    //void ProjectMergedPoseToXYPlane(double Rot[][3], double* tvec);
    
    //! Computes a canonical isometry that maps
    //! (i) all surface/background markers to the XY-plane
    //! (ii) a given set of background markers to the X-axis
    //! (iii) a given set of background markers to the Y-axis
    //! (iv) shifts the origin to the intersection of the best fits along X and Y axes.
    //! \param[in] xmarkers Markers defining the X-axis
    //! \param[in] ymarkers Markers defining the Y-axis
    //! \param[out] Rot Computed rotation matrix
    //! \param[out] tvec Computed translation vector
    void ComputeCanonicalIsometry(const std::set<int>& xmarkers,
				  const std::set<int>& ymarkers,
				  double Rot[][3], double* tvec) const;

    //! Scales merged pose by a constant factor
    //! \param[in] scale Scale factor
    void ScaleMergedPose(const double scale);
  
    //! Visualize the merged pose
    //! \param[in] f1 XYZ file in which to print merged surface markers
    //! \param[in] f2 XYZ file in which to print merged background markers
    void Visualize(const cv::String f1, const cv::String f2) const;

    //! Prints merged pose in an XML file
    //! \param[in] xmlfile XML file
    void PrintXML(const cv::String xmlfile) const;

    //! Prints a tec file to visualize the merged pose
    //! \param[in] f1 Tec file in which to print merged surface markers
    //! \param[in] f2 Tec file in which to print merged background markers
    void VisualizeTec(const cv::String f1, const cv::String f2) const;
    
  private:
    int nPoses; //!< Number of poses
    int tgtPose; //!< Target pose
    std::vector<std::pair<int,int>> mergeSequence; //!< Merge sequence
    std::vector<ArucoPose> arPoses; //!< Vector of poses
    MergedArucoPose mergedPose; //!< Merged pose
    cv::String debug_folder;
    mutable bool is_merged; //!< Flag to indicate if poses have been merged
  };
  
}

#endif
