#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>
#include <aruco.hpp>
#include <fstream>
#include <string>
#include <cmath>

using namespace cv;

const int factor = 3;//pixel factor increased
const int numDivision = 100;
const int nCols = 3; // Number of cols of markers
const int nRows = numDivision; // Number of rows of markers
const int prod = nRows*nCols;
int nHalfPixelsPerMarker = 20*factor;
int nPixelsPerMarker = 40*factor;
int nbufferPixels = 10*factor;// #buffer pixels

const double PI = 4.*(std::atan(1.));
const double Rin = 800*factor;  
double d = static_cast<double>(nPixelsPerMarker+nbufferPixels);
const double Rout = 1000*factor;
double incrad = 1.;

int main()
{
   // Size of the image
  const int Ny = 2*Rout;
  const int Nx = 2*Rout;

  // Output image
  cv::Mat_<uchar> img(Nx,Ny,static_cast<uchar>(255));

  // Create a dictionary
  cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_4X4_1000);

  // Create markers and paint the image
  for(int i=0; i<nRows; ++i)
    for(int j=0; j<nCols; ++j)
      {
	double r = Rin + j*d*incrad + nHalfPixelsPerMarker;
	incrad = r/Rin;
	double incTheta = 2.*PI/(static_cast<double>(numDivision));
	double Theta = i*incTheta;
	double center[2];
	center[0] = r*(std::cos(Theta+PI/2)) + Rout;
	center[1] = r*(std::sin(Theta+PI/2))+ Rout;
	
	// Generate the marker (i,j)
	int mnum = nCols*i+j;
	cv::Mat marker;
	dictionary->drawMarker(mnum, nPixelsPerMarker*incrad, marker, 1);
	int kstart = static_cast<int>(center[0]-(nHalfPixelsPerMarker*incrad));
	int Lstart = static_cast<int>(center[1]-(nHalfPixelsPerMarker*incrad));
	
	// Paint the image with the marker
	for(int k=kstart; k<kstart+nPixelsPerMarker*incrad; ++k)
	  for(int L=Lstart; L<Lstart+nPixelsPerMarker*incrad; ++L)
	    {
	      int cx =kstart+(nPixelsPerMarker/2); int cy= Lstart + (nPixelsPerMarker/2);
	      int m = static_cast<int>(std::cos(Theta) * (k - cx) - std::sin(Theta) * (L - cy) + cx);
	      int n = static_cast<int>(std::sin(Theta) * (k - cx) + std::cos(Theta) * (L - cy) + cy);
	       img.at<uchar>(m,n) = marker.at<uchar>(k-kstart, L-Lstart);
	    }
      }

  //cv::imwrite("circle.jpg", img);
  std::cout << "\n\nTotal number of marker is "<< prod << "\n\n"<<std::flush;

  //make inner and outer circles
  cv::circle(img,cv::Point(Nx/2,Ny/2), Rin, cv::Scalar(0),1, 8, 0);
  cv::circle(img,cv::Point(Nx/2,Ny/2), Rout, cv::Scalar(0),1, 8, 0);
  cv::imwrite("circle2.jpg", img);
}

