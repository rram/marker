

# Project
project(unit-tests)

file(COPY "${CMAKE_SOURCE_DIR}/src/aruco/test/example" DESTINATION ${CMAKE_BINARY_DIR}/src/aruco/test/)

# Identical for all targets
foreach(TARGET
    detection)
  
  # Add this target
  add_executable(${TARGET}  ${TARGET}.cpp)
  
  # Link
  target_link_libraries(${TARGET} PUBLIC marker_aruco)

  # choose appropriate compiler flags
  target_compile_features(${TARGET} PUBLIC
    cxx_constexpr
    cxx_auto_type
    cxx_decltype
    cxx_decltype_auto
    cxx_enum_forward_declarations
    cxx_inline_namespaces
    cxx_lambdas
    cxx_nullptr
    cxx_override
    cxx_range_for)

  add_test(NAME ${TARGET} COMMAND ${TARGET})

endforeach()
