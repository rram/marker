// Sriramajayam

#include <mkr_ArucoSurface.h>
#include <mkr_Triangulator.h>
#include <iostream>

using namespace mkr;


int main()
{
  // Create a template file when testing for the first time
  //ArucoSurface::CreateTemplate("aruco_surface_template.xml"); exit(1);
  
  // Stereo rig
  StereoRig rig("example/stereorig.xml");

  // Triangulator
  Triangulator Tri(rig);

  // Create surface
  ArucoSurface arucoSurf("example/aruco_surface_template_ms.xml", Tri, TriMethod::Sym);
  CV_Assert(arucoSurf.GetNumPoses()==3);
  
  // Merge poses to pose 0
  arucoSurf.MergePoses();
  CV_Assert(arucoSurf.GetReferencePoseNumber()==0);

  // Visualize the merged pose
  arucoSurf.Visualize("surf.xyz", "bg.xyz");

  // Print merged pose details
  arucoSurf.PrintXML("merged.xml"); 
}
