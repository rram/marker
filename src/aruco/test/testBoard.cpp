// Sriramajayam

#include <mkr_Boards.h>
#include <iostream>

// Oracle for a half-annulus
bool HalfAnnulusOracle(const int* cnrs, const int rows, const int cols, void* usr);

int main()
{
  // Print a rectangular board
  mkr::RectangularBoardSpecs rect_specs
    ({.dictionaryName="DICT_4X4_250", .nX=10, .nY=15,
	.markerSize=80, .borderBits=1, .spacing=8});
  mkr::CreateBoard(rect_specs, "rect_board.JPG");

  // Create an annular board
  mkr::RectangularBoardSpecs ann_specs
    ({.dictionaryName="DICT_4X4_1000", .nX=55, .nY=55,
	.markerSize=50, .borderBits=1, .spacing=5});
  const int half_len = (ann_specs.nX*(ann_specs.markerSize+ann_specs.spacing) +
			ann_specs.spacing)/2;
  const int inRad = half_len/2;
  const int outRad = 3*half_len/4;
  std::cout<<"\nHalf side length: "<<half_len
	   <<"\nIn radius: "<<inRad
	   <<"\nOut radius: "<<outRad<<std::flush;
  mkr::CreateBoard(ann_specs, inRad, outRad, "ann_board.JPG");

  // Create a half-annulus board
  mkr::MarkerBoardOracle half_ann(HalfAnnulusOracle);
  int radii[] = {inRad, outRad};
  mkr::CreateBoard(ann_specs, half_ann, &radii, "half_ann_board.JPG");

}

// Oracle for a half-annulus
bool HalfAnnulusOracle(const int* cnrs, const int N, const int M, void* usr)
{
  CV_Assert(usr!=nullptr);
  const int* radii = static_cast<const int*>(usr);
  for(int c=0; c<4; ++c)
    {
      // Does this marker lie in the top half
      if(cnrs[2*c]>N/2) return false;

      // Does it lie within the annulus
      int rad2 = (cnrs[2*c]-N/2)*(cnrs[2*c]-N/2) + (cnrs[2*c+1]-M/2)*(cnrs[2*c+1]-M/2);
      if(rad2<radii[0]*radii[0] || rad2>radii[1]*radii[1]) return false;
    }
  return true;
}

