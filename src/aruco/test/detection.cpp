// Sriramajayam

#include <mkr_ArucoSet.h>

int main()
{
  mkr::ArucoSet aset("DICT_4X4_100");
  mkr::ArucoSetImage imgSet;
  aset.Detect("calib_event.jpg",  imgSet);
  aset.Visualize(imgSet, "detected.png");
}
