// Sriramajayam

#include <mkr_ArucoSet.h>
#include <set>
#include <iostream>
#include <mkr_Triangulator.h>

using namespace mkr;

// Checks that A is a subset of B
void IsASubset(const std::vector<int>& A, const std::vector<int>& B);

int main()
{
  // Create an aruco set
  ArucoSet aruset("DICT_4X4_250");

  // Create triangulator
  StereoRig rig("example/stereorig.xml");
  Triangulator tri(rig);
  for(int num=0; num<3; ++num)
    {
      std::cout<<"\n\nPose number: "<<num<<std::flush;
      
      // Detect markers in image 1
      ArucoSetImage A;
      std::string fA = "example/lcam/" + std::to_string(num) + ".JPG"; 
      aruset.Detect(fA, A); 
      std::cout<<"\nNumber of markers: "<<A.nMarkers<<std::flush;
      fA = "lcam_" + std::to_string(num) + ".JPG";
      aruset.Visualize(A, fA);
      
      // Detect markers in image 2
      ArucoSetImage B;
      std::string fB = "example/rcam/" + std::to_string(num) + ".JPG";
      aruset.Detect(fB, B);
      std::cout<<"\nNumber of markers: "<<B.nMarkers<<std::flush;
      fB = "rcam_" + std::to_string(num) + ".JPG";
      aruset.Visualize(B, fB);
      
      // Detect markers common to both images A&B
      ArucoSetImagePair AB;
      aruset.Match(A, B, tri, TriMethod::Sym, AB);
      std::cout<<"\nNumber of common markers: "<<AB.nMarkers<<std::flush;
      // Sanity check for common markers.
      IsASubset(AB.markerIDs, A.markerIDs);
      IsASubset(AB.markerIDs, B.markerIDs);

      // Visualize
      fA = "lcam_" + std::to_string(num) + "_common.JPG";
      fB = "rcam_" + std::to_string(num) + "_common.JPG";
      std::string fAB = std::to_string(num) + "_common.xyz";
      aruset.Visualize(AB, fA, fB, fAB);
      fAB = std::to_string(num) + "_common.xml";
      aruset.PrintXML(fAB, AB);
    }  
}


// Checks that A is a subset of B
void IsASubset(const std::vector<int>& A, const std::vector<int>& B)
{
  std::set<int> P(A.begin(), A.end());
  std::set<int> Q(B.begin(), B.end());
  assert(std::includes(Q.begin(), Q.end(), P.begin(), P.end())==true);
}
