// Sriramajayam

#include <mkr_ArucoDeformationMap.h>
#include <map>
#include <iostream>
#include <fstream>

namespace mkr
{

  // Anonymous namespace for marker-matching & reading files
  namespace
  {
    // Marker matching
    // A[Aindx[i]] = B[Bindx[i]].
    void GetMarkerCorrespondences(const std::vector<int>& A, const std::vector<int>& B,
				  std::vector<int>& Aindx, std::vector<int>& Bindx)
    {
      const int nAmarkers = static_cast<int>(A.size());
      const int nBmarkers = static_cast<int>(B.size());
      CV_Assert((nAmarkers>0 && nBmarkers>0) &&
		"GetMarkerCorrespondence- Unexpected number of markers");
    
      // Create mapping from markers->index for list B
      std::map<int, int> Bmarker2indx;
      for(int i=0; i<nBmarkers; ++i)
	Bmarker2indx[B[i]] = i;

      // Detect coincident markers
      Aindx.clear(); Bindx.clear();
      for(int i=0; i<nAmarkers; ++i)
	{
	  int  mnum = A[i];
	  // Does 'mnum' appear in list B?
	  auto it = Bmarker2indx.find(mnum);
	  if(it!=Bmarker2indx.end())
	    {
	      Aindx.push_back( i );
	      Bindx.push_back( it->second );
	    }
	}

      // Sanity check
      const int nCommon = static_cast<int>(Aindx.size());
      assert(static_cast<int>(Bindx.size())==nCommon &&
	     "GetMarkerCorrespondences- Unexpected scenario");
      
      for(int i=0; i<nCommon; ++i)	
	assert(A[Aindx[i]]==B[Bindx[i]] &&
	       "GetMarkerCorrespondences- Unexpected scenario");

      // Done
      return;
    }


    // Read a merged pose
    void ReadMergedPose(const cv::String xmlfile,
			MergedArucoPose& pose)
    {
      std::cout<<"\nReading file "<<xmlfile<<std::flush;
      
      // Open
      cv::FileStorage fs(xmlfile, cv::FileStorage::READ);
      CV_Assert(fs.isOpened() && "ReadMergedPose- Could not open file");

      // Read number of markers
      auto fn = fs["num_surf_markers"];
      CV_Assert(!fn.empty() && "Could not read number of surface markers");
      int nMarkers;
      fn >> nMarkers;
      
      // Read marker IDs
      fn = fs["surf_marker_ids"];
      CV_Assert(!fn.empty() && "Could not read surface marker IDs");
      cv::Mat ids;
      fn >> ids;
      CV_Assert(ids.rows==nMarkers && ids.cols==1);

      // Read marker corner coordinates
      fn = fs["surf_marker_coords"];
      CV_Assert(!fn.empty() && "Could not read surface marker coordinates");
      cv::Mat coords;
      fn >> coords;
      CV_Assert(coords.rows==nMarkers && coords.cols==4);

      // Close the file
      fs.release();
      
      // Pack data into merged pose
      pose.nSurfMarkers = nMarkers;
      pose.surfMarkerIDs.resize(nMarkers);
      pose.surfMarkerCoords.resize(nMarkers);
      for(int i=0; i<nMarkers; ++i)
	{
	  pose.surfMarkerIDs[i] = ids.at<int>(i,0);
	  pose.surfMarkerCoords[i].resize(4);
	  for(int c=0; c<4; ++c)
	    pose.surfMarkerCoords[i][c] = coords.at<cv::Point3d>(i,c);
	}

      // -- done --
      return;
    }

    
  } // End anonymous namespace



  // Constructor
  ArucoDeformationMap::ArucoDeformationMap(const cv::String refFile,
					   const cv::String defFile)
    :refMarkerCoords({}), defMarkerCoords({}),
     markerIDs({}), nMarkers(0)
  {
    // Read a merged pose representing the reference configuration
    MergedArucoPose refPose;
    ReadMergedPose(refFile, refPose);
    refPose.Check();
    
    // Read a merged pose representing the deformed configuration
    MergedArucoPose defPose;
    ReadMergedPose(defFile, defPose);
    defPose.Check();

    // Map between markers in the reference/deformed configurations
    std::vector<int> refIndex({}), defIndex({});
    GetMarkerCorrespondences(refPose.surfMarkerIDs, defPose.surfMarkerIDs,
			     refIndex, defIndex);
    CV_Assert(refIndex.size()==defIndex.size());
    const int nCommonMarkers = static_cast<int>(refIndex.size());
    CV_Assert(nCommonMarkers<=refPose.nSurfMarkers && nCommonMarkers<=defPose.nSurfMarkers);

    // Create the deformation map
    nMarkers = nCommonMarkers;
    markerIDs.resize(nCommonMarkers);
    refMarkerCoords.resize(nCommonMarkers);
    defMarkerCoords.resize(nCommonMarkers);
    for(int i=0; i<nCommonMarkers; ++i)
      {
	const int& rindx = refIndex[i];
	const int& dindx = defIndex[i];
	CV_Assert(refPose.surfMarkerIDs[rindx]==defPose.surfMarkerIDs[dindx]);
	markerIDs[i] = refPose.surfMarkerIDs[rindx];
	refMarkerCoords[i] = refPose.surfMarkerCoords[rindx];
	defMarkerCoords[i] = defPose.surfMarkerCoords[dindx];
      }
    
    // -- done --
  }

  // Print XML file
  void ArucoDeformationMap::PrintXML(const cv::String xmlfile) const
  {
    cv::FileStorage fs;
    fs.open(xmlfile, cv::FileStorage::WRITE);
    CV_Assert(fs.isOpened());

    // Number of markers
    fs.writeComment((char*)"Number of markers", 0);
    fs<<"num_surf_markers"<<nMarkers;

    // Marker IDs
    fs.writeComment((char*)"Marker IDs", 0);
    cv::Mat_<int> mat_ids(nMarkers,1);
    for(int i=0; i<nMarkers; ++i)
      mat_ids(i,0) = markerIDs[i];
    fs.writeComment((char*)"Marker coordinates in the reference configuration", 0);
    fs<<"surf_marker_ids"<<mat_ids;

    // Reference coordinates
    cv::Mat_<cv::Point3d> coords(nMarkers, 4);
    for(int i=0; i<nMarkers; ++i)
      for(int c=0; c<4; ++c)
	coords(i,c) = refMarkerCoords[i][c];
    fs.writeComment((char*)"Reference coordinates of markers", 0);
    fs<<"ref_surf_marker_coords"<<coords;

    // Deformed coordinates
    for(int i=0; i<nMarkers; ++i)
      for(int c=0; c<4; ++c)
	coords(i,c) = defMarkerCoords[i][c];
    fs.writeComment((char*)"Deformed coordinates of markers", 0);
    fs<<"def_surf_marker_coords"<<coords;

    fs.release();

    // -- done --
    return;
  }


  // Visualize a deformation map as an XYZ file
  void ArucoDeformationMap::Visualize(const cv::String reffile,
				      const cv::String deffile) const
  {
    std::fstream pfile;
    pfile.open(reffile.c_str(), std::ios::out);
    CV_Assert(pfile.good());

    for(int i=0; i<nMarkers; ++i)
      for(int c=0; c<4; ++c)
	{
	  const auto& pt = refMarkerCoords[i][c];
	  pfile<<pt.x<<" "<<pt.y<<" "<<pt.z<<"\n";
	}
	    pfile.close();
	  
	  pfile.open(deffile.c_str(), std::ios::out);
	  CV_Assert(pfile.good());
	  for(int i=0; i<nMarkers; ++i)
	    for(int c=0; c<4; ++c)
	      {
		const auto& pt = defMarkerCoords[i][c];
		pfile<<pt.x<<" "<<pt.y<<" "<<pt.z<<"\n";
	      }
	  pfile.close();

	  // -- done --
	  return;
  }
  

  // Prints a tec-file with displacements as fields
  // Print reference coordinates, displacements
  // deformed coordinates and zero displacements
  void ArucoDeformationMap::PrintTec(const cv::String filename) const
  {
    std::fstream pfile(filename.c_str(), std::ios::out);
    pfile<<"VARIABLES = \"X\", \"Y\", \"Z\", \"F1\", \"F2\", \"F3\" \n"
	 <<"ZONE t=\"t:0\" N="<<2*4*nMarkers<<", E="<<2*nMarkers<<", F=FEPOINT, ET=QUADRILATERAL";
    // Coordinates
    for(int i=0; i<nMarkers; ++i)
      for(int c=0; c<4; ++c)
	{
	  const auto& pt = refMarkerCoords[i][c];
	  const auto& qt = defMarkerCoords[i][c];
	  pfile<<"\n"<<pt.x<<" "<<pt.y<<" "<<pt.z
	       <<" "<<qt.x-pt.x<<" "<<qt.y-pt.y<<" "<<qt.z-pt.z;
	}
    for(int i=0; i<nMarkers; ++i)
      for(int c=0; c<4; ++c)
	{
	  const auto& pt = defMarkerCoords[i][c];
	  pfile<<"\n"<<pt.x<<" "<<pt.y<<" "<<pt.z
	       <<" 0. 0. 0."; // Zero displacements
	}
    // Connectivity
    for(int i=0; i<2*nMarkers; ++i)
      pfile<<"\n"<<4*i+1<<" "<<4*i+2<<" "<<4*i+3<<" "<<4*i+4;

    pfile.flush();
    pfile.close();

    // -- done --
  }
  
}
