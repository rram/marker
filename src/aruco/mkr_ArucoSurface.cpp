// Sriramajayam

#include <mkr_ArucoSurface.h>
#include <mkr_ICP.h>
#include <iostream>
#include <set>
#include <mkr_ICP.h>


namespace mkr
{
  // Anonymous namespace for helper functions
  namespace
  {
    // Helper method for reading data from an xml file
    void ReadTemplateXML(const cv::String xmlfile,
			 int& nPoses,
			 std::vector<cv::String>& lcam_fnames,
			 std::vector<cv::String>& rcam_fnames,
			 cv::String& dictionaryName,
			 int& tgtPose,
			 std::vector<std::pair<int,int>>& mergeSeq,
			 cv::String& debug_folder,
			 std::set<int>& bgMarkers)
    {
      // Open the xml file to create this surface from
      cv::FileStorage fs(xmlfile, cv::FileStorage::READ);
      CV_Assert(fs.isOpened() && "ReadTemplateXML Could not open xml file to create surface");

      // Read the number of poses
      auto fn = fs["nPoses"];
      CV_Assert(!fn.empty() && "ReadTemplateXML Could not read number of poses");
      cv::read(fn, nPoses, -1);
      CV_Assert(nPoses>=1 && "ReadTemplateXML Unexpected number of poses");

      // Read string of left camera filenames
      lcam_fnames.clear();
      fn = fs["lcam_filenames"];
      CV_Assert(!fn.empty() && "ReadTemplateXML Could not read left camera filenames");
      cv::read(fn, lcam_fnames);
      CV_Assert(static_cast<int>(lcam_fnames.size())==nPoses &&
		"ReadTemplateXML Unexpected number of left camera files");
    
      // Read string of right camera filenames
      rcam_fnames.clear();
      fn = fs["rcam_filenames"];
      CV_Assert(!fn.empty() && "ReadTemplateXML Could not read right camera filenames");
      cv::read(fn, rcam_fnames);
      CV_Assert(static_cast<int>(rcam_fnames.size())==nPoses &&
		"ReadTemplateXML Unexpected number of right camera files");

      // Name of the dictionary used
      fn = fs["dictionary_name"];
      CV_Assert(!fn.empty() && "ReadTemplateXML Could not read dictionary name");
      cv::read(fn, dictionaryName, "");

      // Read the target pose
      fn = fs["target_pose"];
      CV_Assert(!fn.empty() && "ReadTemplateXML Could not read target pose");
      cv::read(fn, tgtPose, -1);
      CV_Assert(tgtPose>=0 && tgtPose<nPoses);

      // Read the merge sequence, if provided
      fn = fs["merge_sequence"];
      mergeSeq.clear();
      if(fn.empty()) // Create a default
	{
	  if(nPoses>1) // nPoses = 1 does not require a merge sequence
	    { std::cout<<"\nMerge sequence not provided. Generating a default sequence."<<std::flush;
	      for(int i=0; i<nPoses; ++i)
		if(i!=tgtPose)
		  mergeSeq.push_back( std::pair<int,int>(i,tgtPose) );
	    } }
      else
	{
	  cv::Mat_<int> ms;
	  cv::read(fn, ms);
	  CV_Assert(ms.cols==2);
	  mergeSeq.resize(ms.rows);
	  for(int i=0; i<ms.rows; ++i)
	    mergeSeq[i] = std::pair<int,int>(ms.at<int>(i,0), ms.at<int>(i,1));
	}
      // At most one of the two should be provided: id range or ids of background markers
      auto fnrange = fs["bg_marker_id_range"];
      auto fnids = fs["bg_marker_ids"];
      CV_Assert(fnrange.empty() || fnids.empty());
      bgMarkers.clear();
      if(!fnrange.empty()) // Range is provided
	{
	  cv::Mat_<int> bgrange;
	  cv::read(fnrange, bgrange);
	  CV_Assert( (bgrange.rows==1 && bgrange.cols==2) &&
		     "ReadTemplateXML:: Unexpected range provided for background markers");
	  for(int id=bgrange.at<int>(0,1); id<=bgrange.at<int>(0,2); ++id)
	    bgMarkers.insert(id);
	}
      else if(!fnids.empty()) // IDs are provided
	{
	  cv::Mat_<int> bgids;
	  cv::read(fnids, bgids);
	  CV_Assert( (bgids.rows==1) &&  "ReadTemplateXML::Number of rows should be 1.");
	  for(int i=0; i<bgids.cols; ++i)
	    bgMarkers.insert( bgids.at<int>(0,i) );
	}
      
      // Interim folder to place images in. Optional argument
      debug_folder.clear();
      fn = fs["debug_folder"];
      if(!fn.empty())
	cv::read(fn, debug_folder, "");
      
      // Done reading
      fs.release();
      return;
    }


    // Helper method to encapsulate calls to read a pose
    void ReadPose(const ArucoSet& arSet,
		  const str::Triangulator& tri, const str::TriMethod tmethod, 
		  const cv::String lcamfile, const cv::String rcamfile,
		  const std::set<int> bgMarkers, ArucoPose& arPose)
    {
      // Detect markers in left image
      ArucoSetImage lset;
      arSet.Detect(lcamfile, lset);
      lset.Check();

      // Detect markers in right imahe
      ArucoSetImage rset;
      arSet.Detect(rcamfile, rset);
      rset.Check();
      
      // Create pairing from left/right images
      ArucoSetImagePair lrPair;
      arSet.Match(lset, rset, tri, tmethod, lrPair);
      lrPair.Check();

      // Partition markers into surface and background 
      auto& surf = arPose.surf;
      auto& bg = arPose.bg;
      for(int n=0; n<lrPair.nMarkers; ++n)
	{
	  // This marker
	  const auto& m = lrPair.markerIDs[n];

	  // Is this a background marker
	  if(bgMarkers.find(m)!=bgMarkers.end())
	    {
	      bg.AimgName = lrPair.AimgName;
	      bg.BimgName = lrPair.BimgName;
	      bg.dictionaryName = lrPair.dictionaryName;
	      bg.markerIDs.push_back(m);
	      bg.Acorners.push_back( lrPair.Acorners[n] );
	      bg.Bcorners.push_back( lrPair.Bcorners[n] );
	      bg.Tri = lrPair.Tri;
	      bg.CornerPts.push_back( lrPair.CornerPts[n] );
	      ++bg.nMarkers;
	    }
	  else // This is a surface marker
	    {
	      surf.AimgName = lrPair.AimgName;
	      surf.BimgName = lrPair.BimgName;
	      surf.dictionaryName = lrPair.dictionaryName;
	      surf.markerIDs.push_back(m);
	      surf.Acorners.push_back( lrPair.Acorners[n] );
	      surf.Bcorners.push_back( lrPair.Bcorners[n] );
	      surf.Tri = lrPair.Tri;
	      surf.CornerPts.push_back( lrPair.CornerPts[n] );
	      ++surf.nMarkers;
	    }
	}
      
      // Sanity check on the partitioning
      CV_Assert(surf.nMarkers+bg.nMarkers==lrPair.nMarkers);
      
      // -- done --
      return;
    }
  }

  

  
  // Create template XML file
  void ArucoSurface::CreateTemplate(const cv::String xmlfile)
  {
    // Create a set of dummy template parameters
    const int nPoses = 2; // Number of poses for this surface
    std::vector<cv::String> lcam_filenames({"filename_1", "filename_2"}); // Left cam images
    std::vector<cv::String> rcam_filenames({"filename_1", "filename_2"}); // Right cam images
    cv::String dictionaryName = "dictionary_name"; // Dictionary used
    int bgIDrange[2]={102,192}; // Range of background markers
    std::set<int> bgmarkers({102,103,175,191});
    cv::String debug_folder = "dbg_folder_name"; // Folder to place interim images
    int targetPose = 2;
    cv::Mat_<int> mergeSeq(3,2);
    mergeSeq.at<int>(0,0) = 1; mergeSeq.at<int>(0,1) = 3;
    mergeSeq.at<int>(1,0) = 2; mergeSeq.at<int>(1,1) = 3;
    mergeSeq.at<int>(2,0) = 0; mergeSeq.at<int>(2,1) = 3;
    
    // Open this file
    cv::FileStorage fs(xmlfile, cv::FileStorage::WRITE);
    CV_Assert(fs.isOpened() && "ArucoSurface::CreateTemplate- Could not open file");

    // Number of poses
    fs.writeComment((char*)"Number of poses recorded for this surface", 0);
    fs.writeComment((char*)"First pose defines the reference coordinate system", 0);
    fs<<"nPoses"<<nPoses;
    
    // Absolute paths to left camera files for all poses
    fs.writeComment((char*)"Left camera file names for all poses", 0);
    fs<<"lcam_filenames"<<lcam_filenames;

    // Absolute paths to right camera files for all poses
    fs.writeComment((char*)"Right camera file names for all poses", 0);
    fs<<"rcam_filenames"<<rcam_filenames;

    // Marker dictionary used
    fs.writeComment((char*)"Marker dictionary name", 0);
    fs<<"dictionary_name"<<dictionaryName;

    // Target pose
    fs.writeComment((char*)"Target pose", 0);
    fs<<"target_pose"<<targetPose;

    // Sequence in which to merge poses
    fs.writeComment((char*)"Sequence in which to merge poses: source pose -> target pose", 0);
    fs.writeComment((char*)"Optional. If not provided, a default merging all poses directly to the target pose will be created", 0);
    fs<<"merge_sequence"<<mergeSeq;
    
    // Range of markers in the background
    fs.writeComment((char*)"Optional. Marker id range in the background", 0);
    fs.writeComment((char*)"Inclusive of extereme marker IDs", 0);
    fs.writeComment((char*)"Provide EITHER a range OR the ids of background markers. Not both.", 0);
    cv::Mat_<int> idrange(1,2);
    idrange.at<int>(0,0) = bgIDrange[0];
    idrange.at<int>(0,1) = bgIDrange[1];
    fs<<"bg_marker_id_range"<<idrange;

    // IDs of markers in the background
    fs.writeComment((char*)"Optional. Marker ids in the background", 0);
    fs.writeComment((char*)"Provide EITHER a range OR the ids of background markers. Not both.", 0);
    cv::Mat_<int> bgids(1,bgmarkers.size());
    int count = 0;
    for(auto& it:bgmarkers)
      bgids.at<int>(0,count++) = it;
    fs<<"bg_marker_ids"<<bgids;

    // Path to folder in which to print intermediate images
    fs.writeComment((char*)"Optional. Folder to place intermediate images", 0);
    fs<<"debug_folder"<<debug_folder;
    
    fs.release();
  }


  
  // Constructor
  ArucoSurface::ArucoSurface(const cv::String xmlfile,
			     const str::Triangulator& tri,
			     const str::TriMethod tmethod)
  {
    // Read XML file with details
    std::vector<cv::String> lcam_fnames, rcam_fnames; // Images from left/right cameras
    cv::String dictionaryName; // Dictionary of aruco markers
    std::set<int> bgMarkers; // IDs of background markers
    ReadTemplateXML(xmlfile, nPoses, lcam_fnames, rcam_fnames, dictionaryName, tgtPose, mergeSequence, debug_folder, bgMarkers);
    const bool dbg_flag = static_cast<bool>(debug_folder.size());

    // Read all poses
    arPoses.resize(nPoses);
    ArucoSet arSet(dictionaryName);
    for(int p=0; p<nPoses; ++p)
      {
	std::cout<<"\nReading pose: "<<p<<std::flush;
	ReadPose(arSet, tri, tmethod, lcam_fnames[p], rcam_fnames[p], bgMarkers, arPoses[p]);
      }

    // Note that merging is not done
    is_merged = false;
    
    // Debugging aid: visualize each pose
    if(dbg_flag)
      for(int p=0; p<nPoses; ++p)
	{ cv::String f1, f2, f3;
	  f1 = debug_folder + "/surf-L" + std::to_string(p) + ".JPG";
	  f2 = debug_folder + "/surf-R" + std::to_string(p) + ".JPG";
	  f3 = debug_folder + "/surf-" + std::to_string(p) + ".xyz";
	  ArucoSet::Visualize(arPoses[p].surf,f1, f2, f3);
	  if(arPoses[p].bg.nMarkers>0){
	  f1 = debug_folder + "/bg-L" + std::to_string(p) + ".JPG";
	  f2 = debug_folder + "/bg-R" + std::to_string(p) + ".JPG";
	  f3 = debug_folder + "/bg-" + std::to_string(p) + ".xyz";
	  ArucoSet::Visualize(arPoses[p].bg,f1, f2, f3); }
	}
    // -- done --
  }

  
  // Transforms the merged pose with the given map
  void ArucoSurface::TransformMergedPose(const double Rot[][3],
					 const double* tvec)
  {
    CV_Assert(is_merged==true && "ArucoSurface::TransformMergedPose- poses not merged");
    
    // Surface markers
    for(int i=0; i<mergedPose.nSurfMarkers; ++i)
      for(int c=0; c<4; ++c)
	{
	  auto& pt = mergedPose.surfMarkerCoords[i][c];
	  const double X[] = {pt.x, pt.y, pt.z};
	  double Y[] = {0.,0.,0.};
	  for(int j=0; j<3; ++j)
	    {
	      Y[j] = tvec[j];
	      for(int k=0; k<3; ++k)
		Y[j] += Rot[j][k]*X[k];
	    }
	  pt.x=Y[0]; pt.y=Y[1]; pt.z=Y[2];
	}

    // Background markers
    for(int i=0; i<mergedPose.nbgMarkers; ++i)
      for(int c=0; c<4; ++c)
	{
	  auto& pt = mergedPose.bgMarkerCoords[i][c];
	  const double X[] = {pt.x, pt.y, pt.z};
	  double Y[] = {0.,0.,0.};
	  for(int j=0; j<3; ++j)
	    {
	      Y[j] = tvec[j];
	      for(int k=0; k<3; ++k)
		Y[j] += Rot[j][k]*X[k];
	    }
	  pt.x=Y[0]; pt.y=Y[1]; pt.z=Y[2];
	}
    
    // -- done --
    return;
  }
    
  // Visualize the merged pose
  void ArucoSurface::Visualize(const cv::String f1, const cv::String f2) const
  {
    CV_Assert(is_merged==true && "ArucoSurface::Visualize- poses not merged");
    
    // Print surface markers
    std::fstream pfile;
    pfile.open(f1.c_str(), std::ios::out);
    for(auto& cnrs:mergedPose.surfMarkerCoords)
      for(auto& P:cnrs)
	pfile<<P.x<<" "<<P.y<<" "<<P.z<<"\n";
    pfile.flush(); pfile.close();

    // Print background markers
    pfile.open(f2.c_str(), std::ios::out);
    for(auto& cnrs:mergedPose.bgMarkerCoords)
      for(auto& P:cnrs)
	pfile<<P.x<<" "<<P.y<<" "<<P.z<<"\n";
    pfile.flush(); pfile.close();

    // -- done --
    return;
  }


  // Print merged pose in an XML file
  void ArucoSurface::PrintXML(const cv::String xmlfile) const
  {
    CV_Assert(is_merged==true && "ArucoSurface::PrintXML- poses not merged");
    
    // Open this file
    cv::FileStorage fs(xmlfile, cv::FileStorage::WRITE);
    CV_Assert(fs.isOpened() && "ArucoSurface::PrintXML- Could not open file");

    // Details of surface markers
    fs.writeComment((char*)"Number of surface markers", 0);
    const int& nSurfMarkers = mergedPose.nSurfMarkers;
    fs<<"num_surf_markers"<<nSurfMarkers;

    cv::Mat_<int> m_surfIDs(nSurfMarkers, 1);
    for(int i=0; i<nSurfMarkers; ++i)
      m_surfIDs(i,0) = mergedPose.surfMarkerIDs[i];
    fs.writeComment((char*)"IDs of surface markers", 0);
    fs<<"surf_marker_ids"<<m_surfIDs;

    cv::Mat_<cv::Point3d> m_surfCoords(nSurfMarkers, 4);
    for(int i=0; i<nSurfMarkers; ++i)
      for(int c=0; c<4; ++c) // Number of corners in each marker
	m_surfCoords(i,c) = mergedPose.surfMarkerCoords[i][c];
    fs.writeComment((char*)"Coordinates of surface marker corners", 0);
    fs<<"surf_marker_coords"<<m_surfCoords;

    m_surfIDs.release();
    m_surfCoords.release();

    // Details of background markers
    fs.writeComment((char*)"Number of background markers", 0);
    const int& nbgMarkers = mergedPose.nbgMarkers;
    fs<<"num_bg_markers"<<nbgMarkers;

    cv::Mat_<int> m_bgIDs(nbgMarkers, 1);
    for(int i=0; i<nbgMarkers; ++i)
      m_bgIDs(i,0) = mergedPose.bgMarkerIDs[i];
    fs.writeComment((char*)"IDs of background markers", 0);
    fs<<"bg_marker_ids"<<m_bgIDs;

    cv::Mat_<cv::Point3d> m_bgCoords(nbgMarkers, 4);
    for(int i=0; i<nbgMarkers; ++i)
      for(int c=0; c<4; ++c)
	m_bgCoords(i,c) = mergedPose.bgMarkerCoords[i][c];
    fs.writeComment((char*)"Coords of background marker corners", 0);
    fs<<"bg_marker_coords"<<m_bgCoords;

    m_bgIDs.release();
    m_bgCoords.release();

    fs.release();

    // -- done --
    return;
  }

  
  // Scales merged pose by a constant factor
  void ArucoSurface::ScaleMergedPose(const double scale) 
  {
    CV_Assert(is_merged==true && "ArucoSurface::ScaleMergedPose- poses not merged");
    CV_Assert(scale>0.);
    for(auto& ptvec:mergedPose.surfMarkerCoords)
      for(auto& pt:ptvec)
	{ pt.x *= scale;
	  pt.y *= scale;
	  pt.z *= scale; }

    for(auto& ptvec:mergedPose.bgMarkerCoords)
      for(auto& pt:ptvec)
	{ pt.x *= scale;
	  pt.y *= scale;
	  pt.z *= scale; }

    // -- done --
    return;
  }

  // Prints a tec file to visualize the merged pose
  void ArucoSurface::VisualizeTec(const cv::String f1, const cv::String f2) const
  {
    CV_Assert(is_merged==true && "ArucoSurface::VisualizeTec- poses not merged");

    // Surface markers
    std::fstream pfile;
    pfile.open(f1.c_str(), std::ios::out);
    pfile<<"VARIABLES = \"X\", \"Y\", \"Z\"\n"
	 <<"ZONE t=\"t:0\" N="<<4*mergedPose.nSurfMarkers<<", E="<<mergedPose.nSurfMarkers<<", F=FEPOINT, ET=QUADRILATERAL";
    // Coordinates
    for(int i=0; i<mergedPose.nSurfMarkers; ++i)
      for(int c=0; c<4; ++c)
	{
	  const auto& pt = mergedPose.surfMarkerCoords[i][c];
	  pfile<<"\n"<<pt.x<<" "<<pt.y<<" "<<pt.z;
	}
    // Connectivity
    for(int i=0; i<mergedPose.nSurfMarkers; ++i)
      pfile<<"\n"<<4*i+1<<" "<<4*i+2<<" "<<4*i+3<<" "<<4*i+4;
    pfile.flush();
    pfile.close();

    // Background markers
    pfile.open(f2.c_str(), std::ios::out);
    pfile<<"VARIABLES = \"X\", \"Y\", \"Z\"\n"
	 <<"ZONE t=\"t:0\" N="<<4*mergedPose.nbgMarkers<<", E="<<mergedPose.nbgMarkers<<", F=FEPOINT, ET=QUADRILATERAL";
    // Coordinates
    for(int i=0; i<mergedPose.nbgMarkers; ++i)
      for(int c=0; c<4; ++c)
	{
	  const auto& pt = mergedPose.bgMarkerCoords[i][c];
	  pfile<<"\n"<<pt.x<<" "<<pt.y<<" "<<pt.z;
	}
    // Connectivity
    for(int i=0; i<mergedPose.nbgMarkers; ++i)
      pfile<<"\n"<<4*i+1<<" "<<4*i+2<<" "<<4*i+3<<" "<<4*i+4;
    pfile.flush();
    pfile.close();
  }
  
  
  // Canonical transformation implemented in mkr_ArucoSurface_XY.cpp
}
