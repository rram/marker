// Sriramajayam

#ifndef MKR_ARUCO_DEFORMATION_H
#define MKR_ARUCO_DEFORMATION_H

#include <mkr_ArucoStructs.h>

namespace mkr
{
  //! Class encapsulating a deformation mapping
  class ArucoDeformationMap
  {
  public:
    //! Reads a pair of xml files with
    //! aruco marker ids and coordinates representing
    //! the reference and deformed configurations
    //! \param[in] refFile XML file with reference configuration
    //! \param[in] defFile XML file with deformed configuration
    ArucoDeformationMap(const cv::String refFile,
			const cv::String defFile);

    //! Save a deformation map as an XML file
    void PrintXML(const cv::String xmlfile) const;
    
    //! Visualize a deformation map as an XYZ file
    void Visualize(const cv::String reffile,
		   const cv::String deffile) const;

    //! Prints a tec-file with displacements as fields
    void PrintTec(const cv::String filename) const;

  private:
    std::vector<std::vector<cv::Point3d>> refMarkerCoords;
    std::vector<std::vector<cv::Point3d>> defMarkerCoords;
    std::vector<int> markerIDs;
    int nMarkers;

    //! Check
    inline void Check()
    { CV_Assert(nMarkers==static_cast<int>(markerIDs.size()));
      CV_Assert(nMarkers==static_cast<int>(refMarkerCoords.size()));
      CV_Assert(nMarkers==static_cast<int>(defMarkerCoords.size()));
      for(int i=0; i<nMarkers; ++i)
	{ CV_Assert(static_cast<int>(refMarkerCoords[i].size())==4);
	  CV_Assert(static_cast<int>(defMarkerCoords[i].size())==4); }
      return; }
  };
}

#endif
