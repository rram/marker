// Sriramajayam

#ifndef MKR_BOARDS_H
#define MKR_BOARDS_H

#include <opencv2/core.hpp>
#include <functional>

namespace mkr
{
  //! Helper struct to specify board details
  struct RectangularBoardSpecs
  {
    cv::String dictionaryName; //!< Dictionary to use
    int nX; //!< Number of markers in the X-direction
    int nY; //!< Number of markers in the X-direction
    int markerSize; //!< Size of each marker in pixels. 
    int borderBits; //!< Number of border pixels
    int spacing; //! Padding in pixels around markers
    //! Checks that parameters have been defined
    void Check() const;
  };
  
  //! Function to create a rectangular grid of aruco markers
  //! \param[in] specs Specification of the rectangular board
  //! \param[in] imgName Image to print
  void CreateBoard(const RectangularBoardSpecs& specs,
		   const cv::String imgName);


  //! Function to create an annular board of aruco markers
  //! \param[in] specs Specification of the rectangular board
  //! \param[in] inRad Inner radius in number of pixels
  //! \param[in] outRad Outer radius in number of pixels
  //! \param[in] imgName Image to print
  void CreateBoard(const RectangularBoardSpecs& specs,
		   const int inRad, const int outRad,
		   const cv::String imgName);


  //! \param[in] cnrs corners of the marker
  //! \param[in] rows Number of rows in the image
  //! \param[in] cols Number of columns in the image
  //! \return true if marker should be placed and false otherwise
  using MarkerBoardOracle = std::function<bool(const int* cnrs, const int rows, const int cols, void* usr)>;
  
  //! Function to create a customized board of aruco markers
  //! \param[in] specs Specification of the rectangular board
  //! \param[in] func_ptr Function that decided whether or not to place a marker
  //! \param[in] usr Parameters to pass to the oracle
  //! \param[in] imgName Image to print
  void CreateBoard(const RectangularBoardSpecs& specs,
		   MarkerBoardOracle func_ptr, void* usr,
		   const cv::String imgName);
}


#endif
