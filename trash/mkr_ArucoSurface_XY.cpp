// Sriramajayam

#include <mkr_ArucoSurface.h>
#include <gsl/gsl_linalg.h>
#include <iostream>

namespace mkr
{
  // Anonymous namespace for helper functions
  namespace
  {
    // Fit a plane through a given set of marker corners
    void FitPlane(const std::vector<std::vector<cv::Point3d>>& Coords,
		  double* coefs)
    {
      // Compute the matrix and RHS
      double Mat[9];
      std::fill(Mat, Mat+9, 0.);
      double rhs[3] = {0.,0.,0.};
      for(auto& ptvec:Coords)
	for(auto& pt:ptvec)
	  {
	    double xyz[] = {pt.x, pt.y, pt.z};
	    for(int i=0; i<3; ++i)
	      {
		rhs[i] += xyz[i];
		for(int j=0; j<3; ++j)
		  Mat[3*i+j] += xyz[i]*xyz[j];
	      }
	  }

      // Solve AX = RHS using GSL
      gsl_matrix_view m = gsl_matrix_view_array(Mat, 3, 3);
      gsl_vector_view b = gsl_vector_view_array(rhs, 3);
      gsl_vector* sol = gsl_vector_alloc(3);
      int s;
      gsl_permutation* p = gsl_permutation_alloc(3);
      gsl_linalg_LU_decomp(&m.matrix, p, &s);
      gsl_linalg_LU_solve(&m.matrix, p, &b.vector, sol);

      // save the solution
      for(int i=0; i<3; ++i)
	coefs[i] = sol->data[i];

      // Clean up
      gsl_permutation_free(p);
      gsl_vector_free(sol);

      // -- done --
      return;
    }


    // Fit a line through 2D points: ax + by = 1
    void FitLine(const std::vector<cv::Point2d>& coords,
		 double* coefs)
    {
      double Mat[2][2] = {{0.,0.},{0.,0.}};
      double rhs[2] = {0.,0.};
      for(auto& pt:coords)
	{
	  Mat[0][0] += pt.x*pt.x; Mat[0][1] += pt.x*pt.y;
	  Mat[1][0] += pt.y*pt.x; Mat[1][1] += pt.y*pt.y;
	  rhs[0] += pt.x;
	  rhs[1] += pt.y;
	}
      double det = Mat[0][0]*Mat[1][1] - Mat[0][1]*Mat[1][0];
      double MatInv[2][2] = {{Mat[1][1]/det, -Mat[0][1]/det},
			     {-Mat[1][0]/det, Mat[0][0]/det}};

      // solve
      coefs[0] = MatInv[0][0]*rhs[0] + MatInv[0][1]*rhs[1];
      coefs[1] = MatInv[1][0]*rhs[0] + MatInv[1][1]*rhs[1];

      return;
    }
    

    // Map a given normal to the Ez axis
    void GetRotationToXYPlane(const double* coefs, double Rot[][3])
    {
      // Normal
      double nvec[3] = {coefs[0], coefs[1], coefs[2]};
      double norm = std::sqrt(nvec[0]*nvec[0]+nvec[1]*nvec[1]+nvec[2]*nvec[2]);
      CV_Assert(norm>1.e-2);
      for(int k=0; k<3; ++k)
	nvec[k] /= norm;

      // Ez
      const double Ez[] = {0.,0.,1.};

      // Ez.nvec
      const double dot = Ez[0]*nvec[0]+Ez[1]*nvec[1]+Ez[2]*nvec[2];

      // nperp = nvec - (nvec.Ez)Ez, vector in the plane of nvec-Ez, orthogonal to nvec
      double nperp[3] = {0.,0.,0.};
      norm = 0.;
      for(int k=0; k<3; ++k)
	{ nperp[k] = nvec[k]-dot*Ez[k];
	  norm += nperp[k]*nperp[k]; }
      norm = std::sqrt(norm);
      CV_Assert(norm>1.e-2);
      for(int k=0; k<3; ++k)
	nperp[k] /= norm;

      // ncross = nperp x Ez = axis of rotation
      double ncross[3];
      norm = 0.;
      for(int k=0; k<3; ++k)
	{ ncross[k] = nperp[(k+1)%3]*Ez[(k+2)%3] - nperp[(k+2)%3]*Ez[(k+1)%3];
	  norm += ncross[k]*ncross[k]; }
      norm = std::sqrt(norm);
      CV_Assert(std::abs(1.-norm)<1.e-3);
      for(int k=0; k<3; ++k)
	ncross[k] /= norm;

      // Rotation matrix in the basis of nperp, Ez, cross = rotation by angle theta
      double cos = dot;
      double sin = std::sqrt(1.-dot*dot);
      double RMat[3][3] = {{cos, -sin, 0.},
			   {sin, cos, 0.},
			   {0., 0., 1.}};

      // Change basis to Ex, Ey, Ez
      // Rot = Qt RMat Q
      const double Qt[3][3] = {{nperp[0], Ez[0], ncross[0]},
			       {nperp[1], Ez[1], ncross[1]},
			       {nperp[2], Ez[2], ncross[2]}};
      for(int i=0; i<3; ++i)
	for(int j=0; j<3; ++j)
	  {
	    Rot[i][j] = 0.;
	    for(int k=0; k<3; ++k)
	      for(int L=0; L<3; ++L)
		Rot[i][j] += Qt[i][k]*RMat[k][L]*Qt[j][L];
	  }

      // Check that Ez = Rot(nvec)
      double check_Ez[3] = {0.,0.,0.};
      for(int i=0; i<3; ++i)
	for(int j=0; j<3; ++j)
	  check_Ez[i] += Rot[i][j]*nvec[j];
      CV_Assert(std::abs(Ez[0]-check_Ez[0]) +
		std::abs(Ez[1]-check_Ez[1]) + 
		std::abs(Ez[2]-check_Ez[2]) < 1.e-4);

      // -- done --
      return;
    }


    // Intersect a pair of lines
    void Intersect2DAxes(const double* xvec, const double* yvec,
			 double* pt)
    {
      const double Mat[2][2] = {{xvec[0], xvec[1]},
				{yvec[0], yvec[1]}};
      const double det = Mat[0][0]*Mat[1][1]-Mat[0][1]*Mat[1][0];
      CV_Assert(std::abs(det)>1.e-2);
      const double invMat[2][2] = {{Mat[1][1]/det, -Mat[0][1]/det},
			     {-Mat[1][0]/det, Mat[0][0]/det}};
      pt[0] = invMat[0][0]*1. + invMat[0][1]*1.;
      pt[1] = invMat[1][0]*1. + invMat[1][1]*1.;
      
      // -- done --
      return;
    }

    // Rotate a given pair of vectors to the Ex,Ey axes
    void GetRotationToXYAxes(const double* xvec, const double* yvec,
			     double Rot[][3])
    {
      // Normalize
      double norm = std::sqrt(xvec[0]*xvec[0]+xvec[1]*xvec[1]);
      CV_Assert(norm>1.e-2);
      const double nvec[] = {xvec[0]/norm, xvec[1]/norm};
      norm = std::sqrt(yvec[0]*yvec[0]+yvec[1]*yvec[1]);
      CV_Assert(norm>1.e-2);
      //const double mvec[] = {yvec[0]/norm, yvec[1]/norm};

      // Map (nvec, mvec) -> (Ex, Ey)
      const double cos = nvec[0];
      const double sin = nvec[1];

      // Rot = rotation by -theta
      Rot[0][0] = cos;  Rot[0][1] = sin; Rot[0][2] = 0.;
      Rot[1][0] = -sin; Rot[1][1] = cos; Rot[1][2] = 0.;
      Rot[2][0] = 0.;   Rot[2][1] = 0.;  Rot[2][2] = 1.; 

      // Check
      double check_nvec[2] = {0.,0.};
      for(int i=0; i<2; ++i)
	for(int j=0; j<2; ++j)
	  check_nvec[i] += Rot[i][j]*nvec[j];
      CV_Assert(std::abs(check_nvec[0]-1.)+std::abs(check_nvec[1]-0.)<1.e-4);

      // -- done --
      return;
    }
  }
  

  // Computes a canonical isometry for the merged pose
  void ArucoSurface::ComputeCanonicalIsometry(const std::set<int>& xmarkers,
					      const std::set<int>& ymarkers,
					      double Rot[][3], double* tvec) const
  {
    CV_Assert(is_merged==true && "ArucoSurface::ComputeCanonicalIsometry- poses not merged");
    CV_Assert(static_cast<int>(xmarkers.size())>=2 && static_cast<int>(ymarkers.size())>=2);

    // Fit a plane through the set of all triangulated points
    double coefs[3];
    const int nMarkers = mergedPose.nSurfMarkers+mergedPose.nbgMarkers;
    std::vector<int> markerIDs(mergedPose.surfMarkerIDs);
    std::vector<std::vector<cv::Point3d>> triPts = mergedPose.surfMarkerCoords;
    for(int i=0; i<mergedPose.nbgMarkers; ++i)
      { triPts.push_back(mergedPose.bgMarkerCoords[i]);
	markerIDs.push_back(mergedPose.bgMarkerIDs[i]); }
    FitPlane(triPts, coefs);

    // Translate the plane to pass through the origin
    double avg[3] = {0.,0.,0.};
    for(auto& ptvec:triPts)
      for(auto& pt:ptvec)
	{ avg[0] += pt.x;
	  avg[1] += pt.y;
	  avg[2] += pt.z; }
    for(int k=0; k<3; ++k)
      avg[k] /= static_cast<double>(4*triPts.size());

    for(auto& ptvec:triPts)
      for(auto& pt:ptvec)
	{ pt.x -= avg[0];
	  pt.y -= avg[1];
	  pt.z -= avg[2]; }
    
    // Equation of the plane is now: Ax + By + Cz = 0 (after translation)
    // Find a rotation matrix that rotates n=(A,B,C) to Ez=(0,0,1)
    double Rot3D[3][3];
    GetRotationToXYPlane(coefs, Rot3D);

    // Project all points onto the XY plane
    for(auto& ptvec:triPts)
      for(auto& pt:ptvec)
	{
	  double X[] = {pt.x, pt.y, pt.z};
	  double Y[3] = {0., 0., 0.};
	  for(int i=0; i<3; ++i)
	    for(int j=0; j<3; ++j)
	      Y[i] += Rot3D[i][j]*X[j];

	  pt.x = Y[0]; pt.y = Y[1]; pt.z = Y[2];
	}
    
    // Identify coordinates of markers along the X and Y axes
    std::vector<cv::Point2d> xcoords({});
    for(auto& m:xmarkers)
      {
	for(int i=0; i<nMarkers; ++i)
	  if(markerIDs[i]==m)
	    {
	      const auto& qtvec = triPts[i];
	      cv::Point2d pt(0.,0.);
	      for(int c=0; c<4; ++c)
		{ pt.x += 0.25*qtvec[c].x;
		  pt.y += 0.25*qtvec[c].y; }
	      xcoords.push_back(pt);
	    }
      }

    std::vector<cv::Point2d> ycoords({});
    for(auto& m:ymarkers)
      {
	for(int i=0; i<nMarkers; ++i)
	  if(markerIDs[i]==m)
	    {
	      const auto& qtvec = triPts[i];
	      cv::Point2d pt(0.,0.);
	      for(int c=0; c<4; ++c)
		{ pt.x += 0.25*qtvec[c].x;
		  pt.y += 0.25*qtvec[c].y; }
	      ycoords.push_back(pt);
	    }
      }

    // Best fit line through x-markers and y-markers
    double xcoefs[2], ycoefs[2];
    FitLine(xcoords, xcoefs);
    FitLine(ycoords, ycoefs);

    // Intersection of xvec and yvec
    double intersect2D[3] = {0.,0.,0.};
    Intersect2DAxes(xcoefs, ycoefs, intersect2D);
    
    // Vectors along the X/Y axes
    const double xvec[] = {-xcoefs[1], xcoefs[0]};
    double yvec[] = {-ycoefs[1], ycoefs[0]};

    // Check the orientation & flip if necessay
    if(xvec[0]*yvec[1]-xvec[1]*yvec[0]<0.)
      { yvec[0] *= -1.; yvec[1] *= -1.; }
    
    // 2D rotation that maps (xvec,yvec) -> (Ex, Ey)
    // HACK: yvec is not used in this calculation
    double Rot2D[3][3];
    GetRotationToXYAxes(xvec, yvec, Rot2D);
    
    // Apply the 2D transformation
    for(auto& ptvec:triPts)
      for(auto& pt:ptvec)
	{
	  double X[] = {pt.x-intersect2D[0], pt.y-intersect2D[1]};
	  double Y[] = {0., 0.};
	  for(int i=0; i<2; ++i)
	    for(int j=0; j<2; ++j)
	      Y[i] += Rot2D[i][j]*X[j];
	  pt.x = Y[0];
	  pt.y = Y[1];
	}

    // Cumulative transformation:
    // Y = Rot2D(Rot3D(X-tavg)-intersect2D) = Rot2D*Rot3D*X-Rot2D*Rot3D*tavg-Rot2D*intersect2D
    // Rot = Rot2D*Rot3D
    // tvec = -(Rot*avg+Rot2D*intersect2D)
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	{
	  Rot[i][j] = 0.;
	  for(int k=0; k<3; ++k)
	    Rot[i][j] += Rot2D[i][k]*Rot3D[k][j];
	}

    for(int i=0; i<3; ++i)
      {
	tvec[i] = 0.;
	for(int j=0; j<3; ++j)
	  tvec[i] -= (Rot[i][j]*avg[j]+Rot2D[i][j]*intersect2D[j]);
      }

    
    // Sanity check
    for(int i=0; i<mergedPose.nSurfMarkers; ++i)
      {
	const auto& ptvec = mergedPose.surfMarkerCoords[i];
	for(int c=0; c<4; ++c)
	  {
	    const auto& pt = ptvec[c];
	    const double X[] = {pt.x, pt.y, pt.z};
	    double Y[3] = {0., 0., 0.};
	    for(int j=0; j<3; ++j)
	      {
		for(int k=0; k<3; ++k)
		  Y[j] += Rot[j][k]*X[k];
		Y[j] += tvec[j];
	      }
	    const auto& qt = triPts[i][c];
	    CV_Assert(std::abs(Y[0]-qt.x) + std::abs(Y[1]-qt.y) < 1.e-4);
	  }
      }
		    
    // -- done --
    return;
  }

}
