// Sriramajayam

#include <mkr_ArucoSurface.h>
#include <gsl/gsl_linalg.h>
#include <iostream>

namespace mkr
{
  // Anonymous namespace for helper functions
  namespace
  {
    // Fit a plane through a given set of marker corners
    void FitPlane(const std::vector<std::vector<cv::Point3d>>& Coords,
		  double* coefs)
    {
      // Compute the matrix and RHS
      double Mat[9];
      std::fill(Mat, Mat+9, 0.);
      double rhs[3] = {0.,0.,0.};
      for(auto& ptvec:Coords)
	for(auto& pt:ptvec)
	  {
	    double xyz[] = {pt.x, pt.y, pt.z};
	    for(int i=0; i<3; ++i)
	      {
		rhs[i] += xyz[i];
		for(int j=0; j<3; ++j)
		  Mat[3*i+j] += xyz[i]*xyz[j];
	      }
	  }

      // Solve AX = RHS using GSL
      gsl_matrix_view m = gsl_matrix_view_array(Mat, 3, 3);
      gsl_vector_view b = gsl_vector_view_array(rhs, 3);
      gsl_vector* sol = gsl_vector_alloc(3);
      int s;
      gsl_permutation* p = gsl_permutation_alloc(3);
      gsl_linalg_LU_decomp(&m.matrix, p, &s);
      gsl_linalg_LU_solve(&m.matrix, p, &b.vector, sol);

      // save the solution
      for(int i=0; i<3; ++i)
	coefs[i] = sol->data[i];

      // Clean up
      gsl_permutation_free(p);
      gsl_vector_free(sol);

      // -- done --
      return;
    }


        // Map a given normal to the Ez axis
    void GetRotationToXYPlane(const double* coefs, double Rot[][3])
    {
      // Normal
      double nvec[3] = {coefs[0], coefs[1], coefs[2]};
      double norm = std::sqrt(nvec[0]*nvec[0]+nvec[1]*nvec[1]+nvec[2]*nvec[2]);
      CV_Assert(norm>1.e-2);
      for(int k=0; k<3; ++k)
	nvec[k] /= norm;

      // Ez
      const double Ez[] = {0.,0.,1.};

      // Ez.nvec
      const double dot = Ez[0]*nvec[0]+Ez[1]*nvec[1]+Ez[2]*nvec[2];

      // nperp = nvec - (nvec.Ez)Ez, vector in the plane of nvec-Ez, orthogonal to nvec
      double nperp[3] = {0.,0.,0.};
      norm = 0.;
      for(int k=0; k<3; ++k)
	{ nperp[k] = nvec[k]-dot*Ez[k];
	  norm += nperp[k]*nperp[k]; }
      norm = std::sqrt(norm);
      CV_Assert(norm>1.e-2);
      for(int k=0; k<3; ++k)
	nperp[k] /= norm;

      // ncross = nperp x Ez = axis of rotation
      double ncross[3];
      norm = 0.;
      for(int k=0; k<3; ++k)
	{ ncross[k] = nperp[(k+1)%3]*Ez[(k+2)%3] - nperp[(k+2)%3]*Ez[(k+1)%3];
	  norm += ncross[k]*ncross[k]; }
      norm = std::sqrt(norm);
      CV_Assert(std::abs(1.-norm)<1.e-3);
      for(int k=0; k<3; ++k)
	ncross[k] /= norm;

      // Rotation matrix in the basis of nperp, Ez, cross = rotation by angle theta
      double cos = dot;
      double sin = std::sqrt(1.-dot*dot);
      double RMat[3][3] = {{cos, -sin, 0.},
			   {sin, cos, 0.},
			   {0., 0., 1.}};

      // Change basis to Ex, Ey, Ez
      // Rot = Qt RMat Q
      const double Qt[3][3] = {{nperp[0], Ez[0], ncross[0]},
			       {nperp[1], Ez[1], ncross[1]},
			       {nperp[2], Ez[2], ncross[2]}};
      for(int i=0; i<3; ++i)
	for(int j=0; j<3; ++j)
	  {
	    Rot[i][j] = 0.;
	    for(int k=0; k<3; ++k)
	      for(int L=0; L<3; ++L)
		Rot[i][j] += Qt[i][k]*RMat[k][L]*Qt[j][L];
	  }

      // Check that Ez = Rot(nvec)
      double check_Ez[3] = {0.,0.,0.};
      for(int i=0; i<3; ++i)
	for(int j=0; j<3; ++j)
	  check_Ez[i] += Rot[i][j]*nvec[j];
      CV_Assert(std::abs(Ez[0]-check_Ez[0]) +
		std::abs(Ez[1]-check_Ez[1]) + 
		std::abs(Ez[2]-check_Ez[2]) < 1.e-4);

      // -- done --
      return;
    }

  }
  
  // Projects all markers to a Z=0 plane
  void ArucoSurface::ProjectMergedPoseToXYPlane(double Rot3D[][3], double* tvec)
  {
    CV_Assert(is_merged==true && "ArucoSurface::ProjectMergedPoseToXYPlane- poses not merged");
    
    // Fit a plane through the set of all triangulated points
    double coefs[3];
    std::vector<int> markerIDs(mergedPose.surfMarkerIDs);
    std::vector<std::vector<cv::Point3d>> triPts = mergedPose.surfMarkerCoords;
    for(int i=0; i<mergedPose.nbgMarkers; ++i)
      { triPts.push_back(mergedPose.bgMarkerCoords[i]);
	markerIDs.push_back(mergedPose.bgMarkerIDs[i]); }
    FitPlane(triPts, coefs);

    // Translate the plane to pass through the origin
    tvec[0] = tvec[1] = tvec[2] = 0.;
    for(auto& ptvec:triPts)
      for(auto& pt:ptvec)
	{ tvec[0] += pt.x;
	  tvec[1] += pt.y;
	  tvec[2] += pt.z; }
    for(int k=0; k<3; ++k)
      tvec[k] /= static_cast<double>(4*triPts.size());

    for(auto& ptvec:triPts)
      for(auto& pt:ptvec)
	{ pt.x -= tvec[0];
	  pt.y -= tvec[1];
	  pt.z -= tvec[2]; }
    
    // Equation of the plane is now: Ax + By + Cz = 0 (after translation)
    // Find a rotation matrix that rotates n=(A,B,C) to Ez=(0,0,1)
    GetRotationToXYPlane(coefs, Rot3D);

    // Apply the computed transformation to the merged pose
    for(auto& ptvec:mergedPose.surfMarkerCoords)
      for(auto& pt:ptvec)
	{
	  double X[] = {pt.x, pt.y, pt.z};
	  double Y[3] = {0.,0.,0.};
	  for(int i=0; i<3; ++i)
	    for(int j=0; j<3; ++j)
	      Y[i] += Rot3D[i][j]*(X[i]-tvec[i]);
	  pt.x = Y[0];
	  pt.y = Y[1];
	  pt.z = Y[2];
	}
    for(auto& ptvec:mergedPose.bgMarkerCoords)
      for(auto& pt:ptvec)
	{
	  double X[] = {pt.x, pt.y, pt.z};
	  double Y[3] = {0.,0.,0.};
	  for(int i=0; i<3; ++i)
	    for(int j=0; j<3; ++j)
	      Y[i] += Rot3D[i][j]*(X[j]-tvec[j]);
	  pt.x = Y[0];
	  pt.y = Y[1];
	  pt.z = 0.;
	}

    // Translation to be returned: -R*t
    double temp[] = {tvec[0], tvec[1], tvec[2]};
    for(int i=0; i<3; ++i)
      { tvec[i] = 0.;
	for(int j=0; j<3; ++j)
	  tvec[i] -= Rot3D[i][j]*temp[j]; }
    
    // -- done --
    return;
  }
}
